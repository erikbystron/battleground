package cz.flay.battleground.listeners;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.listeners.listener.*;

public class ListenersManager {

    private final BattleGround plugin;

    public ListenersManager(BattleGround plugin) {
        this.plugin = plugin;
        registerListeners();
    }

    private void registerListeners(){
        new PlayerDeathListener();
        new PlayerRespawnListener();
        new PlayerJoinListener();
        new PlayerQuitListener();
        new PlayerDamageListener();
        new PlayerMoveListener();
        new PlayerToggleSneakListener();
        new PlayerToggleFlightListener();
        new PlayerInteractListener();
        new PlayerPickupItemListener();
        new PlayerDropItemListener();
        new PlayerCommandPreprocessListener();

        new BlockBreakListener();
        new BlockPlaceListener();
        new BlockExplodeListener();
        new BlockBurnListener();
        new BlockSpreadListener();
        new BlockIgniteListener();

        new FoodLevelChangeListener();

        new InventoryClickListener();
        new InventoryOpenListener();

        new EntityDamageByEntityListener();
        new EntityDamageListener();

        new GameJoinListener();
        new GameLeaveListener();
        new GameEndListener();
    }
}
