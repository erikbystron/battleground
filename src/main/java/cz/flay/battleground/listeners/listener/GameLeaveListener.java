package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.broadcast.BroadCaster;
import cz.flay.battleground.broadcast.templates.GameLeaved;
import cz.flay.battleground.events.player.GameLeaveEvent;
import cz.flay.battleground.listeners.BattleListener;
import org.bukkit.event.EventHandler;

public class GameLeaveListener extends BattleListener {

    @EventHandler
    public void onGameLeave(GameLeaveEvent e) {
        BroadCaster.cast(new GameLeaved(e.getPlayer()));
    }
}
