package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener extends BattleListener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        BattlePlayer player = plugin.getBattlePlayer(e.getPlayer());

        player.leaveGame();
        player.reset();
    }
}
