package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerToggleSneakEvent;

public class PlayerToggleSneakListener extends BattleListener {

    @EventHandler
    public void onToggleSneak(PlayerToggleSneakEvent e) {
        BattlePlayer player = plugin.getBattlePlayer(e.getPlayer());

        if (player.isPlaying()) {
            if (player.getGame().isInGame()) {
                if (player.getGame().getArena().getAirPlane().isPlayerIn(player) && !player.getBukkitPlayer().isSneaking()) {
                    player.getGame().getArena().getAirPlane().ejectPlayer(player);
                }
            }
        }
    }
}
