package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeathListener extends BattleListener {

    @EventHandler
    public void onDeath(PlayerDeathEvent e){
        BattlePlayer player = plugin.getBattlePlayer(e.getEntity());

        player.onDeath();

        Bukkit.getScheduler().runTaskLater(plugin, () -> player.getBukkitPlayer().spigot().respawn(), 1);

        if (player.isPlaying()) {
            if (player.getGame().isInGame()) {
                if (player.getGame().getArena().getAirPlane().isPlayerIn(player) || player.getGame().getArena().getAirPlane().isPlayerFalling(player)) {
                    player.getGame().getArena().getAirPlane().removePlayer(player);
                } else {
                    player.getBukkitPlayer().getLocation().getWorld().strikeLightning(player.getBukkitPlayer().getLocation());
                    plugin.getCorpsesManager().createCorpse(player);
                }

                player.setSpectator();
            } else {
                player.toLobby(plugin.getArenasManager().getLobby());
            }
        }
    }
}
