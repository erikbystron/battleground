package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.broadcast.BroadCaster;
import cz.flay.battleground.broadcast.templates.GameJoined;
import cz.flay.battleground.events.player.GameJoinEvent;
import cz.flay.battleground.listeners.BattleListener;
import org.bukkit.event.EventHandler;

public class GameJoinListener extends BattleListener {

    @EventHandler
    public void onGameJoin(GameJoinEvent e) {
        BroadCaster.cast(new GameJoined(e.getPlayer()));
    }
}
