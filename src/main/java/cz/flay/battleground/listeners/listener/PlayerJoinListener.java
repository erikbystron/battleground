package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.LocaleManager.MessageForm;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener extends BattleListener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent e){
        BattlePlayer player = plugin.getBattlePlayer(e.getPlayer());

        if (!Bukkit.getServer().getPluginManager().isPluginEnabled("LanguageAPI")) {
            player.setLanguage(plugin.getConfigManager().getConfig().getString("default.locale"));
            player.setMessageForm(MessageForm.valueOf(plugin.getConfigManager().getConfig().getString("default.gender").toUpperCase()));
        }

        player.toLobby(plugin.getArenasManager().getLobby());

        if (!player.getBukkitPlayer().hasPermission("battleground.admin.antiautojoin")){
            player.joinGame(plugin.getGamesManager().getGame());
        }
    }
}
