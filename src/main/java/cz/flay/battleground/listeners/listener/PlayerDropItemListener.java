package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.items.GameItemsManager;
import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import de.tr7zw.itemnbtapi.NBTItem;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerDropItemEvent;

public class PlayerDropItemListener extends BattleListener {

    private final GameItemsManager itemsManager;

    public PlayerDropItemListener() {
        itemsManager = plugin.getItemsManager();
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        BattlePlayer player = plugin.getBattlePlayer(e.getPlayer());

        NBTItem item = new NBTItem(e.getItemDrop().getItemStack());
        if (item.hasKey("#bg-sign")) {
            if (!itemsManager.checkHash(player, item.getItem())) {
                e.setCancelled(true);
                return;
            }
        }

        if (!player.isPlaying() && !player.hasAdminStatus()) {
            e.setCancelled(true);
        }
    }
}
