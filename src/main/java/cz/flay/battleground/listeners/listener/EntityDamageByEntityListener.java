package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.player.corpse.Corpse;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class EntityDamageByEntityListener extends BattleListener {

    @EventHandler
    public void onInteract(EntityDamageByEntityEvent e) {
        if (!(e.getDamager() instanceof  Player)) {
            return;
        }
        BattlePlayer player = plugin.getBattlePlayer((Player) e.getDamager());

        if (e.getEntity() instanceof ArmorStand) {
            if (player.isPlaying()) {
                if (player.getGame().isInGame()) {
                    Corpse corpse = plugin.getCorpsesManager().getCorpse(player.getGame().getArena(),
                                                                        e.getEntity().getName());

                    if (corpse == null) {
                        return;
                    }

                    player.getBukkitPlayer().openInventory(corpse.getInventory());
                }
            }

            e.setCancelled(true);
        }
    }
}
