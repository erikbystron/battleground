package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.items.GameItemsManager;
import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import de.tr7zw.itemnbtapi.NBTItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerPickupItemListener extends BattleListener {

    private final GameItemsManager itemsManager;

    public PlayerPickupItemListener() {
        itemsManager = plugin.getItemsManager();
    }

    @EventHandler
    public void onPickup(PlayerPickupItemEvent e) {
        BattlePlayer player = plugin.getBattlePlayer((Player) e.getPlayer());

        if (!player.isPlaying() && !player.hasAdminStatus()) {
            e.getItem().setItemStack(new ItemStack(Material.AIR));
            e.setCancelled(true);
            return;
        }

        NBTItem item = new NBTItem(e.getItem().getItemStack());
        if (item.hasKey("#bg-item")) {
            if (!itemsManager.checkHash(player, item.getItem())) {
                e.setCancelled(true);
                return;
            }

            if (itemsManager.gameItemsContains(item.getString("#bg-item"))) {
                e.getItem().setItemStack(itemsManager.translateItem(player, item.getItem()));
            }
        }
    }
}
