package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerRespawnListener extends BattleListener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onRespawn(PlayerRespawnEvent e) {
        BattlePlayer player = plugin.getBattlePlayer(e.getPlayer());

        if (!player.isPlaying() && !player.isSpectating()) {
            e.setRespawnLocation(plugin.getArenasManager().getLobby().getLocation());
        }

        if (player.isPlaying()) {
            if (!player.getGame().isInGame()) {
                e.setRespawnLocation(plugin.getArenasManager().getLobby().getLocation());
            } else {
                e.setRespawnLocation(player.getGame().getArena().getCenter().toLocation(player.getGame().getArena().getWorld()));
            }
        }
    }
}
