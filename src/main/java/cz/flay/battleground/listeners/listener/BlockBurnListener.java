package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.listeners.BattleListener;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBurnEvent;

public class BlockBurnListener extends BattleListener {

    @EventHandler
    public void onBurn(BlockBurnEvent e) {
        if (e.isCancelled()) {
            return;
        }

        World world = e.getBlock().getWorld();

        for (Arena arena : plugin.getArenasManager().getArenas()) {
            if (arena.getWorld().equals(world)) {
                e.setCancelled(true);
            }
        }
    }
}
