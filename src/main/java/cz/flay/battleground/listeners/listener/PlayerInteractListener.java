package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.items.GameItemsManager;
import cz.flay.battleground.items.GameItemsManager.Item;
import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import de.tr7zw.itemnbtapi.NBTItem;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteractListener extends BattleListener {

    private final GameItemsManager itemsManager;

    public PlayerInteractListener() {
        itemsManager = plugin.getItemsManager();
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        BattlePlayer player = plugin.getBattlePlayer(e.getPlayer());

        if (e.getItem() == null) {
            return;
        }

        NBTItem item = new NBTItem(e.getItem());
        if (item.hasKey("#bg-item")) {
            if (!itemsManager.checkHash(player, item.getItem())) {
                e.setCancelled(true);
                return;
            }

            if (itemsManager.gameItemsContains(item.getString("#bg-item"))) {
                if (!player.isPlaying()) {
                    e.setCancelled(true);
                    player.getBukkitPlayer().getInventory().remove(item.getItem());
                    return;
                }
            }


            if (itemsManager.controlItemsContains(item.getString("#bg-item"))) {
                e.setCancelled(true);

                if (player.isPlaying()) {
                    if (player.getGame().isInGame()) {
                        if (player.getGame().getArena().getAirPlane().isPlayerFalling(player)) {
                            if (itemsManager.itemEquals(Item.PARACHUTE, e.getItem())) {
                                player.openParachute();
                                player.getBukkitPlayer().getInventory().remove(e.getItem());
                            }
                        }
                    }
                }

                if (!player.isPlaying()) {

                }

                itemsManager.getControlItem(item.getString("#bg-item"))
                        .getClickActions().runAction(player, e.getAction());
            }
        }
    }
}
