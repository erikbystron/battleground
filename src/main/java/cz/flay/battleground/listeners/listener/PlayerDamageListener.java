package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;

public class PlayerDamageListener extends BattleListener {

    @EventHandler
    public void onFall(EntityDamageEvent e){
        if (e.getEntity() instanceof Player) {
            BattlePlayer player = plugin.getBattlePlayer((Player) e.getEntity());

            if (player.isPlaying()) {
                if (!player.getGame().isInGame()) {
                    e.setCancelled(true);
                } else {
                    if (player.getGame().getArena().getAirPlane().isPlayerFalling(player) && player.hasOpenedParachute()) {
                        e.setCancelled(true);
                        player.getGame().getArena().getAirPlane().removePlayer(player);
                    }
                }
            } else {
                e.setCancelled(true);
            }

            /*if (e.getCause().equals(DamageCause.FALL)){
                if (player.hasBrokenLeg){
                    player.getBukkitPlayer().setHealth(0);
                } else {
                    player.brokeLeg();
                }
            }*/
        }
    }
}
