package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerToggleFlightEvent;

public class PlayerToggleFlightListener extends BattleListener {

    @EventHandler
    public void onToggleFly(PlayerToggleFlightEvent e) {
        BattlePlayer player = plugin.getBattlePlayer(e.getPlayer());

        if (player.isFreezed()) {
            e.setCancelled(true);
        }
    }
}
