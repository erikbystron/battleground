package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.listeners.BattleListener;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;

public class EntityDamageListener extends BattleListener {

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntityType().equals(EntityType.ARMOR_STAND)) {
            e.setCancelled(true);
        }
    }
}
