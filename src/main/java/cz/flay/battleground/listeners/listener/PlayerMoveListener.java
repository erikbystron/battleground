package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.items.GameItemsManager.Item;
import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

public class PlayerMoveListener extends BattleListener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onMove(PlayerMoveEvent e) {
        BattlePlayer player = plugin.getBattlePlayer(e.getPlayer());

        if (e.getFrom().getX() == e.getTo().getX() && e.getFrom().getY() == e.getTo().getY() && e.getFrom().getZ() == e.getTo().getZ()) {
            return;
        }

        if (player.isFreezed()) {
            e.setTo(e.getFrom());
        }

        if (player.isPlaying()) {
            if (player.getGame().isInGame()) {
                if (player.getGame().getArena().getAirPlane().isPlayerFalling(player)) {
                    if (player.hasOpenedParachute()) {
                        if (e.getFrom().getY() > e.getTo().getY()) {
                            Vector velocity = player.getBukkitPlayer().getVelocity();
                            velocity.setY(velocity.getY() * 0.5D);
                            player.getBukkitPlayer().setVelocity(velocity);
                        } else {
                            player.getGame().getArena().getAirPlane().removePlayer(player);
                        }
                    } else if (player.getBukkitPlayer().getLocation().getY() <= player.getGame().getArena().getAirPlane().getAutoOpenHeight()) {
                        player.openParachute();
                        player.getBukkitPlayer().getInventory().remove(plugin.getItemsManager().getControlItem(player, Item.PARACHUTE));
                    }
                }
            }
        }
    }
}
