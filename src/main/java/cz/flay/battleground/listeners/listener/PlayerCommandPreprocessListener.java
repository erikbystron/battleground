package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.List;


public class PlayerCommandPreprocessListener extends BattleListener {

    private final List<String> lobbyAllowed;
    private final List<String> ingameAllowed;
    private final List<String> spectatingAllowed;

    public PlayerCommandPreprocessListener() {
        YamlConfiguration config = plugin.getConfigManager().getConfig();

        ingameAllowed = config.getStringList("allowed-commands.in-game");
        lobbyAllowed = config.getStringList("allowed-commands.at-lobby");
        spectatingAllowed = config.getStringList("allowed-commands.when-spectating");
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent e) {
        BattlePlayer player = plugin.getBattlePlayer(e.getPlayer());

        if (player.hasAdminStatus()) {
            return;
        }

        String command = e.getMessage().substring(1);

        if (command.equalsIgnoreCase("bg adminmode") &&
                player.getBukkitPlayer().hasPermission("battle-ground.command.player-adminmode")) {
            return;
        }

        if (player.isPlaying()) {
            if (check(ingameAllowed, command)) {
                e.setCancelled(true);
            }

            return;
        }

        if (player.isSpectating()) {
            if (check(spectatingAllowed, command)) {
                e.setCancelled(true);
            }

            return;
        }

        if (!player.isSpectating() && !player.isPlaying()) {
            if (check(lobbyAllowed, command)) {
                e.setCancelled(true);
            }

            return;
        }
    }

    private boolean check(List<String> allowed, String command) {
        for (String allowed_cmd : allowed) {
            if (command.contains(allowed_cmd)) {
                return false;
            }
        }
        return true;
    }
}
