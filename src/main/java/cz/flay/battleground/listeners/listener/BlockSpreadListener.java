package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.listeners.BattleListener;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockSpreadEvent;

public class BlockSpreadListener extends BattleListener {

    @EventHandler
    public void onSpread(BlockSpreadEvent e) {
        if (e.isCancelled() || !e.getSource().getType().equals(Material.FIRE)) {
            return;
        }

        World world = e.getSource().getWorld();

        for (Arena arena : plugin.getArenasManager().getArenas()) {
            if (arena.getWorld().equals(world)) {
                e.setCancelled(true);
            }
        }
    }
}
