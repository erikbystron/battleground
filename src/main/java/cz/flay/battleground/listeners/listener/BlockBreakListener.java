package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.arena.reseting.MapChange;
import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.arena.reseting.MapResetor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakListener extends BattleListener {

    private final MapResetor mapResetor;

    public BlockBreakListener() {
        mapResetor = plugin.getArenasManager().getMapResetor();
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        if (e.isCancelled()) {
            return;
        }

        BattlePlayer player = plugin.getBattlePlayer(e.getPlayer());

        if (player.isPlaying() && !player.hasAdminStatus()) {
            if (player.getGame().isInGame()) {
                if (player.getGame().getBreakableBlocks().contains(e.getBlock().getType())) {
                    mapResetor.registerChange(player.getGame().getArena(), new MapChange(e.getBlock().getState()));
                } else {
                    e.setCancelled(true);
                }
            }  else {
                e.setCancelled(true);
            }
        } else if (!player.hasAdminStatus()) {
            e.setCancelled(true);
        }
    }
}
