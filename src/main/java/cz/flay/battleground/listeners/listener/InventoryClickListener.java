package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.items.GameItemsManager;
import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import de.tr7zw.itemnbtapi.NBTItem;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCreativeEvent;

public class InventoryClickListener extends BattleListener {

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        BattlePlayer player = plugin.getBattlePlayer((Player) e.getWhoClicked());

        if (e.getClickedInventory() == null) {
            return;
        }

        if (e instanceof InventoryCreativeEvent) {
            if (!player.hasAdminStatus()) {
                e.setCancelled(true);
            }

            return;
        }

        if (e.getCurrentItem() != null) {
            NBTItem item = new NBTItem(e.getCurrentItem());
            if (item.hasKey("#bg-item")) {
                GameItemsManager itemsManager = plugin.getItemsManager();
                if (!itemsManager.checkHash(e.getInventory(), item.getItem())) {
                    e.setCancelled(true);
                    return;
                }
            }
        }

        if (e.getClickedInventory().equals(player.getBukkitPlayer().getInventory())) {
            if (!player.isPlaying() && !player.hasAdminStatus()) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem() != null) {
                NBTItem item = new NBTItem(e.getCurrentItem());
                if (item.hasKey("#bg-item")) {
                    GameItemsManager itemsManager = plugin.getItemsManager();

                    if (itemsManager.gameItemsContains(item.getString("#bg-item"))) {
                        if (!player.isPlaying() && !player.hasAdminStatus()) {
                            player.getBukkitPlayer().getInventory().remove(item.getItem());
                            return;
                        }
                    }

                    if (itemsManager.controlItemsContains(item.getString("#bg-item"))) {
                        if (player.isPlaying()) {

                        }

                        if (!player.isPlaying()) {

                        }

                        itemsManager.getControlItem(item.getString("#bg-item"))
                                .getClickActions().runAction(player, e.getClick());
                    }
                }
            }
        }
    }
}
