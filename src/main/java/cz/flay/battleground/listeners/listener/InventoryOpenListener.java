package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.items.GameItemsManager;
import cz.flay.battleground.listeners.BattleListener;
import cz.flay.battleground.player.BattlePlayer;
import de.tr7zw.itemnbtapi.NBTItem;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;

public class InventoryOpenListener extends BattleListener {

    @EventHandler
    public void onOpen(InventoryOpenEvent e) {
        BattlePlayer player = plugin.getBattlePlayer((Player) e.getPlayer());

        GameItemsManager itemsManager = plugin.getItemsManager();
        for (ItemStack itemStack : e.getInventory().getContents()) {
            if (itemStack == null) {
                continue;
            }

            NBTItem item = new NBTItem(itemStack);
            if (item.hasKey("#bg-item")) {
                itemsManager.checkHash(e.getInventory(), itemStack);
            }
        }
    }
}
