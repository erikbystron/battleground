package cz.flay.battleground.listeners.listener;

import cz.flay.battleground.broadcast.BroadCaster;
import cz.flay.battleground.broadcast.templates.GameEnded;
import cz.flay.battleground.events.game.GameEndEvent;
import cz.flay.battleground.listeners.BattleListener;
import org.bukkit.event.EventHandler;

public class GameEndListener extends BattleListener {

    @EventHandler
    public void onGameEnd(GameEndEvent e) {
        BroadCaster.cast(new GameEnded(e.getPlayers(), e.getWinners()));
    }
}
