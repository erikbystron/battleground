package cz.flay.battleground.listeners;

import cz.flay.battleground.BattleGround;
import org.bukkit.Bukkit;

public class BattleListener implements org.bukkit.event.Listener{

    protected final BattleGround plugin;

    protected BattleListener(){
        plugin = (BattleGround) Bukkit.getPluginManager().getPlugin("BattleGround");
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }
}
