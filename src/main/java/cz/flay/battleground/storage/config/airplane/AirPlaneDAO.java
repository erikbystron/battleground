package cz.flay.battleground.storage.config.airplane;

import cz.flay.battleground.arena.plane.AirPlane;
import cz.flay.battleground.arena.plane.PlaneRoute;
import org.bukkit.Location;

import java.util.List;

public interface AirPlaneDAO {

    void saveAirPlane(String id, AirPlane airPlane);

    AirPlaneDAO setSpeed(String id, int speed);
    AirPlaneDAO setHeight(String id, int height);
    AirPlaneDAO setTpTime(String id, int time);
    AirPlaneDAO setAutoOpen(String id, int height);
    AirPlaneDAO setPositions(String id, List<Location> positions);
    AirPlaneDAO addPosition(String id, Location position);
    AirPlaneDAO removePosition(String id, Location position);
    AirPlaneDAO setRoutes(String id, List<PlaneRoute> routes);
    AirPlaneDAO addRoute(String id, PlaneRoute route);
    AirPlaneDAO removeRoute(String id, PlaneRoute route);

    int getSpeed(String id);
    int getHeight(String id);
    int getTpTime(String id);
    int getAutoOpen(String id);
    List<Location> getPositions(String id);
    List<PlaneRoute> getRoutes(String id);
}
