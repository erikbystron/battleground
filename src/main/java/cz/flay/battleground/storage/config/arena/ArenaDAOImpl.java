package cz.flay.battleground.storage.config.arena;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.Arena.Shape;
import cz.flay.battleground.arena.Arena.BorderType;
import cz.flay.battleground.arena.border.types.radiation.zones.ZoneState;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.util.Vector;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ArenaDAOImpl implements ArenaDAO {

    private final BattleGround plugin;

    public ArenaDAOImpl(BattleGround plugin) {
        this.plugin = plugin;
    }

    private YamlConfiguration getFile(String id) {
        File file = new File(plugin.getDataFolder() + "/arenas/" + id + "/", "settings.yml");
        YamlConfiguration config = new YamlConfiguration();

        if (!file.exists()) {
            file.getParentFile().mkdirs();
            plugin.saveResource("settings.yml", true);
            File created = new File(plugin.getDataFolder(), "settings.yml");
            created.renameTo(file);
        }

        try {
            config.load(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return config;
    }

    private void saveFile(String id, YamlConfiguration config) {
        File file = new File(plugin.getDataFolder() + "/arenas/" + id + "/", "settings.yml");
        try {
            YamlConfiguration temp = new YamlConfiguration();
            temp.load(file);

            if (!temp.saveToString().equals(config.saveToString())) {
                config.save(file);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveArena(Arena arena) {
        try {
            String id = arena.getId();

            setName(id, arena.getName());
            setShape(id, arena.getShape());
            setBorderType(id, arena.getBorderType());
            setWorld(id, arena.getWorld());
            setCenter(id, arena.getCenter());
        } catch (NullPointerException e) {
            plugin.getLogger().warning("Some values are missing for arena " + arena.getId() + " - settings.yml");
        }
    }

    @Override
    public void deleteArena(String id) {

    }

    @Override
    public ArenaDAO setName(String id, String name) {
        YamlConfiguration config = getFile(id);
            config.set("name", name);
        saveFile(id, config);
        return this;
    }

    @Override
    public ArenaDAO setShape(String id, Shape shape) {
        YamlConfiguration config = getFile(id);
            config.set("shape", shape.toString());
        saveFile(id, config);
        return this;
    }

    @Override
    public ArenaDAO setZoneShape(String id, Shape shape) {
        YamlConfiguration config = getFile(id);
            config.set("border.zone-shape", shape.toString());
        saveFile(id, config);
        return this;
    }

    @Override
    public ArenaDAO setBorderType(String id, BorderType borderType) {
        YamlConfiguration config = getFile(id);
            config.set("border.type", borderType.toString());
        saveFile(id, config);
        return this;
    }

    @Override
    public ArenaDAO setWorld(String id, World world) {
        YamlConfiguration config = getFile(id);
            config.set("map", world.getName());
        saveFile(id, config);
        return this;
    }

    @Override
    public ArenaDAO setCenter(String id, Vector center) {
        YamlConfiguration config = getFile(id);
            config.set("center.x", center.getX());
            config.set("center.y", center.getY());
            config.set("center.z", center.getZ());
        saveFile(id, config);
        return this;
    }

    @Override
    public ArenaDAO setFirstPos(String id, Vector position) {
        YamlConfiguration config = getFile(id);
            config.set("firstPos.x", position.getX());
            config.set("firstPos.z", position.getZ());
        saveFile(id, config);
        return this;
    }

    @Override
    public ArenaDAO setSecondPos(String id, Vector position) {
        YamlConfiguration config = getFile(id);
            config.set("secondPos.x", position.getX());
            config.set("secondPos.z", position.getZ());
        saveFile(id, config);
        return this;
    }

    @Override
    public ArenaDAO setRadius(String id, double radius) {
        YamlConfiguration config = getFile(id);
            config.set("radius", radius);
        saveFile(id, config);
        return this;
    }

    @Override
    public ArenaDAO setBorderRadiation(String id, double radiation) {
        YamlConfiguration config = getFile(id);
            config.set("border.radiation.border", radiation);
        saveFile(id, config);
        return this;
    }

    @Override
    public ArenaDAO setZoneRadiation(String id, double radiation) {
        YamlConfiguration config = getFile(id);
            config.set("border.radiation.zone", radiation);
        saveFile(id, config);
        return this;
    }

    @Override
    public ArenaDAO setRadiationDecrease(String id, double radiation) {
        YamlConfiguration config = getFile(id);
            config.set("border.radiation.decrease", radiation);
        saveFile(id, config);
        return this;
    }

    @Override
    public ArenaDAO addZoneState(String id, ZoneState state) {
        List<ZoneState> states = getZoneStates(id);
            states.add(state);
        setZoneStates(id, states);
        return this;
    }

    @Override
    public ArenaDAO setZoneStates(String id, List<ZoneState> states) {
        YamlConfiguration config = getFile(id);
            config.set("zone-states", states);
        saveFile(id, config);
        return this;
    }

    @Override
    public String getName(String id) {
        return getFile(id).getString("name");
    }

    @Override
    public Shape getShape(String id) {
        return Shape.valueOf(getFile(id).getString("shape").toUpperCase());
    }

    @Override
    public Shape getZoneShape(String id) {
        return Shape.valueOf(getFile(id).getString("border.zone-shape").toUpperCase());
    }

    @Override
    public BorderType getBorderType(String id) {
        return BorderType.valueOf(getFile(id).getString("border.type").toUpperCase());
    }

    @Override
    public World getWorld(String id) {
        return Bukkit.getWorld(getFile(id).getString("map"));
    }

    @Override
    public Vector getCenter(String id) {
        YamlConfiguration config = getFile(id);
        int x = config.getInt("center.x");
        int y = config.getInt("center.y");
        int z = config.getInt("center.z");
        return new Vector(x, y, z);
    }

    @Override
    public Vector getFirstPos(String id) {
        YamlConfiguration config = getFile(id);
        int x = config.getInt("firstPos.x");
        int z = config.getInt("firstPos.z");
        return new Vector(x, 0, z);
    }

    @Override
    public Vector getSecondPos(String id) {
        YamlConfiguration config = getFile(id);
        int x = config.getInt("secondPos.x");
        int z = config.getInt("secondPos.z");
        return new Vector(x, 0, z);
    }

    @Override
    public double getRadius(String id) {
        return getFile(id).getDouble("radius");
    }

    @Override
    public double getBorderRadiation(String id) {
        return getFile(id).getDouble("border.radiation.border");
    }

    @Override
    public double getZoneRadiation(String id) {
        return getFile(id).getDouble("border.radiation.zone");
    }

    @Override
    public double getRadiationDecrease(String id) {
        return getFile(id).getDouble("border.radiation.decrease");
    }

    @Override
    public List<ZoneState> getZoneStates(String id) {
        return getFile(id).getList("zone-states") != null ? (List<ZoneState>) getFile(id).getList("zone-states") : new ArrayList<>();
    }
}
