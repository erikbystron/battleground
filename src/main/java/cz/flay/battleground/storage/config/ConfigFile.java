package cz.flay.battleground.storage.config;

import cz.flay.battleground.BattleGround;
import org.apache.commons.codec.digest.DigestUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class ConfigFile {

    private final BattleGround plugin;

    private File configFile;
    private YamlConfiguration config;

    private String fromLoad;

    public ConfigFile(File configFile) {
        plugin = (BattleGround) Bukkit.getServer().getPluginManager().getPlugin("BattleGround");
        load(configFile);
    }

    public ConfigFile(String path,  String name) {
        plugin = (BattleGround) Bukkit.getServer().getPluginManager().getPlugin("BattleGround");
        load(name, path);
    }

    public ConfigFile(String name) {
        plugin = (BattleGround) Bukkit.getServer().getPluginManager().getPlugin("BattleGround");
        load(name, "");
    }

    private void load(String name, String path) {
        configFile = new File(plugin.getDataFolder(), path + name + ".yml");

        if (!configFile.exists()) {
            configFile.getParentFile().mkdirs();
            try {
                plugin.saveResource(name + ".yml", false);
                File created = new File(plugin.getDataFolder(), name + ".yml");
                created.renameTo(configFile);
            } catch (IllegalArgumentException e) {
                try {
                    configFile.createNewFile();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        load(configFile);
    }

    private void load(File configFile) {
        this.configFile = configFile;
        config = new YamlConfiguration();

        try {
            config.load(configFile);
            fromLoad = DigestUtils.md5Hex(config.saveToString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void save() {
        try {
            if (!fromLoad.equals(DigestUtils.md5Hex(config.saveToString()))) {
                config.save(configFile);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete() {
        configFile.delete();
    }

    public YamlConfiguration getConfig() {
        return config;
    }
}
