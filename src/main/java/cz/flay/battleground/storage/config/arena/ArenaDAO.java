package cz.flay.battleground.storage.config.arena;

import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.Arena.Shape;
import cz.flay.battleground.arena.Arena.BorderType;
import cz.flay.battleground.arena.border.types.radiation.zones.ZoneState;
import org.bukkit.World;
import org.bukkit.util.Vector;

import java.util.List;

public interface ArenaDAO {

    void saveArena(Arena arena);
    void deleteArena(String id);

    ArenaDAO setName(String id, String name);
    ArenaDAO setShape(String id, Shape shape);
    ArenaDAO setZoneShape(String id, Shape shape);
    ArenaDAO setBorderType(String id, BorderType borderType);
    ArenaDAO setWorld(String id, World world);
    ArenaDAO setCenter(String id, Vector center);
    ArenaDAO setFirstPos(String id, Vector position);
    ArenaDAO setSecondPos(String id, Vector position);
    ArenaDAO setRadius(String id, double radius);
    ArenaDAO setBorderRadiation(String id, double radiation);
    ArenaDAO setZoneRadiation(String id, double radiation);
    ArenaDAO setRadiationDecrease(String id, double radiation);
    ArenaDAO addZoneState(String id, ZoneState state);
    ArenaDAO setZoneStates(String id, List<ZoneState> states);

    String getName(String id);
    Shape getShape(String id);
    Shape getZoneShape(String id);
    BorderType getBorderType(String id);
    World getWorld(String id);
    Vector getCenter(String id);
    Vector getFirstPos(String id);
    Vector getSecondPos(String id);
    double getRadius(String id);
    double getBorderRadiation(String id);
    double getZoneRadiation(String id);
    double getRadiationDecrease(String id);
    List<ZoneState> getZoneStates(String id);
}
