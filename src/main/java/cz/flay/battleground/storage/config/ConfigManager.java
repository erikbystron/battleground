package cz.flay.battleground.storage.config;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigManager {

    private final ConfigFile configFile, gameFile, radiationFile, broadcastFile;

    public ConfigManager() {
        configFile = new ConfigFile("config");
        gameFile = new ConfigFile("game-settings");
        radiationFile = new ConfigFile("radiation-effects");
        broadcastFile = new ConfigFile("broadcast");
    }

    public Location getLocation(String path) {
        double x = getConfig().getDouble(path + ".x");
        double y = getConfig().getDouble(path + ".y");
        double z = getConfig().getDouble(path + ".z");
        float yaw = (float) getConfig().getDouble(path + ".yaw");
        float pitch = (float) getConfig().getDouble(path + ".pitch");
        World world = Bukkit.getWorld(getConfig().getString(path + ".world"));
        return new Location(world, x, y, z, yaw, pitch);
    }

    public YamlConfiguration getConfig() {
        return configFile.getConfig();
    }

    public YamlConfiguration getGameConfig() {
        return gameFile.getConfig();
    }

    public YamlConfiguration getRadiationConfig() {
        return radiationFile.getConfig();
    }

    public YamlConfiguration getBroadcastConfig() {
        return broadcastFile.getConfig();
    }
}
