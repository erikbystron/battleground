package cz.flay.battleground.storage.config.airplane;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.plane.AirPlane;
import cz.flay.battleground.arena.plane.PlaneRoute;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AirPlaneDAOImpl implements AirPlaneDAO {

    private final BattleGround plugin;

    public AirPlaneDAOImpl(BattleGround plugin) {
        this.plugin = plugin;
    }

    private YamlConfiguration getFile(String id) {
        File file = new File(plugin.getDataFolder() + "/arenas/" + id + "/", "airplane.yml");
        YamlConfiguration config = new YamlConfiguration();

        if (!file.exists()) {
            file.getParentFile().mkdirs();
            plugin.saveResource("airplane.yml", true);
            File created = new File(plugin.getDataFolder(), "airplane.yml");
            created.renameTo(file);
        }

        try {
            config.load(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return config;
    }

    private void saveFile(String id, YamlConfiguration config) {
        File file = new File(plugin.getDataFolder() + "/arenas/" + id + "/", "airplane.yml");
        try {
            YamlConfiguration temp = new YamlConfiguration();
            temp.load(file);

            if (!temp.saveToString().equals(config.saveToString())) {
                config.save(file);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveAirPlane(String id, AirPlane airPlane) {
        try {
            setRoutes(id, airPlane.getRoutes());
            setPositions(id, airPlane.getPositions());
            setAutoOpen(id, airPlane.getAutoOpenHeight());
            setHeight(id, airPlane.getHeight());
            setSpeed(id, airPlane.getSpeed());
            setTpTime(id, airPlane.getTptime());
        } catch (NullPointerException e) {
            plugin.getLogger().warning("Some values are missing for arena " + id + " - airplane.yml");
        }
    }

    @Override
    public AirPlaneDAOImpl setSpeed(String id, int speed) {
        YamlConfiguration config = getFile(id);
            config.set("speed", speed);
        saveFile(id, config);
        return this;
    }

    @Override
    public AirPlaneDAOImpl setHeight(String id, int height) {
        YamlConfiguration config = getFile(id);
            config.set("height", height);
        saveFile(id, config);
        return this;
    }

    @Override
    public AirPlaneDAOImpl setTpTime(String id, int time) {
        YamlConfiguration config = getFile(id);
            config.set("tp-time", time);
        saveFile(id, config);
        return this;
    }

    @Override
    public AirPlaneDAOImpl setAutoOpen(String id, int height) {
        YamlConfiguration config = getFile(id);
            config.set("auto-open", height);
        saveFile(id, config);
        return this;
    }

    @Override
    public AirPlaneDAOImpl setPositions(String id, List<Location> positions) {
        YamlConfiguration config = getFile(id);
            config.set("positions", positions);
        saveFile(id, config);
        return this;
    }

    @Override
    public AirPlaneDAOImpl addPosition(String id, Location position) {
        List<Location> positions = getPositions(id);
            positions.add(position);
        setPositions(id, positions);
        return this;
    }

    @Override
    public AirPlaneDAOImpl removePosition(String id, Location position) {
        List<Location> positions = getPositions(id);
            positions.remove(position);
        setPositions(id, positions);
        return this;
    }

    @Override
    public AirPlaneDAOImpl setRoutes(String id, List<PlaneRoute> routes) {
        YamlConfiguration config = getFile(id);
            config.set("routes", routes);
        saveFile(id, config);
        return this;
    }

    @Override
    public AirPlaneDAOImpl addRoute(String id, PlaneRoute route) {
        List<PlaneRoute> routes = getRoutes(id);
            routes.add(route);
        setRoutes(id, routes);
        return this;
    }

    @Override
    public AirPlaneDAOImpl removeRoute(String id, PlaneRoute route) {
        List<PlaneRoute> routes = getRoutes(id);
            routes.remove(route);
        setRoutes(id, routes);
        return this;
    }

    @Override
    public int getSpeed(String id) {
        return getFile(id).getInt("speed");
    }

    @Override
    public int getHeight(String id) {
        return getFile(id).getInt("height");
    }

    @Override
    public int getTpTime(String id) {
        return getFile(id).getInt("tp-time");
    }

    @Override
    public int getAutoOpen(String id) {
        return getFile(id).getInt("auto-open");
    }

    @Override
    public List<Location> getPositions(String id) {
        return getFile(id).getList("positions") != null ? (List<Location>) getFile(id).getList("positions") : new ArrayList<>();
    }

    @Override
    public List<PlaneRoute> getRoutes(String id) {
        return getFile(id).getList("routes") != null ? (List<PlaneRoute>) getFile(id).getList("routes") : new ArrayList<>();
    }
}
