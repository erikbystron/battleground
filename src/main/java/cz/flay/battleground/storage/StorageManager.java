package cz.flay.battleground.storage;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.storage.config.airplane.AirPlaneDAO;
import cz.flay.battleground.storage.config.airplane.AirPlaneDAOImpl;
import cz.flay.battleground.storage.config.arena.ArenaDAO;
import cz.flay.battleground.storage.config.arena.ArenaDAOImpl;
import cz.flay.battleground.storage.config.chests.ChestsDAO;
import cz.flay.battleground.storage.config.chests.ChestsDAOImpl;
import cz.flay.battleground.storage.config.locations.LocationsDAO;
import cz.flay.battleground.storage.config.locations.LocationsDAOImpl;
import cz.flay.battleground.storage.data.DataDAO;
import cz.flay.battleground.storage.data.Storage;

public class StorageManager {

    private final AirPlaneDAO planeDAO;
    private final ArenaDAO arenaDAO;
    private final ChestsDAO chestsDAO;
    private final LocationsDAO locationsDAO;

    private DataDAO dataDAO;
    private Storage storage;

    public StorageManager(BattleGround plugin) {
        planeDAO = new AirPlaneDAOImpl(plugin);
        arenaDAO = new ArenaDAOImpl(plugin);
        chestsDAO = new ChestsDAOImpl(plugin);
        locationsDAO = new LocationsDAOImpl(plugin);
    }

    public AirPlaneDAO getPlaneDAO() {
        return planeDAO;
    }

    public ArenaDAO getArenaDAO() {
        return arenaDAO;
    }

    public ChestsDAO getChestsDAO() {
        return chestsDAO;
    }

    public LocationsDAO getLocationsDAO() {
        return locationsDAO;
    }
}
