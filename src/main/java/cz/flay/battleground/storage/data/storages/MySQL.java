package cz.flay.battleground.storage.data.storages;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.storage.data.Storage;
import org.bukkit.configuration.file.FileConfiguration;

import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class MySQL extends Storage {

    public MySQL(BattleGround plugin){
        this.plugin = plugin;
        connect();
    }

    @Override
    public void connect(){
        if (!isConnected()){
            try {
                FileConfiguration config = plugin.getConfigManager().getConfig();
                String db, ip, port, username, password;
                db = config.getString("mysql-options.database");
                ip = config.getString("mysql-options.host");
                port = config.getString("mysql-options.port");
                username = config.getString("mysql-options.username");
                password = config.getString("mysql-options.password");

                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection("jdbc:mysql://" + ip + ":" + port + "/" + db + "?autoReconnect=true", username, password);

                createTables();
            } catch (Exception e){
                plugin.getLogger().warning(e.getMessage());
                throw new RuntimeException("BattleGround can't run without database !");
            }
        }
    }

    @Override
    public void createTables(){
        if (isConnected()){
            try{
                PreparedStatement statement = connection.prepareStatement(
                        " CREATE TABLE IF NOT EXISTS Messages (id VARCHAR(255) NOT NULL , displayname VARCHAR(255) NOT NULL , jsonstring VARCHAR(2500) NOT NULL , actionbar VARCHAR(2500) NOT NULL , bossbar VARCHAR(2500) NOT NULL , minplayers INT NOT NULL , timer INT NOT NULL , permission VARCHAR(255) NOT NULL , sound VARCHAR(255) NOT NULL , tag VARCHAR(255) NOT NULL , UNIQUE (id)) CHARACTER SET utf8 COLLATE utf8_bin"
                );
                statement.executeUpdate();
                statement.close();
            }catch (Exception e){
                plugin.getLogger().warning(e.getMessage());
            }
        }
    }
}
