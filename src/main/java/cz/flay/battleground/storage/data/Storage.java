package cz.flay.battleground.storage.data;

import cz.flay.battleground.BattleGround;

import java.sql.Connection;

public class Storage {

    protected BattleGround plugin;
    protected Connection connection;

    public void connect(){

    }

    public void unConnect() {
        if (isConnected()) {
            try {
                connection.close();
            } catch (Exception e) {
                plugin.getLogger().warning(e.getMessage());
            }
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public boolean isConnected(){
        return connection != null;
    }

    public void createTables(){

    }

}
