package cz.flay.battleground.storage.data.storages;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.storage.data.Storage;

import java.io.File;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class SQLite extends Storage {

    private final File db;

    public SQLite(BattleGround plugin){
        this.plugin = plugin;
        db = new File(plugin.getDataFolder(),  "data.db");
        connect();
    }

    private void createdb(){
        if (!db.exists()){
            try{
                db.createNewFile();
            } catch (Exception e){
                plugin.getLogger().warning(e.getMessage());
            }
        }
    }

    @Override
    public void connect(){
        if (!isConnected()){
            if (db.exists()){
                try {
                    String url = "jdbc:sqlite:" + db;

                    Class.forName("org.sqlite.JDBC");
                    connection = DriverManager.getConnection(url);
                } catch (Exception e){
                    plugin.getLogger().warning(e.getMessage());
                }
            } else {
                createdb();
            }
        }
        createTables();
    }

    @Override
    public void createTables(){
        if (isConnected()){
            try{
                PreparedStatement statement = connection.prepareStatement(
                        "CREATE TABLE IF NOT EXISTS Messages (id VARCHAR(255) NOT NULL , displayname VARCHAR(255) NOT NULL , jsonstring VARCHAR(2500) NOT NULL , actionbar VARCHAR(2500) NOT NULL , bossbar VARCHAR(2500) NOT NULL , minplayers INT NOT NULL , timer INT NOT NULL , permissions VARCHAR(255) NOT NULL , sound VARCHAR(255) NOT NULL , tag VARCHAR(255) NOT NULL , UNIQUE (id))"
                );
                statement.executeUpdate();
                statement.close();
            }catch (Exception e){
                plugin.getLogger().warning(e.getMessage());
            }
        } else {
            connect();
        }
    }
}
