package cz.flay.battleground.storage.locale;

public enum Message {
    NO_PERMISSIONS("messages", "no-permissions"),
    ONLY_PLAYER("messages", "only-player-cmd"),
    USE_HELP("messages", "use-help"),

    GAME_PLANE_TELEPORTING_CHAT("game", "game-plane-teleporting.chat"),
    GAME_PLANE_TELEPORTING_TITLE("game", "game-plane-teleporting.title"),
    GAME_PLANE_TELEPORTING_SUBTITLE("game", "game-plane-teleporting.subtitle"),
    GAME_PLANE_TELEPORTING_ACTIONBAR("game", "game-plane-teleporting.action-bar"),
    GAME_PLANE_TELEPORTING_BOSSBAR("game", "game-plane-teleporting.boss-bar"),

    GAME_JOINED_CHAT("game", "game-joined.chat"),
    GAME_JOINED_TITLE("game", "game-joined.title"),
    GAME_JOINED_SUBTITLE("game", "game-joined.subtitle"),
    GAME_JOINED_ACTIONBAR("game", "game-joined.action-bar"),
    GAME_JOINED_BOSSBAR("game", "game-joined.boss-bar"),

    GAME_LEAVED_CHAT("game", "game-leaved.chat"),
    GAME_LEAVED_TITLE("game", "game-leaved.title"),
    GAME_LEAVED_SUBTITLE("game", "game-leaved.subtitle"),
    GAME_LEAVED_ACTIONBAR("game", "game-leaved.action-bar"),
    GAME_LEAVED_BOSSBAR("game", "game-leaved.boss-bar"),

    GAME_ENDED_CHAT("game", "game-ended.chat"),
    GAME_ENDED_TITLE("game", "game-ended.title"),
    GAME_ENDED_SUBTITLE("game", "game-ended.subtitle"),
    GAME_ENDED_ACTIONBAR("game", "game-ended.action-bar"),
    GAME_ENDED_BOSSBAR("game", "game-ended.boss-bar"),

    GAME_COUNT_DOWN_CHAT("game", "game-count-down.chat"),
    GAME_COUNT_DOWN_TITLE("game", "game-count-down.title"),
    GAME_COUNT_DOWN_SUBTITLE("game", "game-count-down.subtitle"),
    GAME_COUNT_DOWN_ACTIONBAR("game", "game-count-down.action-bar"),
    GAME_COUNT_DOWN_BOSSBAR("game", "game-count-down.boss-bar"),

    ARENA_NAME_INSERT("menus", "anvil-insert.arena-name"),
    ARENA_NAME_ALREADY_USED_INSERT("menus", "anvil-insert.arena-name-already-used"),

    GAME_MENU_ITEM_NAME("menus", "menu-items.main-menu.game-menu-item.name"),
    GAME_MENU_ITEM_LORE("menus", "menu-items.main-menu.game-menu-item.lore"),

    CONFIG_MENU_ITEM_NAME("menus", "menu-items.main-menu.config-menu-item.name"),
    CONFIG_MENU_ITEM_LORE("menus", "menu-items.main-menu.config-menu-item.lore"),

    ITEMS_MENU_ITEM_NAME("menus", "menu-items.main-menu.items-menu-item.name"),
    ITEMS_MENU_ITEM_LORE("menus", "menu-items.main-menu.items-menu-item.lore"),

    ARENAS_MENU_ITEM_NAME("menus", "menu-items.main-menu.arenas-menu-item.name"),
    ARENAS_MENU_ITEM_LORE("menus", "menu-items.main-menu.arenas-menu-item.lore"),

    BACK_ITEM_NAME("menus", "menu-items.backward.name"),
    BACK_ITEM_LORE("menus", "menu-items.backward.lore"),

    NEW_ARENA_ITEM_NAME("menus", "menu-items.arenas-menu.new-arena-item.name"),
    NEW_ARENA_ITEM_LORE("menus", "menu-items.arenas-menu.new-arena-item.lore"),

    ARENA_INFO_ITEM_NAME("menus", "menu-items.arena-menu.arena-info-item.name"),
    ARENA_INFO_ITEM_LORE("menus", "menu-items.arena-menu.arena-info-item.lore"),
    PLANE_ITEM_NAME("menus", "menu-items.arena-menu.plane-item.name"),
    PLANE_ITEM_LORE("menus", "menu-items.arena-menu.plane-item.lore"),
    DROPS_ITEM_NAME("menus", "menu-items.arena-menu.drops-item.name"),
    DROPS_ITEM_LORE("menus", "menu-items.arena-menu.drops-item.lore"),
    BORDER_ITEM_NAME("menus", "menu-items.arena-menu.border-item.name"),
    BORDER_ITEM_LORE("menus", "menu-items.arena-menu.border-item.lore"),
    RADIATION_ITEM_NAME("menus", "menu-items.arena-menu.radiation-item.name"),
    RADIATION_ITEM_LORE("menus", "menu-items.arena-menu.radiation-item.lore"),

    NO_FALL_ZONE_ITEM_NAME("menus", "menu-items.route-menu.no-fall-zone-item.name"),
    NO_FALL_ZONE_ITEM_LORE("menus", "menu-items.route-menu.no-fall-zone-item.lore"),
    FALL_ZONE_ITEM_NAME("menus", "menu-items.route-menu.fall-zone-item.name"),
    FALL_ZONE_ITEM_LORE("menus", "menu-items.route-menu.fall-zone-item.lore"),

    PLANE_HEIGHT_ITEM_NAME("menus", "menu-items.plane-menu.plane-height-item.name"),
    PLANE_HEIGHT_ITEM_LORE("menus", "menu-items.plane-menu.plane-height-item.lore"),
    TP_TIME_ITEM_NAME("menus", "menu-items.plane-menu.tp-time-item.name"),
    TP_TIME_ITEM_LORE("menus", "menu-items.plane-menu.tp-time-item.lore"),
    AUTO_OPEN_HEIGHT_ITEM_NAME("menus", "menu-items.plane-menu.auto-open-height-item.name"),
    AUTO_OPEN_HEIGHT_ITEM_LORE("menus", "menu-items.plane-menu.auto-open-height-item.lore"),
    SPEED_ITEM_NAME("menus", "menu-items.plane-menu.speed-item.name"),
    SPEED_ITEM_LORE("menus", "menu-items.plane-menu.speed-item.lore"),
    PLUS_ITEM_NAME("menus", "menu-items.plane-menu.plus-item.name"),
    PLUS_ITEM_LORE("menus", "menu-items.plane-menu.plus-item.lore"),
    MINUS_ITEM_NAME("menus", "menu-items.plane-menu.minus-item.name"),
    MINUS_ITEM_LORE("menus", "menu-items.plane-menu.minus-item.lore"),
    ROUTES_ITEM_NAME("menus", "menu-items.plane-menu.routes-item.name"),
    ROUTES_ITEM_LORE("menus", "menu-items.plane-menu.routes-item.lore"),
    POSITIONS_ITEM_NAME("menus", "menu-items.plane-menu.positions-item.name"),
    POSITIONS_ITEM_LORE("menus", "menu-items.plane-menu.positions-item.lore"),

    ROUTES_MENU_TITLE("menus", "menu-titles.routes-menu"),
    ROUTE_MENU_TITLE("menus", "menu-titles.route-menu"),
    POSITIONS_MENU_TITLE("menus", "menu-titles.positions-menu"),
    POSITION_MENU_TITLE("menus", "menu-titles.position-menu"),
    BORDER_MENU_TITLE("menus", "menu-titles.border-menu"),
    DROPS_MENU_TITLE("menus", "menu-titles.drops-menu"),
    PLANE_MENU_TITLE("menus", "menu-titles.plane-menu"),
    RADIATION_MENU_TITLE("menus", "menu-titles.radiation-menu"),
    ARENA_MENU_TITLE("menus", "menu-titles.arena-menu"),
    ARENAS_MENU_TITLE("menus", "menu-titles.arenas-menu"),
    CONFIG_MENU_TITLE("menus", "menu-titles.config-menu"),
    ITEMS_MENU_TITLE("menus", "menu-titles.item-menu"),
    ITEM_MENU_TITLE("menus", "menu-titles.item-menu"),
    GAME_MENU_TITLE("menus", "menu-titles.game-menu"),
    MAIN_MENU_TITLE("menus", "menu-titles.main-menu");

    private final String file;
    private final String path;

    Message(String file, String path) {
        this.file = file;
        this.path = path;
    }

    public String getFile() {return file;}
    public String getPath() {return path;}
}
