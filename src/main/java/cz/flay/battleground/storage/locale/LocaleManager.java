package cz.flay.battleground.storage.locale;

import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.util.Placeholders;

import java.util.List;

public interface LocaleManager {

    String getConsoleMessage(Message message);

    String getMessage(BattlePlayer player, Message message);

    String getMessage(BattlePlayer player, Message message, Placeholders placeholders);

    List<String> getMessagesList(BattlePlayer player, Message message);

    List<String> getMessagesList(BattlePlayer player, Message message, Placeholders placeholders);

    enum MessageForm {
        WOMAN, MAN
    }

}
