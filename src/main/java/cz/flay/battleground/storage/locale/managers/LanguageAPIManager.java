package cz.flay.battleground.storage.locale.managers;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.LocaleManager;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.Placeholders;
import cz.flay.languageapi.LanguageAPI;
import cz.flay.languageapi.api.LangAPI;

import java.util.ArrayList;
import java.util.List;

public class LanguageAPIManager implements LocaleManager {

    private final LangAPI langAPI;

    public LanguageAPIManager(BattleGround plugin) {
        langAPI = ((LanguageAPI) plugin.getServer().getPluginManager().getPlugin("LanguageAPI")).getLangAPI(plugin);
        createDefaultFiles();
    }


    private void createDefaultFiles() {
        List<String> files = new ArrayList<>();
        //files.add("");

        for (String file : files) {
            createDefault(file);
        }
    }

    private void createDefault(String file) {
        langAPI.createDefault(file, false);
    }

    @Override
    public String getMessage(BattlePlayer player, Message message) {
        return langAPI.getMessage(player.getBukkitPlayer(), message.getFile(), message.getPath());
    }

    @Override
    public String getMessage(BattlePlayer player, Message message, Placeholders placeholders) {
        return langAPI.getMessage(player.getBukkitPlayer(), message.getFile(), message.getPath(),
                new cz.flay.languageapi.util.Placeholders(placeholders.getPlaceholders()));
    }

    @Override
    public List<String> getMessagesList(BattlePlayer player, Message message) {
        return langAPI.getMessages(player.getBukkitPlayer(), message.getFile(), message.getPath());
    }

    @Override
    public List<String> getMessagesList(BattlePlayer player, Message message, Placeholders placeholders) {
        return langAPI.getMessages(player.getBukkitPlayer(), message.getFile(), message.getPath(),
                new cz.flay.languageapi.util.Placeholders(placeholders.getPlaceholders()));
    }

    @Override
    public String getConsoleMessage(Message message) {
        return null;
    }
}
