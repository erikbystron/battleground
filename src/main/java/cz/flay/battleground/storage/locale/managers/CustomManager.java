package cz.flay.battleground.storage.locale.managers;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.LocaleManager;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.FileHash;
import cz.flay.battleground.util.Placeholders;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

public class CustomManager implements LocaleManager {

    private final BattleGround plugin;
    private final FileHash fileHash;

    private final HashMap<String, YamlConfiguration> locales;
    private final HashMap<String, String> fileHashes;

    private final boolean placeholderAPIEnabled;

    public CustomManager(BattleGround plugin) {
        this.plugin = plugin;
        this.fileHash = new FileHash();
        locales = new HashMap<>();
        fileHashes = new HashMap<>();
        createDefaultFiles();

        placeholderAPIEnabled = Bukkit.getServer().getPluginManager().isPluginEnabled("PlaceholderAPI");
    }

    private YamlConfiguration getFile(String locale, String fileName) {

        File file = new File(plugin.getDataFolder() + "/locales/" + locale + "/", fileName + ".yml");

        if (file.exists()) {
            if (locales.containsKey(locale + "-" + fileName) && fileHashes.get(locale + "-" + fileName).equals(fileHash.checksum(file))) {
                return locales.get(locale + "-" + fileName);
            }
        } else {
            File defaultFile = new File(plugin.getDataFolder() + "/locales/default/", fileName + ".yml");
            if (!defaultFile.exists()) {
                createDefault(fileName);
            } else {
                if (locales.containsKey("default-" + fileName) && fileHashes.get("default-" + fileName).equals(fileHash.checksum(defaultFile))) {
                    return locales.get("default-" + fileName);
                }
            }

            file = defaultFile;
            locale = "default";
        }

        YamlConfiguration config = new YamlConfiguration();

        try {
            config.load(file);
            locales.put(locale + "-" + fileName, config);
            fileHashes.put(locale + "-" + fileName, fileHash.checksum(file));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!config.contains("multi-gender") || !config.contains("prefix")) {
            if (!config.contains("multi-gender")) {
                config.set("multi-gender", false);
            }

            if (!config.contains("prefix")) {
                config.set("prefix", "[BattleGround]");
            }

            saveConfig(config, file);
        }

        return config;
    }

    private void saveLocale(YamlConfiguration config, String locale, String fileName) {
        if (locale == null) {
            locale = "default";
        }

        File file  = new File(plugin.getDataFolder() + "/locales/" + locale + "/", fileName + ".yml");

        if (!file.exists()) {
            file = new File(plugin.getDataFolder() + "/locales/default/", fileName + ".yml");

            if (!file.exists()) {
                createFile(file);
            }
        }

        saveConfig(config, file);
    }

    private void createFile(File file) {
        file.getParentFile().mkdirs();
        try {
            file.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveConfig(YamlConfiguration config, File file) {
        try {
            config.save(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private String getMessage(String locale, Message message, LocaleManager.MessageForm form) {
        YamlConfiguration config = getFile(locale, message.getFile());

        if (config.getBoolean("multi-gender")) {
            try {
                String string = config.getString(message.getPath() + "." + form.toString().toLowerCase());
                return applyColors(replacePrefix(string, config.getString("prefix")));
            } catch (NullPointerException e) {
                if (form == null) {
                    form = LocaleManager.MessageForm.MAN;
                }

                if (!form.equals(LocaleManager.MessageForm.WOMAN)) {
                    config.set(message.getPath() + "." + form.toString().toLowerCase(), "Missing value: " + message.getPath());
                    saveLocale(config, locale, message.getFile());
                    return "Missing value: " + message.getPath();
                } else {
                    return getMessage(locale, message, LocaleManager.MessageForm.MAN);
                }
            }
        } else {
            try {
                String string = config.getString(message.getPath());
                return applyColors(replacePrefix(string, config.getString("prefix")));
            } catch (NullPointerException e) {
                config.set(message.getPath(), "Missing value: " + message.getPath());
                saveLocale(config, locale, message.getFile());
                return "Missing value: " + message.getPath();
            }
        }
    }

    private List<String> getMessagesList(String locale, Message message, LocaleManager.MessageForm form) {
        YamlConfiguration config = getFile(locale, message.getFile());
        List<String> messages = new ArrayList<>();

        if (config.getBoolean("multi-gender")) {
            try {
                for (String string : config.getStringList(message.getPath() + "." + form.toString().toLowerCase())) {
                    messages.add(applyColors(replacePrefix(string, config.getString("prefix"))));
                }
                return messages;
            } catch (NullPointerException e) {
                if (form == null) {
                    form = LocaleManager.MessageForm.MAN;
                }

                if (!form.equals(LocaleManager.MessageForm.WOMAN)) {
                    messages.add("Missing value: " + message.getPath());
                    config.set(message.getPath() + "." + form.toString().toLowerCase(), messages);
                    saveLocale(config, locale, message.getFile());
                    return messages;
                } else {
                    return getMessagesList(locale, message, LocaleManager.MessageForm.MAN);
                }
            }
        } else {
            try {
                for (String string : config.getStringList(message.getPath())) {
                    messages.add(applyColors(replacePrefix(string, config.getString("prefix"))));
                }
                return messages;
            } catch (NullPointerException e) {
                messages.add("Missing value: " + message.getPath());
                config.set(message.getPath(), messages);
                saveLocale(config, locale, message.getFile());
                return messages;
            }
        }
    }

    private void createDefaultFiles() {
        List<String> files = new ArrayList<>();
            //files.add("");

        for (String file : files) {
            createDefault(file);
        }
    }

    private void createDefault(String fileName) {
        File file = new File(plugin.getDataFolder() + "/locales/default/", fileName + ".yml");

        if (!file.exists()) {
            file.getParentFile().mkdirs();
            createFile(file);

            InputStream inputStream = plugin.getResource(fileName + ".yml");
            FileOutputStream outputStream;

            try {
                outputStream = new FileOutputStream(file);

                byte[] buf = new byte[1024];
                int i = 0;

                while ((i = inputStream.read(buf)) != -1) {
                    outputStream.write(buf, 0, i);
                }
                inputStream.close();
                outputStream.close();
            } catch (Exception e) {
                plugin.getLogger().log(Level.SEVERE, "Failed to load language file "+ fileName +" from JAR !", e);
            }
        }
    }

    @Override
    public String getMessage(BattlePlayer player, Message message) {
        return getMessage(player, message, new Placeholders());
    }

    @Override
    public String getMessage(BattlePlayer player, Message message, Placeholders placeholders) {
        return placeholders.applyPlaceholders(player, getMessage(player.getLanguage(), message, player.getMessageForm()));
    }

    @Override
    public List<String> getMessagesList(BattlePlayer player, Message message) {
        return getMessagesList(player, message, new Placeholders());
    }

    @Override
    public List<String> getMessagesList(BattlePlayer player, Message message, Placeholders placeholders) {
        return placeholders.applyPlaceholders(player, getMessagesList(player.getLanguage(), message, player.getMessageForm()));
    }

    @Override
    public String getConsoleMessage(Message message) {
        YamlConfiguration file = getFile("default", message.getFile());
        if (file.getString(message.getPath()) != null) {
            if (!file.getString(message.getPath()).equals("")) {
                return file.getString(message.getPath());
            } else {
                return message.getPath();
            }
        } else {
            file.set(message.getPath(), "");
            saveLocale(file, "default", message.getFile());
            return message.getPath();
        }
    }

    private String replaceAll(String message, HashMap<String, String> placeholders) {

        for (String placeholder : placeholders.keySet()) {
            message = message.replaceAll(placeholder, placeholders.get(placeholder));
        }

        return message;
    }

    private String replacePrefix(String message, String prefix) {
        return message.replaceAll("%PREFIX%", prefix);
    }

    private String applyColors(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

}
