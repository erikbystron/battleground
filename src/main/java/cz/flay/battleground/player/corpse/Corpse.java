package cz.flay.battleground.player.corpse;

import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.util.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class Corpse {

    private ArmorStand corpse;
    private Inventory inventory;

    private int taskID;

    Corpse(BattlePlayer player) {
        inventory = player.getBukkitPlayer().getInventory();
        create(player.getBukkitPlayer());
    }

    private void create(Player player) {
        PlayerInventory playerInventory = player.getInventory();
        corpse = player.getWorld().spawn(player.getLocation(), ArmorStand.class);
            corpse.setHelmet(new ItemBuilder(player).toItemStack());
            corpse.setVisible(false);
            corpse.setGravity(true);
            corpse.setCanPickupItems(false);
            corpse.setCustomNameVisible(true);
            corpse.setCustomName(player.getDisplayName() + "'s Corpse");
            corpse.setChestplate(playerInventory.getChestplate());

        inventory = Bukkit.createInventory(null, 54, player.getDisplayName() + "'s Corpse");
        int slot = 0;
        for (ItemStack item : playerInventory) {
            if (item == null) {
                continue;
            }

            if (item.getType().equals(Material.AIR)) {
                continue;
            }

            inventory.setItem(slot, item);
            slot++;
        }


        final double[] height = {0.0};
        final double[] angle = {0.0};
        final boolean[] raising = {true};

        final double[] y = new double[1];

        Bukkit.getScheduler().runTaskLater(Bukkit.getPluginManager().getPlugin("BattleGround"), () -> {
            y[0] = corpse.getLocation().getY();
        }, 20);

        taskID = Bukkit.getScheduler().runTaskTimer(Bukkit.getPluginManager().getPlugin("BattleGround"), () -> {
            if (corpse.hasGravity()) {
                corpse.setGravity(false);
            }

            if (raising[0]) {
                angle[0] += 0.2;
            } else {
                angle[0] -= 0.2;
            }

            height[0] = Math.sin(angle[0]);

            if (angle[0] == 90) {
                raising[0] = false;
            } else if (angle[0] == 0) {
                raising[0] = true;
            }

            Location location = corpse.getLocation();
                location.setY(y[0] + height[0]);

            corpse.teleport(location);
        }, 20, 1).getTaskId();
    }

    public Inventory getInventory() {
        return inventory;
    }

    public ArmorStand getArmorStand() {
        return corpse;
    }

    void remove() {
        Bukkit.getScheduler().cancelTask(taskID);
        corpse.remove();
    }
}
