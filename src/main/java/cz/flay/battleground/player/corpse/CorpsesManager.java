package cz.flay.battleground.player.corpse;

import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.player.BattlePlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CorpsesManager {

    private final HashMap<Arena, List<Corpse>> corpses;

    public CorpsesManager() {
        corpses = new HashMap<>();
    }

    public void createCorpse(BattlePlayer player) {
        if (player.getGame().getArena() == null) {
            return;
        }

        Arena arena = player.getGame().getArena();

        if (!corpses.containsKey(arena)) {
            corpses.put(arena, new ArrayList<>());
        }

        corpses.get(arena).add(new Corpse(player));
    }

    public void despawnCorpses(Arena game) {
        if (corpses.containsKey(game)) {
            for (Corpse corpse : corpses.get(game)) {
                corpse.remove();
            }
        }
    }

    public Corpse getCorpse(Arena arena, String name) {
        if (!corpses.containsKey(arena)) {
            return null;
        }

        for (Corpse corpse : corpses.get(arena)) {
            if (corpse.getArmorStand().getName().equals(name)) {
                return corpse;
            }
        }

        return null;
    }
}
