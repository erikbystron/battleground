package cz.flay.battleground.player;

import cz.flay.battleground.BattleGround;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;

public class BattlePlayerManager implements Listener {

    private final HashMap<Player, BattlePlayer> playerList;

    public BattlePlayerManager(BattleGround plugin){
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        playerList = new HashMap<>();
    }

    public BattlePlayer getPlayer(Player player){
        if (!playerList.containsKey(player)){
            playerList.put(player, new BattlePlayer(player));
        }

        return playerList.get(player);
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onJoin(PlayerJoinEvent e) { playerList.put(e.getPlayer(), new BattlePlayer(e.getPlayer()));
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onQuit(PlayerQuitEvent e){
        playerList.remove( e.getPlayer());
    }

}
