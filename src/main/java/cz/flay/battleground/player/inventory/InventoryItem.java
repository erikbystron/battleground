package cz.flay.battleground.player.inventory;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;

import java.util.HashMap;
import java.util.Map;

import static cz.flay.battleground.Logger.error;

@SerializableAs("InventoryItem")
public class InventoryItem implements ConfigurationSerializable {

    private int slot;
    private int priority;
    private String permission;
    private String item;

    public InventoryItem(int slot, int priority, String permission, String item) {
        this.slot = slot;
        this.priority = priority;
        this.permission = permission;
        this.item = item;
    }

    public InventoryItem() {
        priority = 0;
    }

    public int getSlot() {
        return slot;
    }

    public int getPriority() {
        return priority;
    }

    public String getPermission() {
        return permission;
    }

    public String getItem() {
        return item;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();
            data.put("slot", slot);
            data.put("priority", priority);
            data.put("permission", permission);
            data.put("item", item);
        return data;
    }

    public static InventoryItem deserialize(Map<String, Object> args) {
        try {
            int slot = (int) args.get("slot");
            int priority = (int) args.get("priority");
            String permission = (String) args.get("permission");
            String item = (String) args.get("item");

            return new InventoryItem(slot, priority, permission, item);
        } catch (NullPointerException e) {
            error("Problems with loading some inventory item ! Please check configuration");
            return new InventoryItem();
        }
    }
}
