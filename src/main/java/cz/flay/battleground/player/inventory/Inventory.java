package cz.flay.battleground.player.inventory;

import cz.flay.battleground.player.inventory.InventoriesManager.InventoryType;
import cz.flay.battleground.storage.config.ConfigFile;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.List;

import static cz.flay.battleground.Logger.error;

public class Inventory {

    private final String id;

    private int priority;
    private String permission;
    private InventoryType type;
    private List<InventoryItem> inventoryItems;

    public Inventory(String id) {
        this.id = id;
        load();
    }

    public int getPriority() {
        return priority;
    }

    public String getPermission() {
        return permission;
    }

    public InventoryType getType() {
        return type;
    }

    public List<InventoryItem> getInventoryItems() {
        return inventoryItems;
    }

    private void load() {
        YamlConfiguration config = new ConfigFile("/inventories/", id).getConfig();

        try {
            priority = config.getInt("priority");
            permission = config.getString("permission");
            type = InventoryType.valueOf(config.getString("type").toUpperCase());
            inventoryItems = (List<InventoryItem>) config.getList("items");
        } catch (NullPointerException e) {
            error("Problems with loading inventory " + id + ", please check configuration");
            priority = 0;
        }
    }

}
