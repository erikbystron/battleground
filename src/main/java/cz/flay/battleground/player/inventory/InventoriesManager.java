package cz.flay.battleground.player.inventory;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.player.BattlePlayer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class InventoriesManager {

    private final BattleGround plugin;
    private final List<Inventory> inventories;

    public InventoriesManager(BattleGround plugin) {
        this.plugin = plugin;
        inventories = new ArrayList<>();

        loadInventories();
    }

    private void loadInventories() {
        File path = new File(plugin.getDataFolder() + "/inventories/");

        if (!path.exists()) {
            path.mkdirs();
        }

        try {
            for (File file : Objects.requireNonNull(path.listFiles())) {
                inventories.add(new Inventory(file.getName().substring(0, file.getName().indexOf("."))));
            }
        } catch (NullPointerException ignored) { }
    }

    public Inventory getInventory(BattlePlayer player, InventoryType type) {
        Inventory selected = null;

        for (Inventory inventory : inventories) {
            if (inventory.getPriority() == 0) {
                continue;
            }

            if (inventory.getType().equals(type)) {
                boolean hasPerms = true;
                if (inventory.getPermission() != null && !inventory.getPermission().equals("")) {
                    hasPerms = player.getBukkitPlayer().hasPermission(inventory.getPermission());
                }

                if (hasPerms) {
                    if (selected != null) {
                        if (selected.getPriority() < inventory.getPriority()) {
                            selected = inventory;
                        }
                    } else {
                        selected = inventory;
                    }
                }
            }
        }

        return selected;
    }

    public enum InventoryType {
        LOBBY, SPECTATOR, WAITING
    }
}
