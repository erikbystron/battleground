package cz.flay.battleground.player.scoreboard;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.player.BattlePlayer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ScoreBoardsManager {

    private final BattleGround plugin;
    private final List<ScoreBoard> scoreBoards;

    public ScoreBoardsManager(BattleGround plugin) {
        this.plugin = plugin;
        this.scoreBoards = new ArrayList<>();

        loadScoreBoards();
    }

    private void loadScoreBoards() {
        File path = new File(plugin.getDataFolder() + "/scoreboards/");

        if (!path.exists()) {
            path.mkdirs();
        }

        try {
            for (File file : Objects.requireNonNull(path.listFiles())) {
                scoreBoards.add(new ScoreBoard(file.getName().substring(0, file.getName().indexOf("."))));
            }
        } catch (NullPointerException ignored) { }
    }

    public ScoreBoard getScoreBoard(BattlePlayer player, ScoreBoardType type) {
        ScoreBoard selected = null;

        for (ScoreBoard scoreBoard : scoreBoards) {
            if (scoreBoard.getPriority() == 0) {
                continue;
            }

            if (scoreBoard.getType().equals(type)) {
                boolean hasPerms = true;
                if (scoreBoard.getPermission() != null && !scoreBoard.getPermission().equals("")) {
                    hasPerms = player.getBukkitPlayer().hasPermission(scoreBoard.getPermission());
                }

                if (hasPerms) {
                    if (selected != null) {
                        if (selected.getPriority() < scoreBoard.getPriority()) {
                            selected = scoreBoard;
                        }
                    } else {
                        selected = scoreBoard;
                    }
                }
            }
        }

        return selected;
    }

    public enum ScoreBoardType {
        LOBBY, WAITING, INGAME, SPECTATOR
    }
}
