package cz.flay.battleground.player.scoreboard;

import cz.flay.battleground.storage.config.ConfigFile;
import org.bukkit.configuration.file.YamlConfiguration;
import cz.flay.battleground.player.scoreboard.ScoreBoardsManager.ScoreBoardType;

import java.util.ArrayList;
import java.util.List;

import static cz.flay.battleground.Logger.error;

public class ScoreBoard {

    private final String id;
    private final ConfigFile config;

    private int priority;
    private String permission;
    private ScoreBoardType type;

    public ScoreBoard(String id) {
        this.id = id;
        config = new ConfigFile("/scoreboards/", id);
        load();
    }

    public List<String> getScoreBoard(String locale) {
        if (config.getConfig().getConfigurationSection("locale." + locale) != null) {
            return config.getConfig().getStringList("locale." + locale);
        } else {
            try {
                return config.getConfig().getStringList("locale.default");
            } catch (NullPointerException ignored) {}
        }

        return new ArrayList<>();
    }

    private void load() {
        YamlConfiguration config = this.config.getConfig();

        try {
            priority = config.getInt("priority");
            permission = config.getString("permission");
            type = ScoreBoardType.valueOf(config.getString("type").toUpperCase());
        } catch (NullPointerException e) {
            error("Problems with loading scoreboard " + id + ", please check configuration");
            priority = 0;
        }
    }

    public int getPriority() {
        return priority;
    }

    public String getPermission() {
        return permission;
    }

    public ScoreBoardType getType() {
        return type;
    }
}
