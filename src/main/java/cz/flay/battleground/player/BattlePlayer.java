package cz.flay.battleground.player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import cz.flay.battleground.BattleGround;
import cz.flay.battleground.Logger;
import cz.flay.battleground.arena.Lobby;
import cz.flay.battleground.arena.radiation.RadiationEffect;
import cz.flay.battleground.events.player.GameJoinEvent;
import cz.flay.battleground.events.player.GameLeaveEvent;
import cz.flay.battleground.game.Game;
import cz.flay.battleground.items.GameItemsManager;
import cz.flay.battleground.player.inventory.InventoriesManager;
import cz.flay.battleground.player.inventory.Inventory;
import cz.flay.battleground.player.inventory.InventoryItem;
import cz.flay.battleground.player.scoreboard.ScoreBoard;
import cz.flay.battleground.player.scoreboard.ScoreBoardsManager;
import cz.flay.battleground.storage.locale.LocaleManager.MessageForm;
import cz.flay.battleground.util.Placeholders;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static cz.flay.battleground.Logger.warn;

public class BattlePlayer {

    private final Player player;
    private PlayerStatus status;

    private boolean openedParachute;
    private boolean hasBrokenLeg;
    private double radiation;
    private boolean adminStatus;

    private List<String> radiationEffects;
    private List<String> permanentEffects;

    private String locale;
    private MessageForm messageForm;

    private int scoreBoardSyncTask;

    private Game game;

    public BattlePlayer(Player player){
        this.player = player;
        reset();
    }

    public void reset() {
        onDeath();
        game = null;
        status = PlayerStatus.LOBBY;
        player.setGameMode(GameMode.SURVIVAL);
        player.setAllowFlight(false);

        applyInventory(((BattleGround) Bukkit.getPluginManager()
                .getPlugin("BattleGround")).getInventoriesManager()
                .getInventory(this, InventoriesManager.InventoryType.LOBBY));

        applyScoreBoard(((BattleGround) Bukkit.getPluginManager()
                .getPlugin("BattleGround")).getScoreBoardsManager()
                .getScoreBoard(this, ScoreBoardsManager.ScoreBoardType.LOBBY));
    }

    public void onDeath() {
        unfreeze();
        radiationEffects = new ArrayList<>();
        permanentEffects = new ArrayList<>();
        radiation = 0;
        hasBrokenLeg = false;
        openedParachute = false;
        removeRadiationEffects();
        player.setFoodLevel(20);

        for (PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }
    }

    public String getLanguage() {
        if (Bukkit.getServer().getPluginManager().isPluginEnabled("LanguageAPI")) {
            return ((cz.flay.languageapi.LanguageAPI)
                    Bukkit.getServer().getPluginManager().getPlugin("LanguageAPI"))
                        .getPlayerLang(getBukkitPlayer());
        } else {
            return locale;
        }
    }

    public MessageForm getMessageForm() {
        if (Bukkit.getServer().getPluginManager().isPluginEnabled("LanguageAPI")) {
            return MessageForm.valueOf(((cz.flay.languageapi.LanguageAPI)
                    Bukkit.getServer().getPluginManager().getPlugin("LanguageAPI"))
                        .getPlayerMessForm(getBukkitPlayer()).toString());
        } else {
            return messageForm;
        }
    }

    public void setLanguage(String locale) {
        if (Bukkit.getServer().getPluginManager().isPluginEnabled("LanguageAPI")) {
            ((cz.flay.languageapi.LanguageAPI)
                    Bukkit.getServer().getPluginManager().getPlugin("LanguageAPI"))
                        .setPlayerLang(getBukkitPlayer(), locale);
        } else {
            this.locale = locale;
        }
    }

    public void setMessageForm(MessageForm messageForm) {
        if (Bukkit.getServer().getPluginManager().isPluginEnabled("LanguageAPI")) {
            ((cz.flay.languageapi.LanguageAPI)
                    Bukkit.getServer().getPluginManager().getPlugin("LanguageAPI"))
                        .setPlayerMessForm(player,
                                cz.flay.languageapi.storage.locale.LocaleManager.MessageForm.valueOf(messageForm.toString()));
        } else {
            this.messageForm = messageForm;
        }
    }

    public Game getGame() {
        return game;
    }

    public boolean hasAdminStatus() {
        return adminStatus;
    }

    public void setAdminStatus() {
        adminStatus = true;
    }

    public void removeAdminStatus() {
        adminStatus = false;
    }

    public void setSpectator(){
        applyInventory(((BattleGround) Bukkit.getPluginManager()
                .getPlugin("BattleGround")).getInventoriesManager()
                .getInventory(this, InventoriesManager.InventoryType.SPECTATOR));

        applyScoreBoard(((BattleGround) Bukkit.getPluginManager()
                .getPlugin("BattleGround")).getScoreBoardsManager()
                .getScoreBoard(this, ScoreBoardsManager.ScoreBoardType.SPECTATOR));

        status = PlayerStatus.SPECTATOR;
        player.setGameMode(GameMode.SURVIVAL);
        player.setAllowFlight(true);
        game.setSpectator(this);

        removeRadiationEffects();
        fixLeg();
        openedParachute = false;
        radiation = 0;
        unfreeze();

        player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1, false, false));
    }

    public void joinGame(Game game) {
        if (this.game == null) {
            Bukkit.getServer().getPluginManager().callEvent(new GameJoinEvent(this));

            applyInventory(((BattleGround) Bukkit.getPluginManager()
                    .getPlugin("BattleGround")).getInventoriesManager()
                    .getInventory(this, InventoriesManager.InventoryType.WAITING));

            applyScoreBoard(((BattleGround) Bukkit.getPluginManager()
                    .getPlugin("BattleGround")).getScoreBoardsManager()
                    .getScoreBoard(this, ScoreBoardsManager.ScoreBoardType.WAITING));

            status = PlayerStatus.PLAYING;
            this.game = game;
            player.setGameMode(GameMode.SURVIVAL);
            player.setAllowFlight(false);
            game.addPlayer(this);
        }
    }

    public void onGameStart() {
        player.getInventory().clear();

        applyScoreBoard(((BattleGround) Bukkit.getPluginManager()
                .getPlugin("BattleGround")).getScoreBoardsManager()
                .getScoreBoard(this, ScoreBoardsManager.ScoreBoardType.INGAME));
    }

    public void addRadiation(double radiation) {
        this.radiation = Math.floor((this.radiation += radiation) * 100) / 100;
        checkRadiation();
    }

    public double getRadiation() {
        return radiation;
    }

    public void toLobby(Lobby lobby) {
        player.setGameMode(GameMode.SURVIVAL);
        lobby.teleportPlayer(this);

        applyInventory(((BattleGround) Bukkit.getPluginManager().getPlugin("BattleGround"))
                .getInventoriesManager().getInventory(this, InventoriesManager.InventoryType.LOBBY));
    }

    public void leaveGame(){
        Bukkit.getServer().getPluginManager().callEvent(new GameLeaveEvent(this));

        status = PlayerStatus.LOBBY;
        if (status.equals(PlayerStatus.PLAYING)){
            game.removePlayer(this);
        } else if (status.equals(PlayerStatus.SPECTATOR)) {
            game.removeSpectator(this);
            player.removePotionEffect(PotionEffectType.INVISIBILITY);
        }

        reset();
    }

    public void freeze() {
        player.setWalkSpeed(0.0f);
        player.setFlySpeed(0.0f);

        player.setAllowFlight(true);
        player.setFlying(true);
    }

    public boolean isFreezed() {
        return (player.getWalkSpeed() <= 0.1f && player.getFlySpeed() <= 0.1f);
    }

    private void applyInventory(Inventory inventory) {
        if (inventory == null) {
            return;
        }

        player.getInventory().clear();

        GameItemsManager itemsManager = ((BattleGround) Bukkit.getPluginManager().getPlugin("BattleGround"))
                                                .getItemsManager();

        HashMap<Integer, InventoryItem> inventoryItems = new HashMap<>();

        for (InventoryItem item : inventory.getInventoryItems()) {
            if (item.getPriority() == 0) {
                continue;
            }

            boolean hasPerms = true;
            if (item.getPermission() != null && !item.getPermission().equals("")) {
                hasPerms = player.hasPermission(item.getPermission());
            }

            if (hasPerms) {
                if (inventoryItems.containsKey(item.getSlot())) {
                    if (inventoryItems.get(item.getSlot()).getPriority() < item.getPriority()) {
                        inventoryItems.replace(item.getSlot(), item);
                    }
                } else {
                    inventoryItems.put(item.getSlot(), item);
                }
            }
        }

        for (InventoryItem item : inventoryItems.values()) {
            player.getInventory().setItem(item.getSlot(),
                    itemsManager.getControlItem(this, item.getItem()));
        }
    }

    private void applyScoreBoard(ScoreBoard scoreBoard) {
        if (scoreBoard == null) {
            return;
        }

        List<String> lines = scoreBoard.getScoreBoard(locale);
        Bukkit.getScheduler().cancelTask(scoreBoardSyncTask);
        BattleGround plugin = (BattleGround) Bukkit.getPluginManager().getPlugin("BattleGround");
        scoreBoardSyncTask = Bukkit.getScheduler().runTaskTimer(plugin, () -> {
            Placeholders placeholders = new Placeholders();
            if (scoreBoard.getType().equals(ScoreBoardsManager.ScoreBoardType.LOBBY)) {
                placeholders.add("%player_name%", player.getDisplayName());
            }

            if (scoreBoard.getType().equals(ScoreBoardsManager.ScoreBoardType.WAITING)) {
                placeholders.add("%player_name%", player.getDisplayName());
            }

            if (scoreBoard.getType().equals(ScoreBoardsManager.ScoreBoardType.INGAME)) {
                placeholders.add("%player_name%", player.getDisplayName());
                placeholders.add("%radiation%", radiation);
            }

            if (scoreBoard.getType().equals(ScoreBoardsManager.ScoreBoardType.SPECTATOR)) {
                placeholders.add("%player_name%", player.getDisplayName());
            }

            List<String> replaced = placeholders.applyPlaceholders(this, lines);
            Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
            Objective obj = scoreboard.registerNewObjective(replaced.get(0), "dummy");
            obj.setDisplayName(replaced.get(0));
            obj.setDisplaySlot(DisplaySlot.SIDEBAR);

            for (String line : replaced) {
                if (line.equals(replaced.get(0))) {
                    continue;
                }

                Score score = obj.getScore(line);
                score.setScore(0);
            }

            player.setScoreboard(scoreboard);
        }, 20, 20).getTaskId();
    }

    public void unfreeze() {
        if (hasBrokenLeg) {
            player.setWalkSpeed(0.1f);
        } else {
            player.setWalkSpeed(0.25f);
        }

        player.setAllowFlight(false);
        player.setFlying(false);
        player.setFlySpeed(0.1f);
    }

    public boolean hasBrokenLeg() {
        return hasBrokenLeg;
    }

    public void brokeLeg(){
        hasBrokenLeg = true;
        player.setWalkSpeed(0.1f);
    }

    public void fixLeg(){
        hasBrokenLeg = false;
        player.setWalkSpeed(0.25f);
    }

    private void checkRadiation() {
        for (RadiationEffect effect : game.getRadiationEffects()) {
            if (permanentEffects.contains(effect.getId())) {
                return;
            }

            if (radiationEffects.contains(effect.getId())) {
                if (!effect.isInRange(radiation)) {
                    if (!effect.cancel(this, false)) {
                        permanentEffects.add(effect.getId());
                    }
                }
                return;
            }

            if (effect.isInRange(radiation)) {
                if (effect.apply(this)) {
                    radiationEffects.add(effect.getId());
                }
            }
        }
    }

    private void removeRadiationEffects() {
        BattleGround plugin = (BattleGround) Bukkit.getPluginManager().getPlugin("BattleGround");
        if (permanentEffects.isEmpty() && radiationEffects.isEmpty() && game == null) {
            for (RadiationEffect effect : plugin.getGamesManager().getGame().getRadiationEffects()) {
                effect.cancel(this, true);
            }
        } else {
            for (RadiationEffect effect : plugin.getGamesManager().getGame().getRadiationEffects()) {
                if (permanentEffects.contains(effect.getId())) {
                    effect.cancel(this, true);
                }

                if (radiationEffects.contains(effect.getId())) {
                    effect.cancel(this, true);
                }
            }
        }
    }

    public boolean hasOpenedParachute() {
        return openedParachute;
    }

    public void openParachute() {
        openedParachute = true;
    }

    public boolean isPlaying() {
        return status.equals(PlayerStatus.PLAYING);
    }

    public boolean isSpectating() {
        return status.equals(PlayerStatus.SPECTATOR);
    }

    public Player getBukkitPlayer() {
        return player;
    }

    public void sendTitle(String title, int fadeIn, int stay, int fadeOut) {
        PacketContainer packet = new PacketContainer(PacketType.Play.Server.TITLE);
            packet.getTitleActions().write(0, EnumWrappers.TitleAction.TITLE);
            packet.getChatComponents().write(0, WrappedChatComponent.fromText(title));
            packet.getIntegers().write(0, fadeIn)
                .write(1, stay)
                .write(2, fadeOut);
        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(player, packet);
        } catch (Exception e) {
            warn("Problems with sending title packet !");
        }
    }

    public void sendSubTitle(String subtitle, int fadeIn, int stay, int fadeOut) {
        PacketContainer packet = new PacketContainer(PacketType.Play.Server.TITLE);
        packet.getTitleActions().write(0, EnumWrappers.TitleAction.SUBTITLE);
        packet.getChatComponents().write(0, WrappedChatComponent.fromText(subtitle));
        packet.getIntegers().write(0, fadeIn)
                .write(1, stay)
                .write(2, fadeOut);
        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(player, packet);
        } catch (Exception e) {
            warn("Problems with sending subtitle packet !");
        }
    }

    public void sendActionBar(String message) {
        PacketContainer packet = new PacketContainer(PacketType.Play.Server.CHAT);
            if (Bukkit.getServer().getClass().getPackage().getName().contains("1_8")) {
                packet.getBytes().write(0, (byte) 2);
            } else {
                packet.getChatTypes().write(0, EnumWrappers.ChatType.GAME_INFO);
            }
            packet.getChatComponents().write(0, WrappedChatComponent.fromText(message));

        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(player, packet);
        } catch (Exception e) {
            warn("Problems with sending action bar packet !");
        }
    }

    public void sendJsonChatMessage(String message) {
        PacketContainer packet = new PacketContainer(PacketType.Play.Server.CHAT);
        packet.getChatTypes().write(0, EnumWrappers.ChatType.CHAT);
        packet.getChatComponents().write(0, WrappedChatComponent.fromJson(message));

        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(player, packet);
        } catch (Exception e) {
            warn("Problems with sending json chat packet !");
        }
    }

    public void sendChatMessage(String message) {
        PacketContainer packet = new PacketContainer(PacketType.Play.Server.CHAT);
            packet.getChatTypes().write(0, EnumWrappers.ChatType.CHAT);
            packet.getChatComponents().write(0, WrappedChatComponent.fromText(message));

        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(player, packet);
        } catch (Exception e) {
            warn("Problems with sending chat packet !");
        }
    }

    public void sendBossBar(String message) {

    }

    private enum PlayerStatus {
        PLAYING, SPECTATOR, LOBBY
    }
}
