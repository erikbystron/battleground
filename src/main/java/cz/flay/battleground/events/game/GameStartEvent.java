package cz.flay.battleground.events.game;

import cz.flay.battleground.events.BattleEvent;
import cz.flay.battleground.game.Game;
import cz.flay.battleground.player.BattlePlayer;

import java.util.List;

public class GameStartEvent extends BattleEvent {

    private final Game game;

    public GameStartEvent(Game game) {
        this.game = game;
    }

    public List<BattlePlayer> getPlayers() {
        return game.getAllPlayers();
    }
}
