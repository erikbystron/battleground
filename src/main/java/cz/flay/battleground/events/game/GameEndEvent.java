package cz.flay.battleground.events.game;

import cz.flay.battleground.events.BattleEvent;
import cz.flay.battleground.game.Game;
import cz.flay.battleground.game.Winners;
import cz.flay.battleground.player.BattlePlayer;

import java.util.List;

public class GameEndEvent extends BattleEvent {

    private final List<BattlePlayer> players;
    private final Winners winners;

    public GameEndEvent(Game game, Winners winners) {
        players = game.getAllPlayers();
        this.winners = winners;
    }

    public List<BattlePlayer> getPlayers() {
        return players;
    }

    public Winners getWinners() {
        return winners;
    }
}
