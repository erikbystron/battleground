package cz.flay.battleground.events.player;

import cz.flay.battleground.events.BattleEvent;
import cz.flay.battleground.player.BattlePlayer;

public class GameJoinEvent extends BattleEvent {

    private final BattlePlayer player;

    public GameJoinEvent(BattlePlayer player) {
        this.player = player;
    }

    public BattlePlayer getPlayer() {
        return player;
    }
}
