package cz.flay.battleground.game;

import cz.flay.battleground.player.BattlePlayer;

public class Winners {

    private BattlePlayer first;
    private BattlePlayer second;
    private BattlePlayer third;

    Winners() {
        reset();
    }

    void setFirst(BattlePlayer first) {
        this.first = first;
    }

    void setSecond(BattlePlayer second) {
        this.second = second;
    }

    void setThird(BattlePlayer third) {
        this.third = third;
    }

    public BattlePlayer getFirst() {
        return first;
    }

    public BattlePlayer getSecond() {
        return second;
    }

    public BattlePlayer getThird() {
        return third;
    }

    void reset() {
        first = null;
        second = null;
        third = null;
    }
}
