package cz.flay.battleground.game;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.Logger;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.arena.radiation.RadiationEffect;
import cz.flay.battleground.arena.radiation.RadiationEffectsManager;
import cz.flay.battleground.broadcast.templates.GameCountDown;
import cz.flay.battleground.events.game.GameEndEvent;
import cz.flay.battleground.events.game.GameStartEvent;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.broadcast.BroadCaster;
import org.bukkit.Bukkit;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Game {

    private final BattleGround plugin;
    private final ArenasManager arenasManager;
    private final RadiationEffectsManager effectsManager;

    private Winners winners;
    private RadiationMultipliers radiationMultipliers;

    private String name;
    private List<Arena> arenas;
    private Arena arena;
    private GameState state;
    private GameType type;

    private final List<BattlePlayer> playerList;
    private final List<BattlePlayer> spectatorList;

    private final List<Material> placeableBlocks;
    private final List<Material> breakableBlocks;

    private int taskId;

    private int minPlayers;
    private int maxPlayers;
    private int countDown;

    Game(BattleGround plugin, ArenasManager arenasManager) {
        this.plugin = plugin;
        this.arenasManager = arenasManager;
        playerList = new ArrayList<>();
        spectatorList = new ArrayList<>();
        state = GameState.WAITING;
        effectsManager = new RadiationEffectsManager(plugin);
        winners = new Winners();
        radiationMultipliers = new RadiationMultipliers();

        placeableBlocks = new ArrayList<>();
        breakableBlocks = new ArrayList<>();
    }

    void startTimer() {
        if (state.equals(GameState.INGAME)) {
            return;
        }

        choseArena();
        final int[] countDown = {this.countDown};
        taskId = Bukkit.getScheduler().runTaskTimer(plugin, () -> {
            if (state.equals(GameState.INGAME)) {
                cancelTask();
                return;
            }

            if (playerList.size() >= minPlayers) {
                state = GameState.STARTING;
                    if (countDown[0] == 0){
                        state = GameState.INGAME;
                        startGame();
                        cancelTask();
                    } else {
                        BroadCaster.cast(new GameCountDown(playerList, countDown[0]));
                        countDown[0]--;
                    }
            } else {
                state = GameState.WAITING;
                if (playerList.isEmpty()) {
                    countDown[0] = this.countDown;
                }
            }
        }, 20, 20).getTaskId();
    }

    private void startGame() {
        Bukkit.getServer().getPluginManager().callEvent(new GameStartEvent(this));

        arena.getBorder().start();
        arena.getAirPlane().addPlayers(new ArrayList<>(playerList));
    }

    public Game setCountDown(int countDown) {
        this.countDown = countDown;
        return this;
    }

    private void endGame() {
        Bukkit.getServer().getPluginManager().callEvent(new GameEndEvent(this, winners));

        for (BattlePlayer player : getAllPlayers()) {
            player.leaveGame();
            player.toLobby(plugin.getArenasManager().getLobby());
        }

        state = GameState.END;
        arena.getBorder().stop();
        arenasManager.resetArena(arena);
        winners = new Winners();

        restartGame();
    }

    private void restartGame() {
        playerList.clear();
        spectatorList.clear();

        state = GameState.WAITING;
        startTimer();
    }

    public void setSpectator(BattlePlayer player){
        spectatorList.add(player);

        if (playerList.contains(player)){
            removePlayer(player);
        }
    }

    public List<BattlePlayer> getAllPlayers() {
        return Stream.concat(playerList.stream(), spectatorList.stream())
                .collect(Collectors.toList());
    }

    private void choseArena() {
        int i = ThreadLocalRandom.current().nextInt(arenas.size());
        arena = arenas.get(i);
        arena.initialize(this);
    }

    public List<BattlePlayer> getSpectators() {
        return spectatorList;
    }

    public void addPlayer(BattlePlayer player) {
        if (state.equals(GameState.WAITING) || state.equals(GameState.STARTING)) {
            playerList.add(player);
            player.toLobby(plugin.getArenasManager().getLobby());
        } else {
            spectatorList.add(player);
            player.setSpectator();
        }
    }

    public List<BattlePlayer> getPlayers() {
        return playerList;
    }

    public void removePlayer(BattlePlayer player) {
        if (playerList.contains(player)) {
            playerList.remove(player);
            if (playerList.size() < 3 && state.equals(GameState.INGAME)) {
                place(player);
            }
        }
        arena.getAirPlane().removePlayer(player);

        if ((playerList.isEmpty() || playerList.size() == 1) && state != GameState.END) {
            endGame();
        }
    }

    public void removeSpectator(BattlePlayer player) {spectatorList.remove(player);}

    public Game setGameType(GameType type){
        this.type = type;
        return  this;
    }

    public GameType getType() {
        return type;
    }

    public Arena getArena() {
        return arena;
    }

    public List<Arena> getArenas() {
        return arenas;
    }

    public Game setArena(Arena arena) {
        this.arena = arena;
        return this;
    }

    public Game setArenas(List<Arena> arenas) {
        this.arenas = arenas;
        return this;
    }

    public Game setType(GameType type) {
        this.type = type;
        return this;
    }

    public Game setName(String name) {
        this.name = name;
        return this;
    }

    public String getName() {
        return name;
    }

    public Game setMinPlayers(int minPlayers) {
        this.minPlayers = minPlayers;
        return this;
    }

    public RadiationMultipliers getRadiationMultipliers() {
        return radiationMultipliers;
    }

    public List<Material> getBreakableBlocks() {
        return breakableBlocks;
    }

    public List<Material> getPlaceableBlocks() {
        return placeableBlocks;
    }

    public Game setPlaceableBlocks(List<String> placeableBlocks) {
        for (String type : placeableBlocks) {
            try {
                this.placeableBlocks.add(Material.valueOf(type));
            } catch (IllegalArgumentException e) {
                plugin.getLogger().severe(type + " is not valid material name !");
            }
        }
        return this;
    }

    public Game setBreakableBlocks(List<String> breakableBlocks) {
        for (String type : breakableBlocks) {
            try {
                this.breakableBlocks.add(Material.valueOf(type));
            } catch (IllegalArgumentException e) {
                plugin.getLogger().severe(type + " is not valid material name !");
            }
        }
        return this;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public Game setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
        return this;
    }

    private int place(BattlePlayer player) {
        if (playerList.isEmpty()) {
            winners.setFirst(player);
            return 1;
        }

        if (playerList.size() == 1) {
            winners.setSecond(player);
            return 2;
        }

        if (playerList.size() == 2) {
            winners.setThird(player);
            return 3;
        }

        return playerList.size() + 1;
    }

    public List<RadiationEffect> getRadiationEffects() {
        return effectsManager.getEffects();
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public boolean isInGame(){
        return state.equals(GameState.INGAME);
    }

    public boolean isStarting() {
        return state.equals(GameState.STARTING);
    }

    public boolean isWaiting() {
        return state.equals(GameState.WAITING);
    }

    public boolean isEnding() {
        return state.equals(GameState.END);
    }

    public enum GameState {
        STARTING, INGAME, END, WAITING
    }

    public enum GameType {
        SOLO, DUO, QUATRO
    }

    private void cancelTask(){
        Bukkit.getScheduler().cancelTask(taskId);
    }
}
