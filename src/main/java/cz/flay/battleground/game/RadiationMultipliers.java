package cz.flay.battleground.game;

public class RadiationMultipliers {

    private double zoneRadiation;
    private double borderRadiation;
    private double decreaseRadiation;

    RadiationMultipliers() {
        zoneRadiation = 1;
        borderRadiation = 1;
        decreaseRadiation = 1;
    }

    public RadiationMultipliers setDecreaseRadiation(double decreaseRadiation) {
        this.decreaseRadiation = decreaseRadiation;
        return this;
    }

    public RadiationMultipliers setZoneRadiation(double zoneRadiation) {
        this.zoneRadiation = zoneRadiation;
        return this;
    }

    public RadiationMultipliers setBorderRadiation(double borderRadiation) {
        this.borderRadiation = borderRadiation;
        return this;
    }

    public double getDecreaseRadiation() {
        return decreaseRadiation;
    }

    public double getZoneRadiation() {
        return zoneRadiation;
    }

    public double getBorderRadiation() {
        return borderRadiation;
    }
}
