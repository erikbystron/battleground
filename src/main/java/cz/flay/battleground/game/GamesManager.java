package cz.flay.battleground.game;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.ArenasManager;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;

public class GamesManager {

    private final BattleGround plugin;
    private final ArenasManager arenasManager;

    private Game game;

    public GamesManager(BattleGround plugin) {
        this.plugin = plugin;
        arenasManager = plugin.getArenasManager();
        loadGames();
    }

    private void loadGames(){

        FileConfiguration config = plugin.getConfigManager().getGameConfig();

        String section = "game.";
        game  = new Game(plugin, arenasManager);
        List<Arena> arenas = new ArrayList<>();
        for (String id : config.getStringList(section + "arenas")) {
            Arena arena = arenasManager.getArena(id);
            if (arena != null) {
                arenas.add(arena);
            }
        }
        game.setName(config.getString(section + "name"))
                .setArenas(arenas)
                .setGameType(Game.GameType.SOLO)
                .setMinPlayers(config.getInt(section + "min-players"))
                .setMaxPlayers(config.getInt(section + "max-players"))
                .setCountDown(config.getInt(section + "countdown"))
                .setPlaceableBlocks(config.getStringList(section + "breakable-blocks"))
                .setBreakableBlocks(config.getStringList(section + "placeable-blocks"));
        game.getRadiationMultipliers()
                .setDecreaseRadiation(config.getDouble(section + "radiation-multipliers.decrease"))
                .setZoneRadiation(config.getDouble(section + "radiation-multipliers.zone"))
                .setBorderRadiation(config.getDouble(section + "radiation-multipliers.border"));
        game.startTimer();
    }

    public Game getGame(){ return game; }
}
