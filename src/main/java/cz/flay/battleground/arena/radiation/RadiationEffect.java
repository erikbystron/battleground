package cz.flay.battleground.arena.radiation;

import cz.flay.battleground.player.BattlePlayer;
import org.apache.commons.lang3.Range;

import java.util.concurrent.ThreadLocalRandom;

public class RadiationEffect {

    private final String id;
    protected Range radiationRange;
    protected final ThreadLocalRandom random;

    protected int applyChance;
    protected int permanentChance;

    protected RadiationEffect(String id) {
        this.id = id;
        random = ThreadLocalRandom.current();
    }

    public boolean isInRange(double radiation) {
        return radiationRange.contains(radiation);
    }

    public boolean apply(BattlePlayer player) {
        return random.nextInt(0, 100) <= applyChance;

    }

    public boolean cancel(BattlePlayer player, boolean force) {
        if (!force) {
            return random.nextInt(0, 100) >= permanentChance;
        }

        return true;
    }

    public String getId() {
        return id;
    }

    public void setRadiationRange(double minimum, double maximum) {
        this.radiationRange = Range.between(minimum, maximum);
    }

    protected void load() {

    }
}
