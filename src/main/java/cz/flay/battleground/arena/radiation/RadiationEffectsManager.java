package cz.flay.battleground.arena.radiation;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.radiation.effects.PotionEffect;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.ArrayList;
import java.util.List;

public class RadiationEffectsManager {

    private final BattleGround plugin;
    private final List<RadiationEffect> effects;

    public RadiationEffectsManager(BattleGround plugin) {
        this.plugin = plugin;
        effects = new ArrayList<>();
        loadEffects();
    }

    private void loadEffects() {
        YamlConfiguration config = plugin.getConfigManager().getRadiationConfig();

        for (String section : config.getConfigurationSection("radiation-effects").getKeys(false)) {
            switch (Effect.valueOf(((String) config.get("radiation-effects." + section + ".type")).toUpperCase())) {
                case POTION:
                    effects.add(new PotionEffect(section));
            }
        }
    }

    public List<RadiationEffect> getEffects() {
        return effects;
    }

    public enum Effect {
        POTION
    }
}
