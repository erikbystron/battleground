package cz.flay.battleground.arena.radiation.effects;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.radiation.RadiationEffect;
import cz.flay.battleground.player.BattlePlayer;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.potion.PotionEffectType;

import static cz.flay.battleground.Logger.error;

public class PotionEffect extends RadiationEffect {

    private org.bukkit.potion.PotionEffect potion;

    public PotionEffect(String id) {
        super(id);
        load();
    }

    @Override
    public boolean apply(BattlePlayer player) {
        boolean apply = super.apply(player);
        if (apply) {
            player.getBukkitPlayer().addPotionEffect(potion);
        }

        return apply;
    }

    @Override
    public boolean cancel(BattlePlayer player, boolean force) {
        boolean cancel = super.cancel(player, force);
        if (cancel) {
            player.getBukkitPlayer().removePotionEffect(potion.getType());
        }

        return cancel;
    }

    @Override
    protected void load() {
        BattleGround plugin = (BattleGround) Bukkit.getPluginManager().getPlugin("BattleGround");
        YamlConfiguration config = plugin.getConfigManager().getRadiationConfig();
        String path = "radiation-effects." + getId() + ".";

        try {
            applyChance = config.getInt(path + "apply-chance");
            permanentChance = config.getInt(path + "permanent-chance");

            double maximum = config.getDouble(path + "radiation-range.min");
            double minimum = config.getDouble(path + "radiation-range.max");
            setRadiationRange(minimum, maximum);

            PotionEffectType potionEffectType = PotionEffectType.getByName(config.getString(path + "potion.type"));
            int amplifier = config.getInt(path + "potion.amplifier");
            boolean ambient = config.getBoolean(path + "potion.ambient");
            boolean particles = config.getBoolean(path + "potion.particles");

            potion = new org.bukkit.potion.PotionEffect(potionEffectType, Integer.MAX_VALUE, amplifier, ambient, particles);
        } catch (NullPointerException e) {
            error("Problems with loading radiation effect " + getId() + ", please check configuration");
        }
    }
}
