package cz.flay.battleground.arena.drops.types;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;

public class ChestDrop {

    private final Chest chest;

    public ChestDrop(Location location) {
        if (!location.getBlock().getType().equals(Material.CHEST)) {
            location.getBlock().setType(Material.CHEST);
        }

        chest = (Chest) location.getBlock();
    }

    public void reFill() {
        chest.getBlockInventory().clear();


    }

}
