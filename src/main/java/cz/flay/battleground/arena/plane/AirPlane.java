package cz.flay.battleground.arena.plane;

import com.google.common.collect.Lists;
import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.broadcast.BroadCaster;
import cz.flay.battleground.broadcast.templates.GamePlaneTeleporting;
import cz.flay.battleground.items.GameItemsManager.Item;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.config.airplane.AirPlaneDAO;
import cz.flay.battleground.util.RandomPicker;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.map.MapView;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.*;

public class AirPlane {

    private final BattleGround plugin;
    private final RandomPicker random;
    private final AirPlaneDAO planeDAO;
    private final Arena arena;

    private final HashMap<BattlePlayer, Location> playerList;
    private final List<BattlePlayer> ejectedList;
    private List<Location> positions;
    private List<PlaneRoute> routes;

    private Location planeLoc;
    private Vector firstLocation;
    private double noFallZone;
    private int taskID;

    private int height;
    private int speed;
    private int tptime;

    private int autoOpenHeight;
    private MapView.Scale mapsScale = MapView.Scale.CLOSEST;

    public AirPlane(BattleGround plugin, Arena arena) {
        this.plugin = plugin;
        this.arena = arena;
        random = new RandomPicker();
        playerList = new HashMap<>();
        positions = new ArrayList<>();
        ejectedList = new ArrayList<>();
        routes = new ArrayList<>();
        planeLoc = null;
        planeDAO = plugin.getStorageManager().getPlaneDAO();
        load();
    }

    private void load() {
        try {
            String id = arena.getId();
            positions = planeDAO.getPositions(id);
            routes = planeDAO.getRoutes(id);

            height = planeDAO.getHeight(id);
            speed = planeDAO.getSpeed(id);
            if (speed > 20) {
                speed = 20;
            } else if (speed < 1) {
                speed = 1;
            }
            tptime = planeDAO.getTpTime(id);
            autoOpenHeight = planeDAO.getAutoOpen(id);
        } catch (NullPointerException ignored) { }
    }

    public void addPlayers(List<BattlePlayer> players) {
        BroadCaster.cast(new GamePlaneTeleporting(players));

        for (BattlePlayer player : players) {
            player.freeze();
        }

        List<Location> emptyPositions = new ArrayList<>(positions);

        taskID = Bukkit.getScheduler().runTaskTimer(plugin, () -> {
            if (emptyPositions.isEmpty() || players.isEmpty()) {
                cancelTask();
                start();
                return;
            }

            BattlePlayer player = players.get(0);
            Location location = emptyPositions.get(0);
                location.setY(location.getY() + 0.02);
                playerList.put(player, location);
                player.getBukkitPlayer().teleport(location, TeleportCause.PLUGIN);
                player.onGameStart();
            emptyPositions.remove(0);
            players.remove(0);
        }, 20, 5).getTaskId();
    }

    private void start() {
        PlaneRoute route = routes.get(random.getRandom(routes));
        Iterator<Block> finalLine = route.getRoute(arena.getWorld());

        noFallZone = Math.pow(route.getNoFallZone(), 2);

        firstLocation = new Vector(0,0,0);
        if (finalLine.hasNext()) {
            firstLocation = finalLine.next().getLocation().toVector();
        }

        List<MapView> views = new ArrayList<>();

        if (firstLocation != null && finalLine.hasNext()) {
            List<Block> line = Lists.newArrayList(route.getRoute(arena.getWorld()));

            int maps = line.size() / ((mapsScale.getValue() + 1) * 128);

            if (line.size() > (maps * (mapsScale.getValue() + 1) * 128)) {
                maps += 1;
            }

            for (int i = 0; i <= maps; i++) {
                int index = i * ((mapsScale.getValue() + 1) * 128);

                if (line.size() - 1 < index) {
                    index = line.size() - 1;
                }

                Block block = line.get(index);

                MapView view = Bukkit.createMap(arena.getWorld());
                    view.setCenterX(block.getLocation().getBlockX());
                    view.setCenterZ(block.getLocation().getBlockZ());
                    view.setScale(mapsScale);
                views.add(view);
            }

        } else if (firstLocation != null) {
            MapView view = Bukkit.createMap(arena.getWorld());
                view.setCenterX(firstLocation.getBlockX());
                view.setCenterZ(firstLocation.getBlockZ());
                view.setScale(mapsScale);
            views.add(view);
        }

        final int[] blocks = {0};
        final int[] mapIndex = {0};

        ItemStack map = new ItemStack(Material.MAP);

        for (BattlePlayer player : playerList.keySet()) {
            if (views.size() > 0) {
                map.setDurability(views.get(0).getId());
                player.getBukkitPlayer().getInventory().setItem(2, map);
            }

            if (views.size() > 1) {
                map.setDurability(views.get(1).getId());
                player.getBukkitPlayer().getInventory().setItem(3, map);
            }

            if (views.size() > 2) {
                map.setDurability(views.get(2).getId());
                player.getBukkitPlayer().getInventory().setItem(4, map);
            }

            if (views.size() > 3) {
                map.setDurability(views.get(3).getId());
                player.getBukkitPlayer().getInventory().setItem(5, map);
            }

            if (views.size() > 4) {
                map.setDurability(views.get(4).getId());
                player.getBukkitPlayer().getInventory().setItem(6, map);
            }

            if (views.size() > 5) {
                map.setDurability(views.get(5).getId());
                player.getBukkitPlayer().getInventory().setItem(7, map);
            }

            if (views.size() > 6) {
                map.setDurability(views.get(6).getId());
                player.getBukkitPlayer().getInventory().setItem(8, map);
            }
        }

        taskID = new BukkitRunnable() {
            @Override
            public void run() {
                Location location = null;

                if (blocks[0] > (mapsScale.getValue() + 1) * 128) {
                    blocks[0] = 0;
                    mapIndex[0]++;

                    for (BattlePlayer player : playerList.keySet()) {
                        map.setDurability(views.get(mapIndex[0] - 3).getId());
                        player.getBukkitPlayer().getInventory().setItem(2, map);

                        map.setDurability(views.get(mapIndex[0] - 2).getId());
                        player.getBukkitPlayer().getInventory().setItem(3, map);

                        map.setDurability(views.get(mapIndex[0] - 1).getId());
                        player.getBukkitPlayer().getInventory().setItem(4, map);

                        map.setDurability(views.get(mapIndex[0]).getId());
                        player.getBukkitPlayer().getInventory().setItem(5, map);

                        map.setDurability(views.get(mapIndex[0] + 1).getId());
                        player.getBukkitPlayer().getInventory().setItem(6, map);

                        map.setDurability(views.get(mapIndex[0] + 2).getId());
                        player.getBukkitPlayer().getInventory().setItem(7, map);

                        map.setDurability(views.get(mapIndex[0] + 3).getId());
                        player.getBukkitPlayer().getInventory().setItem(8, map);
                    }
                }

                if (finalLine.hasNext()) {
                    location = finalLine.next().getLocation();
                    blocks[0]++;
                } else {
                    for (BattlePlayer player : playerList.keySet()) {
                        ejectPlayer(player);
                    }
                    cancelTask();
                }

                if (location != null) {
                    planeLoc = location;
                }

            }
        }.runTaskTimer(plugin, 0, (20 / speed)).getTaskId();
    }

    private void cancelTask() {
        Bukkit.getServer().getScheduler().cancelTask(taskID);
    }

    public void ejectPlayer(BattlePlayer player) {

        if (planeLoc == null) {
            return;
        }

        if (planeLoc.toVector().distanceSquared(firstLocation) >= noFallZone) {
            new BukkitRunnable() {

                @Override
                public void run() {
                    Location location = player.getBukkitPlayer().getLocation();
                    location.setY(player.getBukkitPlayer().getLocation().getY() - 2);

                    player.unfreeze();
                    player.getBukkitPlayer().teleport(location, TeleportCause.PLUGIN);
                    player.getBukkitPlayer().getInventory().setItem(4, plugin.getItemsManager().getControlItem(player, Item.PARACHUTE));

                    Location finalLocation = planeLoc;
                        finalLocation.setY(height);
                    new BukkitRunnable(){

                        @Override
                        public void run() {
                            player.getBukkitPlayer().teleport(finalLocation, TeleportCause.PLUGIN);
                            playerList.remove(player);
                            ejectedList.add(player);
                        }

                    }.runTaskLater(plugin, (tptime));
                }
            }.runTask(plugin);
        }
    }

    public void removePlayer(BattlePlayer player) {
        if (isPlayerIn(player)) {
            playerList.remove(player);
        }

        if (isPlayerFalling(player)) {
            ejectedList.remove(player);
        }

        if (playerList.size() == 0) {
            cancelTask();
        }
    }

    public void removePosition(Location location) {
        if (positions.contains(location)) {
            positions.remove(location);
        }
    }

    public void addPosition(Location location) {
        positions.add(location);
    }

    public List<Location> getPositions() {
        return positions;
    }

    public boolean isPlayerIn(BattlePlayer player) {
        return playerList.containsKey(player);
    }

    public Location getPlayerLoc(BattlePlayer player) {
        return playerList.get(player);
    }

    public boolean isPlayerFalling(BattlePlayer player) {
        return ejectedList.contains(player);
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getAutoOpenHeight() {
        return autoOpenHeight;
    }

    public void setAutoOpenHeight(int autoOpenHeight) {
        this.autoOpenHeight = autoOpenHeight;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getTptime() {
        return tptime;
    }

    public void setTptime(int tptime) {
        this.tptime = tptime;
    }

    public List<PlaneRoute> getRoutes() {
        return routes;
    }

    public void addRoute(PlaneRoute route) {
        routes.add(route);
    }
}
