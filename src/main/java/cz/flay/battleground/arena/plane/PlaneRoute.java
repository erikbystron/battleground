package cz.flay.battleground.arena.plane;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.util.Randomable;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static cz.flay.battleground.Logger.error;

@SerializableAs("PlaneRoute")
public class PlaneRoute extends Randomable implements ConfigurationSerializable{

    private String name;
    private final Vector firstPoint;
    private final Vector secondPoint;

    private int noFallZone;
    private int fallZone;

    public PlaneRoute(String name, Vector firstPoint, Vector secondPoint, int probability, int noFallZone, int fallZone) {
        this.name = name;
        this.firstPoint = firstPoint;
        this.secondPoint = secondPoint;
        this.probability = probability;
        this.fallZone = fallZone;
        this.noFallZone = noFallZone;
    }

    public PlaneRoute() {
        firstPoint = new Vector(0,0,0);
        secondPoint = new Vector(0,0,0);
        probability = 0;
        noFallZone = 0;
        fallZone = 0;
    }

    public Iterator<Block> getRoute(World world) {
        validatePoints();
        try {
            return new BlockIterator(world, firstPoint, secondPoint, 0.1, (noFallZone + fallZone));
        } catch (NullPointerException e) {
            error("Problems with creating plane route !");
            return new ArrayList<Block>().iterator();
        }
    }

    public String getName() {
        return name;
    }

    public int getNoFallZone() {
        return noFallZone;
    }

    public int getFallZone() {
        return fallZone;
    }

    private void validatePoints() {
        if (firstPoint.getX() == 0) {
            firstPoint.setX(1);
        }

        if (firstPoint.getZ() == 0) {
            firstPoint.setZ(1);
        }

        if (secondPoint.getX() == 0) {
            secondPoint.setX(1);
        }

        if (secondPoint.getZ() == 0) {
            secondPoint.setZ(1);
        }

        if (firstPoint.length() > secondPoint.length()) {
            Vector first = firstPoint;
            Vector second = secondPoint;

            firstPoint.setX(second.getX());
            firstPoint.setZ(second.getZ());

            secondPoint.setX(first.getX());
            secondPoint.setZ(first.getZ());
        }
    }

    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();
            data.put("name", name);
            data.put("firstX", firstPoint.getX());
            data.put("firstZ", firstPoint.getZ());
            data.put("secondX", secondPoint.getX());
            data.put("secondZ", secondPoint.getZ());
            data.put("probability", probability);
            data.put("noFallZone", noFallZone);
            data.put("fallZone", fallZone);
        return data;
    }

    public static PlaneRoute deserialize(Map<String, Object> args) {
        BattleGround plugin = (BattleGround) Bukkit.getServer().getPluginManager().getPlugin("BattleGround");

        String name = "Unspecified";
        try {
            if (args.get("name") != null) {
                name = (String) args.get("name");
            } else {
                throw  new NullPointerException();
            }
        } catch (NullPointerException e) {
            error("Missing name value for plane route " + name + " !");
        }

        Vector firstPoint = new Vector(0,0,0);
        try {
            firstPoint.setX((double) args.get("firstX"));
            firstPoint.setZ((double) args.get("firstZ"));
        } catch (NullPointerException e) {
            error("Missing first point values for plane route " + name + " !");
        }

        Vector secondPoint = new Vector(0,0,0);
        try {
            secondPoint.setX((double) args.get("secondX"));
            secondPoint.setZ((double) args.get("secondZ"));
        } catch (NullPointerException e) {
            error("Missing second point values for plane route " + name + " !");
        }

        int probability = 0;
        try {
            probability = (int) args.get("probability");
        } catch (NullPointerException e) {
            error("Missing probability value for plane route " + name + " !");
        }

        int noFallZone = 0;
        try {
            noFallZone = (int) args.get("noFallZone");
        } catch (NullPointerException e) {
            error("Missing noFallZone value for plane route " + name + " !");
        }

        int fallZone = 0;
        try {
            fallZone = (int) args.get("fallZone");
        } catch (NullPointerException e) {
            error("Missing fallZone value for plane route " + name + " !");
        }

        return new PlaneRoute(name, firstPoint, secondPoint, probability, noFallZone, fallZone);
    }
}
