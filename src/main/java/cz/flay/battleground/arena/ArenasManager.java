package cz.flay.battleground.arena;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.reseting.MapResetor;
import cz.flay.battleground.storage.config.airplane.AirPlaneDAO;
import cz.flay.battleground.storage.config.arena.ArenaDAO;

import java.util.ArrayList;
import java.util.List;

public class ArenasManager {

    private final BattleGround plugin;
    private final ArenaDAO arenaDAO;
    private final AirPlaneDAO planeDAO;
    private final MapResetor mapResetor;
    private final Lobby lobby;

    private final List<Arena> arenas;

    public ArenasManager(BattleGround plugin) {
        this.plugin = plugin;
        mapResetor = new MapResetor(plugin);
        lobby = new Lobby(plugin);
        arenas = new ArrayList<>();
        arenaDAO = plugin.getStorageManager().getArenaDAO();
        planeDAO = plugin.getStorageManager().getPlaneDAO();
        loadArenas();
    }

    private void loadArenas() {
        for (String id : plugin.getConfigManager().getConfig().getStringList("arenas")) {
            Arena arena = new Arena(plugin, id);

            arena.setName(arenaDAO.getName(id))
                    .setShape(arenaDAO.getShape(id))
                    .setBorderType(arenaDAO.getBorderType(id))
                    .setWorld(arenaDAO.getWorld(id))
                    .setCenter(arenaDAO.getCenter(id))
                    .setUpBorder();
            arena.getBorder().load(arenaDAO);
            setUpArena(arena);
        }
    }

    public Arena createArena(String name) {
        return new Arena(plugin, name.toLowerCase()).setName(name);
    }

    public void setUpArena(Arena arena) {
        arenas.add(arena);
    }

    public void saveAll() {
        for (Arena arena : arenas) {
            saveArena(arena);
        }
    }

    private void saveArena(Arena arena) {
        arenaDAO.saveArena(arena);
        planeDAO.saveAirPlane(arena.getId(), arena.getAirPlane());
    }

    public Arena getArena(String id) {
        for (Arena arena : arenas) {
            if (arena.getId().equals(id.toLowerCase())) {
                return arena;
            }
        }

        return null;
    }

    public boolean isNameUsed(String name) {
        for (Arena arena : arenas) {
            if (arena.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }

        return false;
    }

    public void deleteArena(Arena arena) {
        arenas.remove(arena);
    }

    public MapResetor getMapResetor() {
        return mapResetor;
    }

    public Lobby getLobby() {
        return lobby;
    }

    public void resetArena(Arena arena) {
        mapResetor.resetArena(arena);
        plugin.getCorpsesManager().despawnCorpses(arena);
    }

    public List<Arena> getArenas() {
        return arenas;
    }
}
