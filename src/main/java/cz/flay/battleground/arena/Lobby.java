package cz.flay.battleground.arena;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.player.BattlePlayer;
import org.bukkit.Location;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

public class Lobby {

    private final Location location;

    Lobby(BattleGround plugin){
        location = plugin.getConfigManager().getLocation("lobby-settings");
    }

    public void teleportPlayer(BattlePlayer player) {
        player.getBukkitPlayer().teleport(location, TeleportCause.PLUGIN);
    }

    public Location getLocation() {
        return location;
    }
}
