package cz.flay.battleground.arena;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.border.Border;
import cz.flay.battleground.arena.border.types.radiation.CircleRadiationBorder;
import cz.flay.battleground.arena.plane.AirPlane;
import cz.flay.battleground.game.Game;
import org.bukkit.World;
import org.bukkit.util.Vector;

public class Arena {

    private Game game;
    private Border border;
    private AirPlane airPlane;

    private final String id;
    private String name;
    private Shape shape;
    private BorderType borderType;
    private World world;
    private Vector center;

    public Arena(BattleGround plugin, String id) {
        this.id = id;
        airPlane = new AirPlane(plugin, this);
    }

    public Arena initialize(Game game) {
        this.game = game;
        return this;
    }

    public Game getGame(){
        return game;
    }

    public Arena setName(String name){
        this.name = name;
        return this;
    }

    public String getName(){
        return name;
    }

    public Arena setShape(Shape shape){
        this.shape = shape;
        return this;
    }

    public Shape getShape() {
        return shape;
    }

    public Arena setWorld(World world) {
        this.world = world;
        return this;
    }

    public World getWorld() {
        return world;
    }

    public AirPlane getAirPlane() {
        return airPlane;
    }

    public Arena setAirPlane(AirPlane airPlane) {
        this.airPlane = airPlane;
        return this;
    }

    public Border getBorder() {
        return border;
    }

    public Arena setUpBorder() {
        if (borderType.equals(BorderType.RADIATION)) {
            if (shape.equals(Shape.CIRCLE)) {
                border = new CircleRadiationBorder(this);
            }

            if (shape.equals(Shape.RECTANGULAR)) {

            }

            if (shape.equals(Shape.SQUARE)) {

            }

            return this;
        }

        if (borderType.equals(BorderType.COMBINED)) {
            return this;
        }

        if (borderType.equals(BorderType.VANILLA)) {
            return this;
        }

        return this;
    }

    public String getId() {
        return id;
    }

    public Arena setCenter(Vector center) {
        this.center = center;
        return this;
    }

    public Vector getCenter() {
        return center;
    }

    public BorderType getBorderType() {
        return borderType;
    }

    public Arena setBorderType(BorderType borderType) {
        this.borderType = borderType;
        return this;
    }

    public enum Shape {
        CIRCLE, RECTANGULAR, SQUARE
    }

    public enum BorderType {
        VANILLA, RADIATION, COMBINED
    }

}
