package cz.flay.battleground.arena.border;

import cz.flay.battleground.BattleGround;
import org.bukkit.Bukkit;

public class SafeZone {

    protected final BattleGround plugin;
    protected Border border;
    protected boolean applied = false;

    public SafeZone() {
        plugin = (BattleGround) Bukkit.getPluginManager().getPlugin("BattleGround");
    }

    public void apply() {

    }

    public boolean isApplied() {
        return applied;
    }
}
