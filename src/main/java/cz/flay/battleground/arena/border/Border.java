package cz.flay.battleground.arena.border;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.storage.config.arena.ArenaDAO;
import org.bukkit.Bukkit;

public class Border {

    protected final BattleGround plugin;
    protected final Arena arena;

    protected Border(Arena arena) {
        plugin = (BattleGround) Bukkit.getPluginManager().getPlugin("BattleGround");
        this.arena = arena;
    }

    public void load(ArenaDAO arenaDAO) {

    }

    public void start() {

    }

    public void stop() {

    }

    public Arena getArena() {
        return arena;
    }
}
