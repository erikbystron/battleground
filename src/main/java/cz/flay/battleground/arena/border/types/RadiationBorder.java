package cz.flay.battleground.arena.border.types;

import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.Arena.Shape;
import cz.flay.battleground.arena.border.Border;
import cz.flay.battleground.arena.border.types.radiation.RadiationZone;
import cz.flay.battleground.arena.border.types.radiation.zones.CircleRadiationZone;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.config.arena.ArenaDAO;
import org.bukkit.Bukkit;

import java.util.concurrent.ThreadLocalRandom;

public class RadiationBorder extends Border {

    private int taskID;
    protected RadiationZone safeZone;

    private double zoneRadiation;
    private double borderRadiation;
    private double decreaseRadiation;

    public RadiationBorder(Arena arena) {
        super(arena);
    }

    @Override
    public void start() {
        taskID = Bukkit.getScheduler().runTaskTimer(plugin, () -> {
            for (BattlePlayer player : arena.getGame().getAllPlayers()) {
                if (player.hasAdminStatus()) {
                    continue;
                }

                CHECK_RESULT result = checkPlayer(player);

                if (player.isSpectating()) {
                    if (result.equals(CHECK_RESULT.IN_ZONE) || result.equals(CHECK_RESULT.OUT_ZONE)) {
                        return;
                    }

                    if (result.equals(CHECK_RESULT.OUT_BORDER)) {
                        try {
                            player.getBukkitPlayer().teleport(
                                    player.getGame().getPlayers()
                                            .get(ThreadLocalRandom.current()
                                                    .nextInt(0, player.getGame().getPlayers().size() - 1))
                                            .getBukkitPlayer().getLocation()
                            );
                        } catch (IllegalArgumentException e) {
                            player.getBukkitPlayer().teleport(arena.getCenter().toLocation(arena.getWorld()));
                        }
                        return;
                    }
                }

                if (player.isPlaying()) {
                    if (result.equals(CHECK_RESULT.IN_ZONE)) {
                        if (player.getRadiation() > decreaseRadiation) {
                            player.addRadiation(-decreaseRadiation);
                        }
                        return;
                    }

                    if (result.equals(CHECK_RESULT.OUT_ZONE)) {
                        player.addRadiation(zoneRadiation);
                        return;
                    }

                    if (result.equals(CHECK_RESULT.OUT_BORDER)) {
                        player.addRadiation(borderRadiation);
                        return;
                    }
                }
            }
        }, 0, 20).getTaskId();
    }

    @Override
    public void stop() {
        Bukkit.getScheduler().cancelTask(taskID);
    }

    @Override
    public void load(ArenaDAO arenaDAO) {
        String id = arena.getId();

        decreaseRadiation = arenaDAO.getRadiationDecrease(id);
        zoneRadiation = arenaDAO.getZoneRadiation(id);
        borderRadiation = arenaDAO.getBorderRadiation(id);

        switch (arenaDAO.getShape(id)) {
            case CIRCLE:
                safeZone = new CircleRadiationZone(this);
                ((CircleRadiationZone) safeZone).setStates(arenaDAO.getZoneStates(id));
                break;
            case SQUARE:
                break;
            case RECTANGULAR:
                break;
        }
    }

    protected CHECK_RESULT checkPlayer(BattlePlayer player) {
        return CHECK_RESULT.IN_ZONE;
    }

    protected enum CHECK_RESULT {
        OUT_BORDER, OUT_ZONE, IN_ZONE
    }

    public double getDecreaseRadiation() {
        return decreaseRadiation;
    }

    public double getZoneRadiation() {
        return zoneRadiation;
    }

    public double getBorderRadiation() {
        return borderRadiation;
    }

    public RadiationBorder setDecreaseRadiation(double decreaseRadiation) {
        this.decreaseRadiation = decreaseRadiation;
        return this;
    }

    public RadiationBorder setZoneRadiation(double zoneRadiation) {
        this.zoneRadiation = zoneRadiation;
        return this;
    }

    public RadiationBorder setBorderRadiation(double borderRadiation) {
        this.borderRadiation = borderRadiation;
        return this;
    }
}
