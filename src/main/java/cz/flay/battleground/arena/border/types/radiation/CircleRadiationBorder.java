package cz.flay.battleground.arena.border.types.radiation;

import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.border.types.RadiationBorder;
import cz.flay.battleground.arena.border.types.radiation.zones.CircleRadiationZone;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.config.arena.ArenaDAO;
import org.bukkit.util.Vector;

public class CircleRadiationBorder extends RadiationBorder {

    private Vector borderCenter;
    private double borderRadius;
    private double cachedPowedBorderRadius;

    public CircleRadiationBorder(Arena arena) {
        super(arena);
    }

    @Override
    public void start() {
        cachedPowedBorderRadius = Math.pow(borderRadius, 2);
        super.start();
    }

    @Override
    protected CHECK_RESULT checkPlayer(BattlePlayer player) {
        Vector location = player.getBukkitPlayer().getLocation().toVector();
            location.setY(0);
            borderCenter.setY(0);

        if (safeZone.isApplied()) {
            if (safeZone.isPlayerIn(player)) {
                return CHECK_RESULT.IN_ZONE;
            }
        }

        if (cachedPowedBorderRadius > location.distanceSquared(borderCenter)) {
            if (!safeZone.isApplied()) {
                return CHECK_RESULT.IN_ZONE;
            }

            return CHECK_RESULT.OUT_ZONE;
        }

        return CHECK_RESULT.OUT_BORDER;
    }

    @Override
    public void load(ArenaDAO arenaDAO) {
        super.load(arenaDAO);
        String id = arena.getId();

        borderCenter = arenaDAO.getCenter(id);
        borderRadius = arenaDAO.getRadius(id);
    }

    public Vector getBorderCenter() {
        return borderCenter;
    }

    public double getBorderRadius() {
        return borderRadius;
    }

    public CircleRadiationBorder setBorderCenter(Vector borderCenter) {
        this.borderCenter = borderCenter;
        return this;
    }

    public CircleRadiationBorder setBorderRadius(double borderRadius) {
        this.borderRadius = borderRadius;
        return this;
    }
}
