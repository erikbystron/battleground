package cz.flay.battleground.arena.border.types.radiation.zones.states;

import cz.flay.battleground.arena.border.types.radiation.zones.ZoneState;
import org.bukkit.configuration.serialization.SerializableAs;

import java.util.Map;

import static cz.flay.battleground.Logger.error;

@SerializableAs("ZoneState-Circle")
public class CircleZoneState extends ZoneState {

    private double zoneRadius;

    public CircleZoneState(int stayTime, int shrinkingSpeed, double zoneRadius) {
        super(stayTime, shrinkingSpeed);
        this.zoneRadius = zoneRadius;
    }

    public CircleZoneState() {
        super();
        this.zoneRadius = 0;
    }

    public double getZoneRadius() {
        return zoneRadius;
    }

    public CircleZoneState setZoneRadius(int zoneRadius) {
        this.zoneRadius = zoneRadius;
        return this;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = super.serialize();
            data.put("radius", zoneRadius);
        return data;
    }

    public static CircleZoneState deserialize(Map<String, Object> args) {
        try {
            int stayTime = (int) args.get("stay-time");
            int shrinkingSpeed = (int) args.get("shrinking-speed");
            double zoneRadius = (double) args.get("radius");

            if (shrinkingSpeed > 20) {
                shrinkingSpeed = 20;
            }

            return new CircleZoneState(stayTime, shrinkingSpeed, zoneRadius);
        } catch (NullPointerException e) {
            error("Problems with loading some circle zone state ! Please check configuration");
            return new CircleZoneState();
        }
    }
}
