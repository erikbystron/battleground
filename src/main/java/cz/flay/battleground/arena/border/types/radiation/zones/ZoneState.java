package cz.flay.battleground.arena.border.types.radiation.zones;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

public class ZoneState implements ConfigurationSerializable {

    protected int stayTime;
    protected int shrinkingSpeed;

    public ZoneState(int stayTime, int shrinkingSpeed) {
        this.stayTime = stayTime;
        this.shrinkingSpeed = shrinkingSpeed;
    }

    public ZoneState() {
        stayTime = 0;
        shrinkingSpeed = 0;
    }

    public int getStayTime() {
        return stayTime;
    }

    public int getShrinkingSpeed() {
        return shrinkingSpeed;
    }

    public ZoneState setStayTime(int stayTime) {
        this.stayTime = stayTime;
        return this;
    }

    public ZoneState setShrinkingSpeed(int shrinkingSpeed) {
        if (shrinkingSpeed > 20) {
            this.shrinkingSpeed = 20;
            return this;
        }

        this.shrinkingSpeed = shrinkingSpeed;
        return this;
    }

    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();
            data.put("stay-time", stayTime);
            data.put("shrinking-speed", shrinkingSpeed);
        return data;
    }
}
