package cz.flay.battleground.arena.border.types.radiation.zones;

import cz.flay.battleground.arena.border.Border;
import cz.flay.battleground.arena.border.types.radiation.RadiationZone;
import cz.flay.battleground.arena.border.types.radiation.zones.states.CircleZoneState;
import cz.flay.battleground.player.BattlePlayer;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class CircleRadiationZone extends RadiationZone {

    private Vector safeZoneCenter;
    private double safeZoneRadius;
    private double cachedPowedZoneRadius;

    private int shrinkTaskID;

    private List<CircleZoneState> states;

    public CircleRadiationZone(Border border) {
        this.border = border;
    }

    @Override
    public void apply() {
        applied = true;
        nextState();
    }

    private void nextState() {
        if (states.isEmpty()) {
            return;
        }

        CircleZoneState state = states.get(states.size() - 1);
        states.remove(state);
        applyState(state);
    }

    private void applyState(CircleZoneState state) {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        Vector newCenter = safeZoneCenter;

        double radius = random.nextDouble(0, (safeZoneRadius - state.getZoneRadius()));
        double x = random.nextDouble(0, radius);
        double z = (int) Math.sqrt((Math.pow(radius, 2) - Math.pow(x, 2)));

        if (random.nextBoolean()) {
            x = -x;
        }

        if (random.nextBoolean()) {
            z = -z;
        }

        newCenter.setX(newCenter.getX() + x);
        newCenter.setZ(newCenter.getZ() + z);

        Iterator<Block> line = new BlockIterator(border.getArena().getWorld(), safeZoneCenter, newCenter,
                0.1, (int) safeZoneCenter.distance(newCenter));

        double radiusDecrease = (safeZoneRadius - radius) / safeZoneCenter.distance(newCenter);

        shrinkTaskID = Bukkit.getScheduler().runTaskTimer(plugin, () -> {
            if (safeZoneRadius == radius && safeZoneCenter.equals(newCenter)) {
                stopShrinking();
            }

            if (safeZoneRadius != radius) {
                safeZoneRadius -= radiusDecrease;
            }

            if (!safeZoneCenter.equals(newCenter)) {
                if (line.hasNext()) {
                    safeZoneCenter = line.next().getLocation().toVector();
                }
            }
        }, 0, 20 / state.getShrinkingSpeed()).getTaskId();

        Bukkit.getScheduler().runTaskLater(plugin, this::nextState, state.getStayTime());
    }

    @Override
    public boolean isPlayerIn(BattlePlayer player) {
        return cachedPowedZoneRadius > safeZoneCenter.distanceSquared(player.getBukkitPlayer().getLocation().toVector());
    }

    public Vector getSafeZoneCenter() {
        return safeZoneCenter;
    }

    public double getSafeZoneRadius() {
        return safeZoneRadius;
    }

    public List<CircleZoneState> getStates() {
        return states;
    }

    public CircleRadiationZone setSafeZoneCenter(Vector safeZoneCenter) {
        this.safeZoneCenter = safeZoneCenter;
        return this;
    }

    public CircleRadiationZone setSafeZoneRadius(double safeZoneRadius) {
        this.safeZoneRadius = safeZoneRadius;
        return this;
    }

    public CircleRadiationZone setStates(List<ZoneState> states) {
        HashMap<Double, CircleZoneState> unSorted = new HashMap<>();
        for (ZoneState zoneState : states) {
            if (zoneState instanceof CircleZoneState) {
                unSorted.put( ((CircleZoneState) zoneState).getZoneRadius(), (CircleZoneState) zoneState);
            }
        }

        Map<Double, CircleZoneState> sorted = new TreeMap<>(unSorted);
        Set set = sorted.entrySet();
        for (Object object : set) {
            Map.Entry entry = (Map.Entry) object;
            states.add((CircleZoneState) entry.getValue());
        }
        return this;
    }

    private void stopShrinking() {
        Bukkit.getScheduler().cancelTask(shrinkTaskID);
    }
}
