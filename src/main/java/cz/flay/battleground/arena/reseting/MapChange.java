package cz.flay.battleground.arena.reseting;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.material.MaterialData;

import java.util.HashMap;
import java.util.Map;

import static cz.flay.battleground.Logger.error;

@SerializableAs("MapChange")
public class MapChange implements ConfigurationSerializable {

    private World world;
    private int x;
    private int y;
    private int z;
    private MaterialData data;

    private boolean empty = false;

    public MapChange(BlockState state) {
        Block block = state.getBlock();

        world = block.getWorld();
        x = block.getX();
        y = block.getY();
        z = block.getZ();
        data = new MaterialData(block.getType(), block.getData());
    }

    private MapChange(World world, int x, int y, int z, MaterialData data) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.data = data;
    }

    private MapChange() {
        empty = true;
    }

    void revert() {
        if (!empty) {
            world.getBlockAt(x, y, z).setType(data.getItemType());
            world.getBlockAt(x, y, z).setData(data.getData());
        }
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();
            data.put("world", world.getName());
            data.put("x", x);
            data.put("y", y);
            data.put("z", z);
            data.put("material", this.data.getItemType().toString());
            data.put("data", this.data.getData());
        return data;
    }

    public static MapChange deserialize(Map<String, Object> args) {
        try {
            World world = Bukkit.getWorld((String) args.get("world"));
            int x = (int) args.get("x");
            int y = (int) args.get("y");
            int z = (int) args.get("z");
            MaterialData data = new MaterialData(Material.valueOf((String) args.get("material")), ((Integer) args.get("data")).byteValue());

            return new MapChange(world, x, y, z, data);
        } catch (NullPointerException e) {
            error("Problems with loading not reverted map change");
            return new MapChange();
        }
    }
}
