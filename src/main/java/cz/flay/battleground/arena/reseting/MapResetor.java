package cz.flay.battleground.arena.reseting;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;


public class MapResetor {

    private final ChangesManager changesManager;

    public MapResetor(BattleGround plugin) {
        changesManager = new ChangesManager(plugin);
        resetAll();
    }

    private void resetAll() {
        for (String arenaID : changesManager.checkFiles()) {
            resetArena(arenaID);
        }
    }

    public void registerChange(Arena arena, MapChange change) {
        changesManager.saveChange(arena.getId(), change);
    }

    public void resetArena(Arena arena) {
        resetArena(arena.getId());
    }

    public void resetArena(String arenaID) {
        for (MapChange change : changesManager.getChanges(arenaID)) {
            change.revert();
        }
        changesManager.remove(arenaID);
    }
}
