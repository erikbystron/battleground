package cz.flay.battleground.arena.reseting;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.storage.config.ConfigFile;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class ChangesManager {

    private final BattleGround plugin;
    private final HashMap<String, ConfigFile> files;

    ChangesManager(BattleGround plugin) {
        files = new HashMap<>();
        this.plugin = plugin;
    }

    List<String> checkFiles() {
        List<String> files = new ArrayList<>();
        File folder = new File(plugin.getDataFolder(), "changes");

        try {
            for (File file : folder.listFiles()) {
                if (file.isFile()) {
                    String name = file.getName().substring(0, file.getName().indexOf("."));
                    files.add(name);
                    this.files.put(file.getName(), new ConfigFile("/changes/", name));
                }
            }
        } catch (NullPointerException ignored) {}

        return files;
    }

    void saveChange(String arenaID, MapChange change) {
        if (!files.containsKey(arenaID)) {
            ConfigFile file = new ConfigFile("/changes/", arenaID);
            files.put(arenaID, file);
        }

        ConfigFile file = files.get(arenaID);
        List<MapChange> changes = getChanges(arenaID);
        if (changes == null) {
            changes = new ArrayList<>();
        }
            changes.add(change);
            file.getConfig().set("changes", changes);
        file.save();
    }

    List<MapChange> getChanges(String arenaID) {
        try {
            return (List<MapChange>) files.get(arenaID).getConfig().getList("changes");
        } catch (NullPointerException e) {
            return new ArrayList<>();
        }
    }

    void remove(String arenaID) {
        try {
            files.get(arenaID).delete();
        } catch (NullPointerException ignored) { }
    }
}
