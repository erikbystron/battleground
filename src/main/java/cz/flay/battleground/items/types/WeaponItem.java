package cz.flay.battleground.items.types;

import com.shampaggon.crackshot.CSUtility;
import cz.flay.battleground.items.GameItem;
import cz.flay.battleground.items.GameItemsManager.ItemType;
import cz.flay.battleground.storage.config.ConfigFile;
import cz.flay.battleground.util.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class WeaponItem extends GameItem {

    private String weaponID;

    public WeaponItem(String id, ConfigFile config) {
        super(id, config);
        type = ItemType.WEAPON;
    }

    @Override
    protected void load() {
        YamlConfiguration config = this.config.getConfig();

        try {
            Material material = Material.valueOf(config.getString("properties.type"));
            byte data = Byte.valueOf(config.getString("properties.data"));

            item = new ItemBuilder(material, data)
                    .addNBTTag("#bg-item", id);

            for (String enchantment : config.getStringList("enchantments")) {
                Enchantment enchant = Enchantment.getByName(enchantment.substring(0, enchantment.indexOf('-')));
                int level = 1;
                try { level = Integer.valueOf(enchantment.substring(enchantment.indexOf('-'))); }
                catch (NumberFormatException ignored) { }

                item.addEnchantment(enchant, level);
            }

            weaponID = config.getString("weapon-id");

        } catch (NullPointerException e) {
            plugin.getLogger().severe("Problems with loading " + id + " item, some values are missing !");
            item = new ItemBuilder(Material.BARRIER)
                    .addNBTTag("#bg-item", id);
        }
    }

    @Override
    public ItemStack getItem(String locale) {
        ItemStack item = super.getItem(locale);
            item.getItemMeta().setDisplayName(new CSUtility().generateWeapon(weaponID).getItemMeta().getDisplayName());
        return item;
    }
}
