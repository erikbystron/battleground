package cz.flay.battleground.items.types;

import cz.flay.battleground.items.GameItem;
import cz.flay.battleground.items.GameItemsManager.ItemType;
import cz.flay.battleground.storage.config.ConfigFile;
import cz.flay.battleground.util.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;

public class VanillaItem extends GameItem {

    public VanillaItem(String id, ConfigFile config) {
        super(id, config);
        type = ItemType.VANILLA;
    }

    @Override
    protected void load() {
        YamlConfiguration config = this.config.getConfig();

        try {
            Material material = Material.valueOf(config.getString("properties.type"));
            byte data = Byte.valueOf(config.getString("properties.data"));

            item = new ItemBuilder(material, data)
                    .addNBTTag("#bg-item", id);

            for (String enchantment : config.getStringList("enchantments")) {
                Enchantment enchant = Enchantment.getByName(enchantment.substring(0, enchantment.indexOf('-')));
                int level = 1;
                try { level = Integer.valueOf(enchantment.substring(enchantment.indexOf('-'))); }
                catch (NumberFormatException ignored) { }

                item.addEnchantment(enchant, level);
            }

        } catch (NullPointerException e) {
            plugin.getLogger().severe("Problems with loading " + id + " item, some values are missing !");
            item = new ItemBuilder(Material.BARRIER)
                    .addNBTTag("#bg-item", id);
        }
    }
}
