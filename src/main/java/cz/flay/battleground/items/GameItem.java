package cz.flay.battleground.items;

import cz.flay.battleground.items.GameItemsManager.ItemType;
import cz.flay.battleground.storage.config.ConfigFile;

public class GameItem extends Item {

    protected ItemType type;

    protected GameItem(String id, ConfigFile config) {
        super(id, config);
    }

    @Override
    protected void load() {

    }

    public ItemType getType() {
        return type;
    }
}
