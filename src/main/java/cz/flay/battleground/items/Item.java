package cz.flay.battleground.items;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.storage.config.ConfigFile;
import cz.flay.battleground.util.ItemBuilder;
import de.tr7zw.itemnbtapi.ItemNBTAPI;
import de.tr7zw.itemnbtapi.NBTItem;
import org.apache.commons.codec.digest.DigestUtils;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

import java.time.LocalDateTime;

public class Item {

    protected final String id;
    protected final ConfigFile config;
    protected final BattleGround plugin;

    private final String securityHash;

    protected ItemBuilder item;

    Item(String id, ConfigFile config) {
        this.plugin = (BattleGround) Bukkit.getServer().getPluginManager().getPlugin("BattleGround");
        this.id = id;
        this.config = config;

        securityHash = DigestUtils.md5Hex(LocalDateTime.now().toString());
    }

    public ItemStack getItem(String locale) {
        ItemBuilder item = this.item.clone();

        if (config.getConfig().getString("locale." + locale + ".name") != null) {
            item.setName(config.getConfig().getString("locale." + locale + ".name"));
        } else {
            try {
                item.setName(config.getConfig().getString("locale.default.name"));
            } catch (NullPointerException ignored) { }
        }

        if (config.getConfig().getStringList("locale." + locale + ".lore") != null) {
            item.addLore(config.getConfig().getStringList("locale." + locale + ".lore"));
        } else {
            try {
                item.addLore(config.getConfig().getStringList("locale.default.lore"));
            } catch (NullPointerException ignored) { }
        }

        return putHash(item.toItemStack());
    }

    boolean isAuthentic(ItemStack itemStack) {
        NBTItem item = ItemNBTAPI.getNBTItem(itemStack);

        if (item.hasKey("#bg-item")) {
            return item.getString("#bg-item").equals(id);
        }

        return false;
    }

    boolean checkHash(ItemStack itemStack) {
        if (isAuthentic(itemStack)) {
            NBTItem item = new NBTItem(itemStack);

            if (item.hasKey("#bg-hash")) {
                return item.getString("#bg-hash").equals(securityHash);
            }
        }

        return false;
    }

    private ItemStack putHash(ItemStack itemStack) {
        NBTItem item = new NBTItem(itemStack);
            item.setString("#bg-hash", securityHash);
        return item.getItem();
    }

    protected void load() {

    }

    public String getId() {
        return id;
    }
}
