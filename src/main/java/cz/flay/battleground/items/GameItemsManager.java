package cz.flay.battleground.items;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.items.types.WeaponItem;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.config.ConfigFile;
import de.tr7zw.itemnbtapi.NBTItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class GameItemsManager {

    private final BattleGround plugin;

    private final HashMap<String, GameItem> gameItems;
    private final HashMap<String, ControlItem> controlItems;

    public GameItemsManager(BattleGround plugin) {
        this.plugin = plugin;

        controlItems = new HashMap<>();
        loadControlItems();

        gameItems = new HashMap<>();
        loadGameItems();
    }

    private void loadGameItems() {
        File path = new File(plugin.getDataFolder() + "/game-items/");

        if (!path.exists()) {
            path.mkdirs();
        }

        try {
            for (File file : Objects.requireNonNull(path.listFiles())) {
                String id = file.getName().substring(0, file.getName().indexOf("."));
                ConfigFile configFile = new ConfigFile(file);

                ItemType type = ItemType.valueOf(configFile.getConfig().getString("type").toUpperCase());
                switch (type) {
                    case VANILLA:
                        gameItems.put(id, new GameItem(id, configFile));
                        break;
                    case WEAPON:
                        if (Bukkit.getPluginManager().isPluginEnabled("CrackShot")) {
                            gameItems.put(id, new WeaponItem(id, configFile));
                        }
                        break;
                }
            }
        } catch (NullPointerException ignored) { }

    }

    private void loadControlItems() {
        File path = new File(plugin.getDataFolder() + "/control-items/");

        if (!path.exists()) {
            path.mkdirs();
        }

        for (Item item : Item.values()) {
            File file = new File(path, item.getId() + ".yml");

            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            this.controlItems.put(item.getId(),
                    new ControlItem(item.getId(), new ConfigFile("/control-items/", item.getId())));
        }

        File folder = new File(plugin.getDataFolder(), "/control-items/");

        for (File file : folder.listFiles()) {
            if (!file.isFile()) {
                continue;
            }

            String id = file.getName().substring(0, file.getName().indexOf("."));

            if (controlItems.containsKey(id)) {
                continue;
            }

            this.controlItems.put(id,
                    new ControlItem(id, new ConfigFile("/control-items/", id)));
        }
    }

    public ItemStack getControlItem(BattlePlayer player, Item item) {
        return getControlItem(player, item.getId());
    }

    public ItemStack getControlItem(BattlePlayer player, String item) {
        if (controlItems.containsKey(item)) {
            if (controlItems.get(item).hasPermission(player)) {
                return controlItems.get(item).getItem(player.getLanguage());
            } else {
                return new ItemStack(Material.AIR);
            }
        }

        return null;
    }

    public boolean isItemType(ItemType type, ItemStack itemStack) {
        NBTItem item = new NBTItem(itemStack);
        if (item.hasKey("#bg-item")) {
            if (gameItems.containsKey(item.getString("#bg-item"))) {
                return type.equals(gameItems.get(item.getString("#bg-item")).getType());
            }
        }

        return false;
    }

    public boolean itemEquals(Item item, ItemStack itemStack) {
        return itemEquals(item.getId(), itemStack);
    }

    public boolean itemEquals(String item, ItemStack itemStack) {
        if (controlItems.containsKey(item)) {
            return controlItems.get(item).isAuthentic(itemStack);
        }

        return false;
    }

    public boolean checkHash(BattlePlayer player, ItemStack itemStack) {
        return checkHash(player.getBukkitPlayer().getInventory(), itemStack);
    }

    public  boolean checkHash(Inventory inventory, ItemStack itemStack) {
        NBTItem item = new NBTItem(itemStack);

        if (item.hasKey("#bg-item")) {
            String sign = item.getString("#bg-item");
            if (controlItems.containsKey(sign)) {
                if (!controlItems.get(sign).checkHash(itemStack)) {
                    inventory.remove(itemStack);
                    return false;
                }
            }
        }

        return true;
    }

    public boolean gameItemsContains(String id) {
        return gameItems.containsKey(id);
    }

    public boolean controlItemsContains(String id) {
        return controlItems.containsKey(id);
    }

    public ItemStack translateItem(BattlePlayer player, ItemStack itemStack) {
        if (!new NBTItem(itemStack).hasKey("#bg-item")) {
            return new ItemStack(Material.AIR);
        }

        ItemStack translated = gameItems.get(new NBTItem(itemStack)
                .getString("#bg-item")).getItem(player.getLanguage());

        ItemMeta meta = itemStack.getItemMeta();

        meta.setDisplayName(translated.getItemMeta().getDisplayName());
        meta.setLore(translated.getItemMeta().getLore());

        itemStack.setItemMeta(meta);

        return itemStack;
    }

    public ControlItem getControlItem(String id) {
        for (ControlItem item : getControlItems()) {
            if (item.getId().equals(id)) {
                return item;
            }
        }

        return null;
    }

    public List<GameItem> getGameItems() {
        return new ArrayList<>(gameItems.values());
    }

    public List<ControlItem> getControlItems() {
        return new ArrayList<>(controlItems.values());
    }

    public enum ItemType {
        VANILLA, WEAPON
    }

    public enum Item {
        PARACHUTE("parachute");

        private final String id;

        Item(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }
}
