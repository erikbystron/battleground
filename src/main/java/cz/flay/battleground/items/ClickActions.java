package cz.flay.battleground.items;

import cz.flay.battleground.player.BattlePlayer;
import org.bukkit.Bukkit;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.ClickType;

public class ClickActions {

    private String leftClick, shiftLeftClick, rightClick, shiftRightClick,
            rightInvClick, shiftRightInvClick, leftInvClick, shiftLeftInvClick;

    public ClickActions() {}

    public ClickActions(String leftClick, String shiftLeftClick, String rightClick, String shiftRightClick,
                        String rightInvClick, String shiftRightInvClick, String leftInvClick, String shiftLeftInvClick) {
        this.leftClick = leftClick;
        this.rightClick = rightClick;

        this.leftInvClick = leftInvClick;
        this.rightInvClick = rightInvClick;

        this.shiftLeftClick = shiftLeftClick;
        this.shiftRightClick = shiftRightClick;

        this.shiftLeftInvClick = shiftLeftInvClick;
        this.shiftRightInvClick = shiftRightInvClick;
    }

    public void runAction(BattlePlayer player, ClickType type) {
        if (type.isLeftClick() && !type.isShiftClick()) {
            if (leftInvClick != null && !leftInvClick.equals("")) {
                Bukkit.getServer().dispatchCommand(player.getBukkitPlayer(), leftInvClick);
            }
        }

        if (type.isLeftClick() && type.isShiftClick()) {
            if (shiftLeftInvClick != null && !shiftLeftInvClick.equals("")) {
                Bukkit.getServer().dispatchCommand(player.getBukkitPlayer(), shiftLeftInvClick);
            }
        }

        if (type.isRightClick() && !type.isShiftClick()) {
            if (rightInvClick != null && !rightInvClick.equals("")) {
                Bukkit.getServer().dispatchCommand(player.getBukkitPlayer(), rightInvClick);
            }
        }

        if (type.isRightClick() && type.isShiftClick()) {
            if (shiftRightInvClick != null && !shiftRightInvClick.equals("")) {
                Bukkit.getServer().dispatchCommand(player.getBukkitPlayer(), shiftRightInvClick);
            }
        }
    }

    public void runAction(BattlePlayer player, Action action) {
        boolean shifting = player.getBukkitPlayer().isSneaking();

        if ((action.equals(Action.LEFT_CLICK_AIR) || action.equals(Action.LEFT_CLICK_BLOCK)) && !shifting) {
            if (leftClick != null && !leftClick.equals("")) {
                Bukkit.getServer().dispatchCommand(player.getBukkitPlayer(), leftClick);
            }
        }

        if ((action.equals(Action.LEFT_CLICK_AIR) || action.equals(Action.LEFT_CLICK_BLOCK)) && shifting) {
            if (shiftLeftClick != null && !shiftLeftClick.equals("")) {
                Bukkit.getServer().dispatchCommand(player.getBukkitPlayer(), shiftLeftClick);
            }
        }

        if ((action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) && !shifting) {
            if (rightClick != null && !rightClick.equals("")) {
                Bukkit.getServer().dispatchCommand(player.getBukkitPlayer(), rightClick);
            }
        }

        if ((action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) && shifting) {
            if (shiftRightClick != null && !shiftRightClick.equals("")) {
                Bukkit.getServer().dispatchCommand(player.getBukkitPlayer(), shiftRightClick);
            }
        }
    }
}
