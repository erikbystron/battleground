package cz.flay.battleground.items;

import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.config.ConfigFile;
import cz.flay.battleground.util.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;

import static cz.flay.battleground.Logger.error;

public class ControlItem extends Item {

    private ClickActions clickActions;
    private String get_permission;

    ControlItem(String id, ConfigFile config) {
        super(id, config);
        load();
    }

    @Override
    protected void load() {
        YamlConfiguration config = this.config.getConfig();

        try {
            Material material = Material.valueOf(config.getString("properties.type"));
            byte data = Byte.valueOf(config.getString("properties.data"));

            item = new ItemBuilder(material, data)
                            .addNBTTag("#bg-item", id);

            for (String enchantment : config.getStringList("enchantments")) {
                Enchantment enchant = Enchantment.getByName(enchantment.substring(0, enchantment.indexOf('-')));
                int level = 1;
                try { level = Integer.valueOf(enchantment.substring(enchantment.indexOf('-'))); }
                    catch (NumberFormatException ignored) { }

                item.addEnchantment(enchant, level);
            }

            String use_permission = config.getString("permissions.use");
            get_permission = config.getString("permissions.get");

            clickActions = new ClickActions(config.getString("click-command.left-click"),
                    config.getString("click-command.shift-left-click"),
                    config.getString("click-command.right-click"),
                    config.getString("click-command.shift-right-click"),
                    config.getString("click-command.left-inv-click"),
                    config.getString("click-command.shift-left-inv-click"),
                    config.getString("click-command.right-inv-click"),
                    config.getString("click-command.shift-right-inv-click"));

            item.addNBTTag("#bg-perm", use_permission);
        } catch (NullPointerException e) {
            error("Problems with loading " + id + " item, some values are missing !");
            item = new ItemBuilder(Material.BARRIER)
                .addNBTTag("#bg-item", id);
        }
    }

    public boolean hasPermission(BattlePlayer player) {
        if (get_permission == null || get_permission.equals("")) {
            return true;
        }

        return player.getBukkitPlayer().hasPermission(get_permission);
    }

    public ClickActions getClickActions() {
        if (clickActions != null) {
            return clickActions;
        }

        return new ClickActions();
    }
}
