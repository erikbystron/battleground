package cz.flay.battleground.broadcast.components;

import cz.flay.battleground.broadcast.Component;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.Placeholders;

public class ChatComponent extends Component {

    private final Message message;
    private boolean jsonFormatted;

    public ChatComponent(Message message, boolean animated, Placeholders placeholders, boolean jsonFormatted) {
        super(animated);
        this.message = message;
        this.placeholders = placeholders;
        this.jsonFormatted = jsonFormatted;
    }

    @Override
    public void send(BattlePlayer receiver) {
        if (jsonFormatted) {
            for (String message : localeManager.getMessagesList(receiver, message, placeholders)) {
                receiver.sendJsonChatMessage(message);
            }
        } else {
            for (String message : localeManager.getMessagesList(receiver, message, placeholders)) {
                receiver.sendChatMessage(message);
            }
        }
    }
}
