package cz.flay.battleground.broadcast.components;

import cz.flay.battleground.broadcast.Component;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.Placeholders;
import org.bukkit.Bukkit;

import java.util.List;

public class ActionBarComponent extends Component {

    private final Message message;

    public ActionBarComponent(Message message, boolean animated, Placeholders placeholders) {
        super(animated);
        this.message = message;
        this.placeholders = placeholders;
    }

    @Override
    public void send(BattlePlayer receiver) {
        if (animated) {
            List<String> frames = localeManager.getMessagesList(receiver, message, placeholders);

            final int[] frame = {0};
            taskID = Bukkit.getScheduler().runTaskTimer(Bukkit.getPluginManager().getPlugin("BattleGround"), () -> {
                if (frame[0] >= frames.size()) {
                    cancelTask();
                }

                receiver.sendActionBar(localeManager.getMessage(receiver, message, placeholders));

                frame[0]++;
            }, 0, 20).getTaskId();
        } else {
            receiver.sendActionBar(localeManager.getMessage(receiver, message, placeholders));
        }
    }
}
