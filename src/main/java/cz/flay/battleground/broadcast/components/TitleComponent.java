package cz.flay.battleground.broadcast.components;

import cz.flay.battleground.broadcast.Component;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.Placeholders;
import org.bukkit.Bukkit;

import java.util.List;

public class TitleComponent extends Component {

    private Message title, subTitle;
    private int titleFadeIn, titleStay, titleFadeOut;
    private int subTitleFadeIn, subTitleStay, subTitleFadeOut;

    public TitleComponent(Message title, Message subTitle, boolean animated, Placeholders placeholders,
                          int titleFadeIn, int titleStay, int titleFadeOut,
                          int subTitleFadeIn, int subTitleStay, int subTitleFadeOut) {
        super(animated);

        this.title = title;
        this.subTitle = subTitle;
        this.placeholders = placeholders;

        this.titleFadeIn = titleFadeIn * 20;
        this.titleStay = titleStay * 20;
        this.titleFadeOut = titleFadeOut * 20;

        this.subTitleFadeIn = subTitleFadeIn * 20;
        this.subTitleStay = subTitleStay * 20;
        this.subTitleFadeOut = subTitleFadeOut * 20;
    }

    @Override
    public void send(BattlePlayer receiver) {
        if (animated) {
            List<String> titleFrames = localeManager.getMessagesList(receiver, title, placeholders);
            List<String> subTitleFrames = localeManager.getMessagesList(receiver, subTitle, placeholders);

            final int[] frame = {0};
            taskID = Bukkit.getScheduler().runTaskTimer(Bukkit.getPluginManager().getPlugin("BattleGround"), () -> {
                if (frame[0] >= titleFrames.size() && frame[0] >= subTitleFrames.size()) {
                    cancelTask();
                }

                if (titleFrames.size() > frame[0]) {
                    receiver.sendTitle(titleFrames.get(frame[0]),
                            titleFadeIn, titleStay, titleFadeOut);
                }

                if (subTitleFrames.size() > frame[0]) {
                    receiver.sendSubTitle(subTitleFrames.get(frame[0]),
                            subTitleFadeIn, subTitleStay, subTitleFadeOut);
                }

                frame[0]++;
            }, 0, 20).getTaskId();
        } else {
            receiver.sendTitle(localeManager.getMessage(receiver, title, placeholders),
                    titleFadeIn, titleStay, titleFadeOut);

            receiver.sendSubTitle(localeManager.getMessage(receiver, subTitle, placeholders),
                    subTitleFadeIn, subTitleStay, subTitleFadeOut);
        }
    }
}
