package cz.flay.battleground.broadcast.components;

import cz.flay.battleground.broadcast.Component;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.Placeholders;

public class BossBarComponent extends Component {

    private final Message message;

    public BossBarComponent(Message message, boolean animated, Placeholders placeholders) {
        super(animated);
        this.message = message;
        this.placeholders = placeholders;
    }

    @Override
    public void send(BattlePlayer receiver) {
        if (animated) {

        } else {

        }
    }
}
