package cz.flay.battleground.broadcast;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.LocaleManager;
import cz.flay.battleground.util.Placeholders;
import org.bukkit.Bukkit;


public class Component {

    protected Placeholders placeholders;
    protected final LocaleManager localeManager;

    protected final boolean animated;
    protected int taskID;

    protected Component(boolean animated) {
        this.animated = animated;
        localeManager = ((BattleGround) Bukkit.getPluginManager().getPlugin("BattleGround")).getLocaleManager();
    }

    public void send(BattlePlayer receiver) {

    }

    protected void cancelTask() {
        Bukkit.getScheduler().cancelTask(taskID);
    }
}
