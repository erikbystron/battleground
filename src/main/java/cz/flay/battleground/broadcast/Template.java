package cz.flay.battleground.broadcast;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.broadcast.components.ActionBarComponent;
import cz.flay.battleground.broadcast.components.BossBarComponent;
import cz.flay.battleground.broadcast.components.ChatComponent;
import cz.flay.battleground.broadcast.components.TitleComponent;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.Placeholders;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.ArrayList;
import java.util.List;

public class Template {

    protected Placeholders placeholders;
    private Component actionBar, bossBar, title, chat;

    protected Template() {
        placeholders = new Placeholders();
    }

    private boolean isActionBarEnabled() {
        return actionBar != null;
    }

    private boolean isBossBarEnabled() {
        return bossBar != null;
    }

    private boolean isTitleEnabled() {
        return title != null;
    }

    private boolean isChatEnabled() {
        return chat != null;
    }


    protected void load(String id) {
        BattleGround plugin = (BattleGround) Bukkit.getPluginManager().getPlugin("BattleGround");

        YamlConfiguration config = plugin.getConfigManager().getBroadcastConfig();

        if (config.getBoolean(id + ".chat.enabled")) {
            chat = new ChatComponent(Message.valueOf(id.replaceAll("-", "_").toUpperCase() + "_CHAT"),
                    false, placeholders,
                    config.getBoolean(id + ".chat.properties.json-formatted"));
        }

        if (config.getBoolean(id + ".title.enabled")) {
            title = new TitleComponent(Message.valueOf(id.replaceAll("-", "_").toUpperCase() + "_TITLE"),
                    Message.valueOf(id.replaceAll("-", "_").toUpperCase() + "_SUBTITLE"),
                    config.getBoolean(id + ".title.properties.animated"),
                    placeholders,
                    config.getInt(id + ".title.properties.title.fadeIn"),
                    config.getInt(id + ".title.properties.title.stay"),
                    config.getInt(id + ".title.properties.title.fadeOut"),
                    config.getInt(id + ".title.properties.sub-title.fadeIn"),
                    config.getInt(id + ".title.properties.sub-title.stay"),
                    config.getInt(id + ".title.properties.sub-title.fadeOut"));
        }

        if (config.getBoolean(id + ".action-bar.enabled")) {
            actionBar = new ActionBarComponent(Message.valueOf(id.replaceAll("-", "_").toUpperCase() + "_ACTIONBAR"),
                    config.getBoolean(id + ".actiobar.properties.animated"), placeholders);
        }

        if (config.getBoolean(id + ".boss-bar.enabled")) {
            bossBar = new BossBarComponent(Message.valueOf(id.replaceAll("-", "_").toUpperCase() + "_BOSSBAR"),
                    config.getBoolean(id + ".bossbar.properties.animated"), placeholders);
        }
    }

    protected void broadcast(BattlePlayer receiver) {
        List<BattlePlayer> receivers = new ArrayList<>();
            receivers.add(receiver);
        broadcast(receivers);
    }

    protected void broadcast(List<BattlePlayer> receivers) {
        for (BattlePlayer receiver : receivers) {
            if (isActionBarEnabled()) {
                actionBar.send(receiver);
            }

            if (isBossBarEnabled()) {
                bossBar.send(receiver);
            }

            if (isTitleEnabled()) {
                title.send(receiver);
            }

            if (isChatEnabled()) {
                chat.send(receiver);
            }
        }
    }

}
