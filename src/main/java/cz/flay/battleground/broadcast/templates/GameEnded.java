package cz.flay.battleground.broadcast.templates;

import cz.flay.battleground.broadcast.Template;
import cz.flay.battleground.game.Winners;
import cz.flay.battleground.player.BattlePlayer;

import java.util.List;

public class GameEnded extends Template {

    public GameEnded(List<BattlePlayer> players, Winners winners) {
        super();

        if (winners.getFirst() != null) {
            placeholders.add("%first_player%", winners.getFirst().getBukkitPlayer().getDisplayName());
        } else {
            placeholders.add("%first_player%", "---");
        }

        if (winners.getSecond() != null) {
            placeholders.add("%second_player%", winners.getSecond().getBukkitPlayer().getDisplayName());
        } else {
            placeholders.add("%second_player%", "---");
        }

        if (winners.getThird() != null) {
            placeholders.add("%third_player%", winners.getThird().getBukkitPlayer().getDisplayName());
        } else {
            placeholders.add("%third_player%", "---");
        }

        load("game-ended");
        broadcast(players);
    }
}
