package cz.flay.battleground.broadcast.templates;

import cz.flay.battleground.broadcast.Template;
import cz.flay.battleground.player.BattlePlayer;

public class GameJoined extends Template {

    public GameJoined(BattlePlayer player) {
        super();
        placeholders.add("%player_name%", player.getBukkitPlayer().getDisplayName());
        load("game-joined");
        broadcast(player);
    }
}
