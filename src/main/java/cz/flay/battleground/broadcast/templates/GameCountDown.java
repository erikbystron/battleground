package cz.flay.battleground.broadcast.templates;

import cz.flay.battleground.broadcast.Template;
import cz.flay.battleground.player.BattlePlayer;

import java.util.List;

public class GameCountDown extends Template {

    public GameCountDown(List<BattlePlayer> players, int value) {
        super();
        placeholders.add("%value%", String.valueOf(value));
        load("game-count-down");
        broadcast(players);
    }
}
