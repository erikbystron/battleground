package cz.flay.battleground.broadcast.templates;

import cz.flay.battleground.broadcast.Template;
import cz.flay.battleground.player.BattlePlayer;

import java.util.List;

public class GamePlaneTeleporting extends Template {

    public GamePlaneTeleporting(List<BattlePlayer> players) {
        super();
        load("game-plane-teleporting");
        broadcast(players);
    }
}
