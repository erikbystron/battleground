package cz.flay.battleground.broadcast.templates;

import cz.flay.battleground.broadcast.Template;
import cz.flay.battleground.player.BattlePlayer;

public class GameLeaved extends Template {

    public GameLeaved(BattlePlayer player) {
        super();
        placeholders.add("%player_name%", player.getBukkitPlayer().getDisplayName());
        load("game-leaved");
        broadcast(player);
    }
}
