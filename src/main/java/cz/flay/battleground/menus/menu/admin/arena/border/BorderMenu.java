package cz.flay.battleground.menus.menu.admin.arena.border;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.menus.Menu;
import cz.flay.battleground.menus.menu.admin.arena.ArenaMenu;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class BorderMenu extends Menu {

    private final ArenasManager arenasManager;
    private final ArenaMenu arenaMenu;

    public BorderMenu(BattleGround plugin, ArenaMenu arenaMenu) {
        super("#borderMenu", plugin, Message.BORDER_MENU_TITLE);
        this.arenaMenu = arenaMenu;
        arenasManager = plugin.getArenasManager();
    }

    public void createMenu(BattlePlayer player, Arena arena) {
        Inventory inv = Bukkit.getServer().createInventory(null, 27, getName(player));

        inv.setItem(0, getBackItem(player));

        player.getBukkitPlayer().openInventory(inv);
    }

    @Override
    public void onClick(BattlePlayer player, InventoryClickEvent e) {
        ItemStack item = e.getCurrentItem();

        if (signEquals(item, "#back")) {
            arenaMenu.createMenu(player, getArena(e.getInventory()));
        }
    }

    private Arena getArena(Inventory inv) {
        return arenasManager.getArena(getNBTItem(inv.getItem(4)).getString("bg-arena-id"));
    }
}
