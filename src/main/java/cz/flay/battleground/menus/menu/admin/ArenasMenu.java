package cz.flay.battleground.menus.menu.admin;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.menus.Menu;
import cz.flay.battleground.menus.PageableMenu;
import cz.flay.battleground.menus.menu.admin.arena.ArenaMenu;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.ItemBuilder;
import net.wesjd.anvilgui.AnvilGUI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class ArenasMenu extends PageableMenu {

    private final Menu mainMenu, arenaMenu;
    private final ArenasManager arenasManager;

    public ArenasMenu(BattleGround plugin, MainMenu mainMenu) {
        super("#arenasMenu", plugin, Message.ARENAS_MENU_TITLE);
        this.mainMenu = mainMenu;
        arenaMenu = new ArenaMenu(plugin, this);
        this.arenasManager = plugin.getArenasManager();
    }

    @Override
    public void createMenu(BattlePlayer player, int page) {
        List<Arena> arenas = menuUtils.getPageArenas(arenasManager.getArenas(), page);
        Inventory inv = Bukkit.getServer().createInventory(null, menuUtils.getSize(arenas.size()), getName(player));

        inv = addPagination(inv, page, menuUtils.neededPages(arenasManager.getArenas().size()));

        inv.setItem(0, getBackItem(player));

        inv.setItem(4, new ItemBuilder(Material.GRASS)
                .setName(localeManager.getMessage(player, Message.NEW_ARENA_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.NEW_ARENA_ITEM_LORE))
                .addNBTTag("bg-sign", "#newItem" + menuID)
                .toItemStack());

        int[] pos = menuUtils.getPositions(menuUtils.getSize(arenas.size()));
        for (Arena arena : arenas) {
            inv.setItem(pos[arenas.indexOf(arena)], new ItemBuilder(Material.MAP)
                    .addNBTTag("bg-sign", "#arenaItem" + menuID)
                    .addNBTTag("bg-arena-id", arena.getId())
                    .toItemStack());
        }

        player.getBukkitPlayer().openInventory(inv);
    }

    @Override
    public void onClick(BattlePlayer player, InventoryClickEvent e) {
        super.onClick(player, e);
        ItemStack item = e.getCurrentItem();

        if (signEquals(item, "#back")) {
            mainMenu.createMenu(player);
        }

        if (signEquals(item, "#newItem")) {
            new AnvilGUI(plugin, player.getBukkitPlayer(), localeManager.getConsoleMessage(Message.ARENA_NAME_INSERT), (p, reply) -> {
                if (reply != null) {
                    if (reply.length() > 0 && arenasManager.getArena(reply) == null && !arenasManager.isNameUsed(reply)) {
                        arenasManager.setUpArena(arenasManager.createArena(reply));
                        ((ArenaMenu) arenaMenu).createMenu(player, arenasManager.getArena(reply));
                    } else {
                        return localeManager.getConsoleMessage(Message.ARENA_NAME_ALREADY_USED_INSERT);
                    }
                }
                return "";
            }, (p, reply) -> {
                createMenu(player);
                return null;
            });
        }

        if (signEquals(item, "#arenaItem")) {
            ((ArenaMenu) arenaMenu).createMenu(player, arenasManager.getArena(getNBTItem(item).getString("bg-arena-id")));
        }
    }
}
