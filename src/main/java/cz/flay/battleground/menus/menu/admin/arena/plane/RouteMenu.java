package cz.flay.battleground.menus.menu.admin.arena.plane;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.arena.plane.PlaneRoute;
import cz.flay.battleground.menus.Menu;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.ItemBuilder;
import cz.flay.battleground.util.Placeholders;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class RouteMenu extends Menu {

    private final ArenasManager arenasManager;
    private final RoutesMenu routesMenu;

    public RouteMenu(BattleGround plugin, RoutesMenu routesMenu) {
        super("#routeMenu", plugin, Message.ROUTE_MENU_TITLE);
        this.routesMenu = routesMenu;
        arenasManager = plugin.getArenasManager();
    }

    public void createMenu(BattlePlayer player, Arena arena, PlaneRoute route) {
        String plusURL = "http://textures.minecraft.net/texture/0a21eb4c57750729a48b88e9bbdb987eb6250a5bc2157b59316f5f1887db5";
        String minusURL = "http://textures.minecraft.net/texture/a8c67fed7a2472b7e9afd8d772c13db7b82c32ceeff8db977474c11e4611";
        Inventory inv = Bukkit.getServer().createInventory(null, 54, getName(player));

        inv.setItem(0, getBackItem(player));

        inv.setItem(4, new ItemBuilder(Material.BOOK_AND_QUILL)
                .addNBTTag("bg-sign", "#editItem" + menuID)
                .addNBTTag("bg-arena-id", arena.getId())
                .addNBTTag("bg-route-id", String.valueOf(arena.getAirPlane().getRoutes().indexOf(route)))
                .toItemStack());

        inv.setItem(11, new ItemBuilder("http://textures.minecraft.net/texture/b1dd4fe4a429abd665dfdb3e21321d6efa6a6b5e7b956db9c5d59c9efab25")
                .addNBTTag("bg-sign", "#firstLocItem" + menuID)
                .toItemStack());

        inv.setItem(15, new ItemBuilder("http://textures.minecraft.net/texture/b1dd4fe4a429abd665dfdb3e21321d6efa6a6b5e7b956db9c5d59c9efab25")
                .addNBTTag("bg-sign", "#secondLocItem" + menuID)
                .toItemStack());

        inv.setItem(19, new ItemBuilder(plusURL)
                .addNBTTag("bg-sign", "#plusFirX" + menuID)
                .toItemStack());

        inv.setItem(20, new ItemBuilder("http://textures.minecraft.net/texture/5a6787ba32564e7c2f3a0ce64498ecbb23b89845e5a66b5cec7736f729ed37")
                .addNBTTag("bg-sign", "#firXItem" + menuID)
                .toItemStack());

        inv.setItem(21, new ItemBuilder(minusURL)
                .addNBTTag("bg-sign", "#minusFirX" + menuID)
                .toItemStack());

        inv.setItem(28, new ItemBuilder(plusURL)
                .addNBTTag("bg-sign", "#plusFirZ" + menuID)
                .toItemStack());

        inv.setItem(29, new ItemBuilder("http://textures.minecraft.net/texture/90582b9b5d97974b11461d63eced85f438a3eef5dc3279f9c47e1e38ea54ae8d")
                .addNBTTag("bg-sign", "#firZItem" + menuID)
                .toItemStack());

        inv.setItem(30, new ItemBuilder(minusURL)
                .addNBTTag("bg-sign", "#minusFirZ" + menuID)
                .toItemStack());

        inv.setItem(22, new ItemBuilder(Material.NAME_TAG)
                .addNBTTag("bg-sign", "#nameItem" + menuID)
                .toItemStack());

        inv.setItem(23, new ItemBuilder(plusURL)
                .addNBTTag("bg-sign", "#plusSecX" + menuID)
                .toItemStack());

        inv.setItem(24, new ItemBuilder("http://textures.minecraft.net/texture/5a6787ba32564e7c2f3a0ce64498ecbb23b89845e5a66b5cec7736f729ed37")
                .addNBTTag("bg-sign", "#secXItem" + menuID)
                .toItemStack());

        inv.setItem(25, new ItemBuilder(minusURL)
                .addNBTTag("bg-sign", "#minusSecX" + menuID)
                .toItemStack());

        inv.setItem(32, new ItemBuilder(plusURL)
                .addNBTTag("bg-sign", "#plusSecZ" + menuID)
                .toItemStack());

        inv.setItem(33, new ItemBuilder("http://textures.minecraft.net/texture/90582b9b5d97974b11461d63eced85f438a3eef5dc3279f9c47e1e38ea54ae8d")
                .addNBTTag("bg-sign", "#secZItem" + menuID)
                .toItemStack());

        inv.setItem(34, new ItemBuilder(minusURL)
                .addNBTTag("bg-sign", "#minusSecZ" + menuID)
                .toItemStack());

        inv.setItem(46, new ItemBuilder(plusURL)
                .setName(localeManager.getMessage(player, Message.PLUS_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.PLUS_ITEM_LORE))
                .addNBTTag("bg-sign", "#plusNoFall" + menuID)
                .toItemStack());

        Placeholders nofallItem = new Placeholders()
                .add("%value%", String.valueOf(route.getNoFallZone()));

        inv.setItem(47, new ItemBuilder("http://textures.minecraft.net/texture/9e42f682e430b55b61204a6f8b76d5227d278ed9ec4d98bda4a7a4830a4b6")
                .setName(localeManager.getMessage(player, Message.NO_FALL_ZONE_ITEM_NAME, nofallItem))
                .addLore(localeManager.getMessagesList(player, Message.NO_FALL_ZONE_ITEM_LORE, nofallItem))
                .addNBTTag("bg-sign", "#nofallItem" + menuID)
                .toItemStack());

        inv.setItem(48, new ItemBuilder(minusURL)
                .setName(localeManager.getMessage(player, Message.MINUS_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.MINUS_ITEM_LORE))
                .addNBTTag("bg-sign", "#minusNoFall" + menuID)
                .toItemStack());

        inv.setItem(50, new ItemBuilder(plusURL)
                .setName(localeManager.getMessage(player, Message.PLUS_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.PLUS_ITEM_LORE))
                .addNBTTag("bg-sign", "#plusFall" + menuID)
                .toItemStack());

        Placeholders fallItem = new Placeholders()
                .add("%value%", String.valueOf(route.getFallZone()));

        inv.setItem(51, new ItemBuilder("http://textures.minecraft.net/texture/b55d5019c8d55bcb9dc3494ccc3419757f89c3384cf3c9abec3f18831f35b0")
                .setName(localeManager.getMessage(player, Message.FALL_ZONE_ITEM_NAME, fallItem))
                .addLore(localeManager.getMessagesList(player, Message.FALL_ZONE_ITEM_LORE, fallItem))
                .addNBTTag("bg-sign", "#fallItem" + menuID)
                .toItemStack());

        inv.setItem(52, new ItemBuilder(minusURL)
                .setName(localeManager.getMessage(player, Message.MINUS_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.MINUS_ITEM_LORE))
                .addNBTTag("bg-sign", "#minusFall" + menuID)
                .toItemStack());

        player.getBukkitPlayer().openInventory(inv);
    }

    @Override
    public void onClick(BattlePlayer player, InventoryClickEvent e) {
        ItemStack item = e.getCurrentItem();
        ClickType click = e.getClick();

        if (signEquals(item, "#back")) {
            routesMenu.createMenu(player, getArena(e.getInventory()));
        }

        /*if (signEquals(item, "#plusNoFall")) {
            int noFall = getArena(e.getInventory()).getAirPlane().getStartAfter();

            if (click.isRightClick() && !click.isShiftClick()) {
                noFall += 1;
            }

            if (click.isLeftClick() && !click.isShiftClick()) {
                noFall += 5;
            }

            if (click.isShiftClick() && click.isRightClick()) {
                noFall += 50;
            }

            if (click.isShiftClick() && click.isLeftClick()) {
                noFall += 100;
            }

            getArena(e.getInventory()).getAirPlane().setStartAfter(noFall);
        }

        if (signEquals(item, "#nofallItem")) {

        }

        if (signEquals(item, "#minusNoFall")) {
            int noFall = getArena(e.getInventory()).getAirPlane().getStartAfter();

            if (click.isRightClick() && !click.isShiftClick()) {
                noFall -= 1;
            }

            if (click.isLeftClick() && !click.isShiftClick()) {
                noFall -= 5;
            }

            if (click.isShiftClick() && click.isRightClick()) {
                noFall -= 50;
            }

            if (click.isShiftClick() && click.isLeftClick()) {
                noFall -= 100;
            }

            getArena(e.getInventory()).getAirPlane().setStartAfter(noFall);
        }

        if (signEquals(item, "#plusFall")) {
            int fall = getArena(e.getInventory()).getAirPlane().getFallZone();

            if (click.isRightClick() && !click.isShiftClick()) {
                fall += 1;
            }

            if (click.isLeftClick() && !click.isShiftClick()) {
                fall += 5;
            }

            if (click.isShiftClick() && click.isRightClick()) {
                fall += 50;
            }

            if (click.isShiftClick() && click.isLeftClick()) {
                fall += 100;
            }

            getArena(e.getInventory()).getAirPlane().setFallZone(fall);
        }

        if (signEquals(item, "#fallItem")) {

        }

        if (signEquals(item, "#minusFall")) {
            int fall = getArena(e.getInventory()).getAirPlane().getFallZone();

            if (click.isRightClick() && !click.isShiftClick()) {
                fall -= 1;
            }

            if (click.isLeftClick() && !click.isShiftClick()) {
                fall -= 5;
            }

            if (click.isShiftClick() && click.isRightClick()) {
                fall -= 50;
            }

            if (click.isShiftClick() && click.isLeftClick()) {
                fall -= 100;
            }

            getArena(e.getInventory()).getAirPlane().setFallZone(fall);
        }*/
    }

    private Arena getArena(Inventory inv) {
        return arenasManager.getArena(getNBTItem(inv.getItem(4)).getString("bg-arena-id"));
    }
}
