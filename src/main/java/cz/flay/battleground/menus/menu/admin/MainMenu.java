package cz.flay.battleground.menus.menu.admin;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.menus.Menu;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class MainMenu extends Menu {

    private final Menu arenasMenu, gameMenu, itemsMenu, configMenu;

    public MainMenu(BattleGround plugin) {
        super("#mainMenu", plugin, Message.MAIN_MENU_TITLE);
        arenasMenu = new ArenasMenu(plugin, this);
        gameMenu = new GameMenu(plugin, this);
        itemsMenu = new ItemsMenu(plugin, this);
        configMenu = new ConfigMenu(plugin, this);
    }

    @Override
    public void createMenu(BattlePlayer player) {
        Inventory inv = Bukkit.getServer().createInventory(null, 27, getName(player));

        inv.setItem(10, new ItemBuilder(Material.WORKBENCH)
                .setName(localeManager.getMessage(player, Message.ITEMS_MENU_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.ITEMS_MENU_ITEM_LORE))
                .addNBTTag("bg-sign", "#itemsMenu" + menuID)
                .toItemStack());

        inv.setItem(12, new ItemBuilder(Material.GRASS)
                .setName(localeManager.getMessage(player, Message.ARENAS_MENU_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.ARENAS_MENU_ITEM_LORE))
                .addNBTTag("bg-sign", "#arenasMenu" + menuID)
                .toItemStack());

        inv.setItem(14, new ItemBuilder(Material.REDSTONE)
                .setName(localeManager.getMessage(player, Message.GAME_MENU_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.GAME_MENU_ITEM_LORE))
                .addNBTTag("bg-sign", "#gameMenu" + menuID)
                .toItemStack());

        inv.setItem(16, new ItemBuilder(Material.PAPER)
                .setName(localeManager.getMessage(player, Message.CONFIG_MENU_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.CONFIG_MENU_ITEM_LORE))
                .addNBTTag("bg-sign", "#configMenu" + menuID)
                .toItemStack());

        player.getBukkitPlayer().openInventory(inv);
    }

    @Override
    public void onClick(BattlePlayer player, InventoryClickEvent e) {
        ItemStack item = e.getCurrentItem();

        if (signEquals(item, "#itemsMenu")) {
            itemsMenu.createMenu(player);
        }

        if (signEquals(item, "#arenasMenu")) {
            arenasMenu.createMenu(player);
        }

        if (signEquals(item, "#gameMenu")) {
            gameMenu.createMenu(player);
        }

        if (signEquals(item, "#configMenu")) {
            configMenu.createMenu(player);
        }
    }
}
