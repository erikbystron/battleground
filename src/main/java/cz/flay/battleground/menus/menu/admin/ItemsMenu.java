package cz.flay.battleground.menus.menu.admin;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.items.GameItem;
import cz.flay.battleground.items.GameItemsManager;
import cz.flay.battleground.menus.Menu;
import cz.flay.battleground.menus.PageableMenu;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class ItemsMenu extends PageableMenu {

    private final Menu mainMenu;
    private final GameItemsManager itemsManager;

    public ItemsMenu(BattleGround plugin, MainMenu mainMenu) {
        super("#itemsMenu", plugin, Message.ITEMS_MENU_TITLE);
        this.mainMenu = mainMenu;
        itemsManager = plugin.getItemsManager();
    }

    @Override
    public void createMenu(BattlePlayer player, int page) {
        List<GameItem> items = menuUtils.getPageGameItems(itemsManager.getGameItems(), page);
        Inventory inv = Bukkit.getServer().createInventory(null, menuUtils.getSize(items.size()), getName(player));

        inv = addPagination(inv, page, menuUtils.neededPages(itemsManager.getGameItems().size()));

        inv.setItem(0, getBackItem(player));

        inv.setItem(5, new ItemBuilder(Material.NAME_TAG)
                .addNBTTag("bg-sign", "#searchItem")
                .toItemStack());

        inv.setItem(4, new ItemBuilder(Material.COMPASS)
                .addNBTTag("bg-sign", "#sortItem")
                .toItemStack());

        inv.setItem(3, new ItemBuilder(Material.WORKBENCH)
                .addNBTTag("bg-sign", "#newItem")
                .toItemStack());

        int[] pos = menuUtils.getPositions(menuUtils.getSize(items.size()));
        for (GameItem item : items) {
            inv.setItem(pos[items.indexOf(item)], new ItemBuilder(Material.MAP)
                    .addNBTTag("bg-sign", "#arenaItem")
                    .addNBTTag("bg-arena-menuID", "")
                    .toItemStack());
        }

        player.getBukkitPlayer().openInventory(inv);
    }

    @Override
    public void onClick(BattlePlayer player, InventoryClickEvent e) {
        super.onClick(player, e);
        ItemStack item = e.getCurrentItem();

        if (signEquals(item, "#back")) {
            mainMenu.createMenu(player);
        }
    }
}
