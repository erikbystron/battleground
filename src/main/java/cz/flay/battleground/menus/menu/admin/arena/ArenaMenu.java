package cz.flay.battleground.menus.menu.admin.arena;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.menus.Menu;
import cz.flay.battleground.menus.menu.admin.ArenasMenu;
import cz.flay.battleground.menus.menu.admin.arena.border.BorderMenu;
import cz.flay.battleground.menus.menu.admin.arena.border.RadiationMenu;
import cz.flay.battleground.menus.menu.admin.arena.drops.DropsMenu;
import cz.flay.battleground.menus.menu.admin.arena.plane.PlaneMenu;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.ItemBuilder;
import cz.flay.battleground.util.Placeholders;
import net.wesjd.anvilgui.AnvilGUI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ArenaMenu extends Menu {

    private final ArenasManager arenasManager;
    private final Menu arenasMenu, planeMenu, borderMenu, radiationMenu, dropsMenu;

    public ArenaMenu(BattleGround plugin, ArenasMenu arenasMenu) {
        super("#arenaMenu", plugin, Message.ARENA_MENU_TITLE);
        this.arenasMenu = arenasMenu;
        arenasManager = plugin.getArenasManager();
        planeMenu = new PlaneMenu(plugin, this);
        dropsMenu = new DropsMenu(plugin, this);
        borderMenu = new BorderMenu(plugin, this);
        radiationMenu = new RadiationMenu(plugin, this);
    }

    public void createMenu(BattlePlayer player, Arena arena) {
        Inventory inv = Bukkit.getServer().createInventory(null, 27, getName(player));

        inv.setItem(0, getBackItem(player));

        Placeholders placeholders = new Placeholders()
                .add("%arena_name%", arena.getName())
                .add("%arena_id%", arena.getId());

        inv.setItem(4, new ItemBuilder(Material.GRASS)
                .setName(localeManager.getMessage(player, Message.ARENA_INFO_ITEM_NAME, placeholders))
                .addLore(localeManager.getMessagesList(player, Message.ARENA_INFO_ITEM_LORE, placeholders))
                .addNBTTag("bg-sign", "#arenaItem" + menuID)
                .addNBTTag("bg-arena-id", arena.getId())
                .toItemStack());

        inv.setItem(10, new ItemBuilder(Material.FEATHER)
                .setName(localeManager.getMessage(player, Message.PLANE_ITEM_NAME, placeholders))
                .addLore(localeManager.getMessagesList(player, Message.PLANE_ITEM_LORE, placeholders))
                .addNBTTag("bg-sign", "#planeItem" + menuID)
                .toItemStack());

        inv.setItem(12, new ItemBuilder(Material.CHEST)
                .setName(localeManager.getMessage(player, Message.DROPS_ITEM_NAME, placeholders))
                .addLore(localeManager.getMessagesList(player, Message.DROPS_ITEM_LORE, placeholders))
                .addNBTTag("bg-sign", "#dropsItem" + menuID)
                .toItemStack());

        inv.setItem(14, new ItemBuilder(Material.BARRIER)
                .setName(localeManager.getMessage(player, Message.BORDER_ITEM_NAME, placeholders))
                .addLore(localeManager.getMessagesList(player, Message.BORDER_ITEM_LORE, placeholders))
                .addNBTTag("bg-sign", "#borderItem" + menuID)
                .toItemStack());

        inv.setItem(16, new ItemBuilder(Material.BEACON)
                .setName(localeManager.getMessage(player, Message.RADIATION_ITEM_NAME, placeholders))
                .addLore(localeManager.getMessagesList(player, Message.RADIATION_ITEM_LORE, placeholders))
                .addNBTTag("bg-sign", "#radiationItem" + menuID)
                .toItemStack());

        player.getBukkitPlayer().openInventory(inv);
    }

    @Override
    public void onClick(BattlePlayer player, InventoryClickEvent e) {
        ItemStack item = e.getCurrentItem();

        if (signEquals(item, "#back")) {
            arenasMenu.createMenu(player);
        }

        if (signEquals(item, "#arenaItem")) {
            new AnvilGUI(plugin, player.getBukkitPlayer(), getArena(e.getInventory()).getName(), (p, reply) -> {
                if (reply != null) {
                    if (reply.length() > 0 && !arenasManager.isNameUsed(reply)) {
                        getArena(e.getInventory()).setName(reply);
                        createMenu(player, getArena(e.getInventory()));
                    } else {
                        return localeManager.getConsoleMessage(Message.ARENA_NAME_ALREADY_USED_INSERT);
                    }
                }
                return "";
            }, (p, reply) -> {
                createMenu(player, getArena(e.getInventory()));
                return null;
            });
        }

        if (signEquals(item, "#planeItem")) {
            ((PlaneMenu) planeMenu).createMenu(player, getArena(e.getInventory()));
        }

        if (signEquals(item, "#borderItem")) {
            ((BorderMenu) borderMenu).createMenu(player, getArena(e.getInventory()));
        }

        if (signEquals(item, "#dropsItem")) {
            ((DropsMenu) dropsMenu).createMenu(player, getArena(e.getInventory()));
        }

        if (signEquals(item, "#radiationItem")) {
            ((RadiationMenu) radiationMenu).createMenu(player, getArena(e.getInventory()));
        }
    }

    private Arena getArena(Inventory inv) {
        return arenasManager.getArena(getNBTItem(inv.getItem(4)).getString("bg-arena-id"));
    }
}
