package cz.flay.battleground.menus.menu.admin;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.menus.Menu;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ConfigMenu extends Menu {

    private final Menu mainMenu;

    public ConfigMenu(BattleGround plugin, MainMenu mainMenu) {
        super("#configMenu", plugin, Message.CONFIG_MENU_TITLE);
        this.mainMenu = mainMenu;
    }

    @Override
    public void createMenu(BattlePlayer player) {
        Inventory inv = Bukkit.getServer().createInventory(null, 27, getName(player));

        inv.setItem(0, getBackItem(player));

        player.getBukkitPlayer().openInventory(inv);
    }

    @Override
    public void onClick(BattlePlayer player, InventoryClickEvent e) {
        ItemStack item = e.getCurrentItem();

        if (signEquals(item, "#back")) {
            mainMenu.createMenu(player);
        }
    }
}
