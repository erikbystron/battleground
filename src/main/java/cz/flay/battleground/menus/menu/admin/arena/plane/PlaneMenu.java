package cz.flay.battleground.menus.menu.admin.arena.plane;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.menus.Menu;
import cz.flay.battleground.menus.menu.admin.arena.ArenaMenu;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.ItemBuilder;
import cz.flay.battleground.util.Placeholders;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class PlaneMenu extends Menu {

    private final ArenasManager arenasManager;
    private final Menu arenaMenu, positionsMenu, routesMenu;

    public PlaneMenu(BattleGround plugin, ArenaMenu arenaMenu) {
        super("#planeMenu", plugin, Message.PLANE_MENU_TITLE);
        this.arenaMenu = arenaMenu;
        arenasManager = plugin.getArenasManager();
        positionsMenu = new PositionsMenu(plugin, this);
        routesMenu = new RoutesMenu(plugin, this);
    }

    public void createMenu(BattlePlayer player, Arena arena) {
        String plusURL = "http://textures.minecraft.net/texture/0a21eb4c57750729a48b88e9bbdb987eb6250a5bc2157b59316f5f1887db5";
        String minusURL = "http://textures.minecraft.net/texture/a8c67fed7a2472b7e9afd8d772c13db7b82c32ceeff8db977474c11e4611";
        Inventory inv = Bukkit.getServer().createInventory(null, 54, getName(player));

        inv.setItem(0, getBackItem(player));

        inv.setItem(10, new ItemBuilder(plusURL)
                .setName(localeManager.getMessage(player, Message.PLUS_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.PLUS_ITEM_LORE))
                .addNBTTag("bg-sign", "#plusHeight" + menuID)
                .addNBTTag("bg-arena-id", arena.getId())
                .toItemStack());

        Placeholders heightItem = new Placeholders()
                .add("%value%", String.valueOf(arena.getAirPlane().getHeight()));

        inv.setItem(11, new ItemBuilder("http://textures.minecraft.net/texture/3040fe836a6c2fbd2c7a9c8ec6be5174fddf1ac20f55e366156fa5f712e10")
                .setName(localeManager.getMessage(player, Message.PLANE_HEIGHT_ITEM_NAME, heightItem))
                .addLore(localeManager.getMessagesList(player, Message.PLANE_HEIGHT_ITEM_LORE, heightItem))
                .addNBTTag("bg-sign", "#heightItem" + menuID)
                .toItemStack());

        inv.setItem(12, new ItemBuilder(minusURL)
                .setName(localeManager.getMessage(player, Message.MINUS_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.MINUS_ITEM_LORE))
                .addNBTTag("bg-sign", "#minusHeight" + menuID)
                .toItemStack());

        Placeholders tptimeItem = new Placeholders()
                .add("%value%", String.valueOf(arena.getAirPlane().getTptime()));

        inv.setItem(15, new ItemBuilder("http://textures.minecraft.net/texture/1562e8c1d66b21e459be9a24e5c027a34d269bdce4fbee2f7678d2d3ee4718")
                .setName(localeManager.getMessage(player, Message.TP_TIME_ITEM_NAME, tptimeItem))
                .addLore(localeManager.getMessagesList(player, Message.TP_TIME_ITEM_LORE, tptimeItem))
                .addNBTTag("bg-sign", "#tptimeItem" + menuID)
                .toItemStack());

        inv.setItem(16, new ItemBuilder(minusURL)
                .setName(localeManager.getMessage(player, Message.MINUS_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.MINUS_ITEM_LORE))
                .addNBTTag("bg-sign", "#minusTpTime" + menuID)
                .toItemStack());

        inv.setItem(14, new ItemBuilder(plusURL)
                .setName(localeManager.getMessage(player, Message.PLUS_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.PLUS_ITEM_LORE))
                .addNBTTag("bg-sign", "#plusTpTime" + menuID)
                .toItemStack());

        inv.setItem(23, new ItemBuilder(plusURL)
                .setName(localeManager.getMessage(player, Message.PLUS_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.PLUS_ITEM_LORE))
                .addNBTTag("bg-sign", "#plusAutoOpen" + menuID)
                .toItemStack());

        Placeholders autoopenItem = new Placeholders()
                .add("%value%", String.valueOf(arena.getAirPlane().getAutoOpenHeight()));

        inv.setItem(24, new ItemBuilder("http://textures.minecraft.net/texture/a67d813ae7ffe5be951a4f41f2aa619a5e3894e85ea5d4986f84949c63d7672e")
                .setName(localeManager.getMessage(player, Message.AUTO_OPEN_HEIGHT_ITEM_NAME, autoopenItem))
                .addLore(localeManager.getMessagesList(player, Message.AUTO_OPEN_HEIGHT_ITEM_LORE, autoopenItem))
                .addNBTTag("bg-sign", "#autoopenItem" + menuID)
                .toItemStack());

        inv.setItem(25, new ItemBuilder(minusURL)
                .setName(localeManager.getMessage(player, Message.MINUS_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.MINUS_ITEM_LORE))
                .addNBTTag("bg-sign", "#minusAutoOpen" + menuID)
                .toItemStack());

        inv.setItem(19, new ItemBuilder(plusURL)
                .setName(localeManager.getMessage(player, Message.PLUS_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.PLUS_ITEM_LORE))
                .addNBTTag("bg-sign", "#plusSpeed" + menuID)
                .toItemStack());

        Placeholders speedItem = new Placeholders()
                .add("%value%", String.valueOf(arena.getAirPlane().getSpeed()));

        inv.setItem(20, new ItemBuilder("http://textures.minecraft.net/texture/3e41c60572c533e93ca421228929e54d6c856529459249c25c32ba33a1b1517")
                .setName(localeManager.getMessage(player, Message.SPEED_ITEM_NAME, speedItem))
                .addLore(localeManager.getMessagesList(player, Message.SPEED_ITEM_LORE, speedItem))
                .addNBTTag("bg-sign", "#speedItem" + menuID)
                .toItemStack());

        inv.setItem(21, new ItemBuilder(minusURL)
                .setName(localeManager.getMessage(player, Message.MINUS_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.MINUS_ITEM_LORE))
                .addNBTTag("bg-sign", "#minusSpeed" + menuID)
                .toItemStack());

        inv.setItem(38, new ItemBuilder(Material.SKULL_ITEM, (byte) 3)
                .setName(localeManager.getMessage(player, Message.POSITIONS_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.POSITIONS_ITEM_LORE))
                .addNBTTag("bg-sign", "#positionsItem" + menuID)
                .toItemStack());

        inv.setItem(42, new ItemBuilder(Material.COMPASS)
                .setName(localeManager.getMessage(player, Message.ROUTES_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.ROUTES_ITEM_LORE))
                .addNBTTag("bg-sign", "#routesItem" + menuID)
                .toItemStack());

        player.getBukkitPlayer().openInventory(inv);
    }

    @Override
    public void onClick(BattlePlayer player, InventoryClickEvent e) {
        ItemStack item = e.getCurrentItem();
        ClickType click = e.getClick();

        if (signEquals(item, "#back")) {
            ((ArenaMenu) arenaMenu).createMenu(player, getArena(e.getInventory()));
            return;
        }

        if (signEquals(item, "#positionsItem")) {
            ((PositionsMenu) positionsMenu).createMenu(player, getArena(e.getInventory()));
            return;
        }

        if (signEquals(item, "#routesItem")) {
            ((RoutesMenu) routesMenu).createMenu(player, getArena(e.getInventory()));
            return;
        }

        if (signEquals(item, "#plusHeight")) {
            int height = getArena(e.getInventory()).getAirPlane().getHeight();

            if (click.isRightClick() && !click.isShiftClick()) {
                height += 1;
            }

            if (click.isLeftClick() && !click.isShiftClick()) {
                height += 5;
            }

            if (click.isShiftClick() && click.isRightClick()) {
                height += 50;
            }

            if (click.isShiftClick() && click.isLeftClick()) {
                height += 100;
            }

            getArena(e.getInventory()).getAirPlane().setHeight(height);
        }

        if (signEquals(item, "#heightItem")) {

        }

        if (signEquals(item, "#minusHeight")) {
            int height = getArena(e.getInventory()).getAirPlane().getHeight();

            if (click.isRightClick() && !click.isShiftClick()) {
                height -= 1;
            }

            if (click.isLeftClick() && !click.isShiftClick()) {
                height -= 5;
            }

            if (click.isShiftClick() && click.isRightClick()) {
                height -= 50;
            }

            if (click.isShiftClick() && click.isLeftClick()) {
                height -= 100;
            }

            getArena(e.getInventory()).getAirPlane().setHeight(height);
        }

        if (signEquals(item, "#plusTpTime")) {
            int tptime = getArena(e.getInventory()).getAirPlane().getTptime();

            if (click.isRightClick() && !click.isShiftClick()) {
                tptime += 1;
            }

            if (click.isLeftClick() && !click.isShiftClick()) {
                tptime += 5;
            }

            if (click.isShiftClick() && click.isRightClick()) {
                tptime += 50;
            }

            if (click.isShiftClick() && click.isLeftClick()) {
                tptime += 100;
            }

            getArena(e.getInventory()).getAirPlane().setTptime(tptime);
        }

        if (signEquals(item, "#tptimeItem")) {

        }

        if (signEquals(item, "#minusTpTime")) {
            int tptime = getArena(e.getInventory()).getAirPlane().getTptime();

            if (click.isRightClick() && !click.isShiftClick()) {
                tptime -= 1;
            }

            if (click.isLeftClick() && !click.isShiftClick()) {
                tptime -= 5;
            }

            if (click.isShiftClick() && click.isRightClick()) {
                tptime -= 50;
            }

            if (click.isShiftClick() && click.isLeftClick()) {
                tptime -= 100;
            }

            getArena(e.getInventory()).getAirPlane().setTptime(tptime);
        }

        if (signEquals(item, "#plusAutoOpen")) {
            int autoOpen = getArena(e.getInventory()).getAirPlane().getAutoOpenHeight();

            if (click.isRightClick() && !click.isShiftClick()) {
                autoOpen += 1;
            }

            if (click.isLeftClick() && !click.isShiftClick()) {
                autoOpen += 5;
            }

            if (click.isShiftClick() && click.isRightClick()) {
                autoOpen += 50;
            }

            if (click.isShiftClick() && click.isLeftClick()) {
                autoOpen += 100;
            }

            getArena(e.getInventory()).getAirPlane().setAutoOpenHeight(autoOpen);
        }

        if (signEquals(item, "#autoopenItem")) {

        }

        if (signEquals(item, "#minusAutoOpen")) {
            int autoOpen = getArena(e.getInventory()).getAirPlane().getAutoOpenHeight();

            if (click.isRightClick() && !click.isShiftClick()) {
                autoOpen -= 1;
            }

            if (click.isLeftClick() && !click.isShiftClick()) {
                autoOpen -= 5;
            }

            if (click.isShiftClick() && click.isRightClick()) {
                autoOpen -= 50;
            }

            if (click.isShiftClick() && click.isLeftClick()) {
                autoOpen -= 100;
            }

            getArena(e.getInventory()).getAirPlane().setAutoOpenHeight(autoOpen);
        }

        if (signEquals(item, "#plusSpeed")) {
            int speed = getArena(e.getInventory()).getAirPlane().getSpeed();

            if (click.isRightClick() && !click.isShiftClick()) {
                speed += 1;
            }

            if (click.isLeftClick() && !click.isShiftClick()) {
                speed += 5;
            }

            if (click.isShiftClick() && click.isRightClick()) {
                speed += 10;
            }

            if (click.isShiftClick() && click.isLeftClick()) {
                speed += 20;
            }

            if (speed > 20) {
                speed = 20;
            }

            getArena(e.getInventory()).getAirPlane().setSpeed(speed);
        }

        if (signEquals(item, "#speedItem")) {

        }

        if (signEquals(item, "#minusSpeed")) {
            int speed = getArena(e.getInventory()).getAirPlane().getSpeed();

            if (click.isRightClick() && !click.isShiftClick()) {
                speed -= 1;
            }

            if (click.isLeftClick() && !click.isShiftClick()) {
                speed -= 5;
            }

            if (click.isShiftClick() && click.isRightClick()) {
                speed -= 10;
            }

            if (click.isShiftClick() && click.isLeftClick()) {
                speed -= 20;
            }

            if (speed < 1) {
                speed = 1;
            }

            getArena(e.getInventory()).getAirPlane().setSpeed(speed);
        }

        createMenu(player, getArena(e.getInventory()));
    }

    private Arena getArena(Inventory inv) {
        return arenasManager.getArena(getNBTItem(inv.getItem(10)).getString("bg-arena-id"));
    }
}
