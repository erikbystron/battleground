package cz.flay.battleground.menus.menu.admin.arena.plane;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.menus.Menu;
import cz.flay.battleground.menus.PageableMenu;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class PositionsMenu extends PageableMenu {

    private final ArenasManager arenasManager;
    private final Menu planeMenu, positionMenu;

    PositionsMenu(BattleGround plugin, PlaneMenu planeMenu) {
        super("#positionsMenu", plugin, Message.POSITIONS_MENU_TITLE);
        this.planeMenu = planeMenu;
        positionMenu = new PositionMenu(plugin, this);
        arenasManager = plugin.getArenasManager();
    }

    public void createMenu(BattlePlayer player, Arena arena) {
        createMenu(player, 1, arena);
    }

    public void createMenu(BattlePlayer player, int page, Arena arena) {
        List<Location> positions = menuUtils.getPagePositions(arena.getAirPlane().getPositions(), page);
        Inventory inv = Bukkit.getServer().createInventory(null, menuUtils.getSize(positions.size()), getName(player));

        inv = addPagination(inv, page, menuUtils.neededPages(positions.size()));

        inv.setItem(0, getBackItem(player));

        inv.setItem(4, new ItemBuilder(Material.SKULL_ITEM, (byte) 3)
                .addNBTTag("bg-sign", "#newItem" + menuID)
                .addNBTTag("bg-arena-id", arena.getId())
                .toItemStack());

        int[] pos = menuUtils.getPositions(menuUtils.getSize(positions.size()));
        for (Location location : positions) {
            inv.setItem(pos[positions.indexOf(location)],
                    new ItemBuilder(Material.FIREWORK_CHARGE)
                            .addNBTTag("bg-sign", "#positionItem" + menuID)
                            .addNBTTag("bg-position-id", String.valueOf(positions.indexOf(location)))
                            .toItemStack());
        }

        player.getBukkitPlayer().openInventory(inv);
    }

    @Override
    public void onClick(BattlePlayer player, InventoryClickEvent e) {
        ItemStack item = e.getCurrentItem();

        if (signEquals(item, "#back")) {
            ((PlaneMenu) planeMenu).createMenu(player, getArena(e.getInventory()));
        }

        if (signEquals(item, "#newItem")) {
            if (e.getClick().isLeftClick()) {
                Location location = new Location(getArena(e.getInventory()).getWorld(), 0,0,0);
                    getArena(e.getInventory()).getAirPlane().addPosition(location);
                ((PositionMenu) positionMenu).createMenu(player, getArena(e.getInventory()), location);
            }

            if (e.getClick().isRightClick()) {
                //TODO Givnout hraci item pro editaci
            }
        }

        if (signEquals(item, "#positionItem")) {
            int id = Integer.valueOf(getNBTItem(item).getString("bg-position-id"));
            ((PositionMenu) positionMenu).createMenu(player, getArena(e.getInventory()),
                    getArena(e.getInventory()).getAirPlane().getPositions().get(id));
        }
    }

    private Arena getArena(Inventory inv) {
        return arenasManager.getArena(getNBTItem(inv.getItem(4)).getString("bg-arena-id"));
    }
}
