package cz.flay.battleground.menus.menu.admin.item;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.menus.Menu;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ItemMenu extends Menu {

    public ItemMenu(BattleGround plugin) {
        super("#itemMenu", plugin, Message.ITEM_MENU_TITLE);
    }

    @Override
    public void createMenu(BattlePlayer player) {
        Inventory inv = Bukkit.getServer().createInventory(null, 27, getName(player));
        player.getBukkitPlayer().openInventory(inv);
    }

    @Override
    public void onClick(BattlePlayer player, InventoryClickEvent e) {
        ItemStack item = e.getCurrentItem();
    }
}
