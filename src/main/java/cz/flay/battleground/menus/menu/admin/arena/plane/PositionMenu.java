package cz.flay.battleground.menus.menu.admin.arena.plane;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.menus.Menu;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class PositionMenu extends Menu {

    private final ArenasManager arenasManager;
    private final PositionsMenu positionsMenu;

    public PositionMenu(BattleGround plugin, PositionsMenu positionsMenu) {
        super("#positionMenu", plugin, Message.POSITION_MENU_TITLE);
        this.positionsMenu = positionsMenu;
        arenasManager = plugin.getArenasManager();
    }

    public void createMenu(BattlePlayer player, Arena arena, Location location) {
        String plusURL = "http://textures.minecraft.net/texture/0a21eb4c57750729a48b88e9bbdb987eb6250a5bc2157b59316f5f1887db5";
        String minusURL = "http://textures.minecraft.net/texture/a8c67fed7a2472b7e9afd8d772c13db7b82c32ceeff8db977474c11e4611";
        Inventory inv = Bukkit.getServer().createInventory(null, 54, getName(player));

        inv.setItem(0, getBackItem(player));

        inv.setItem(3, new ItemBuilder("http://textures.minecraft.net/texture/b1dd4fe4a429abd665dfdb3e21321d6efa6a6b5e7b956db9c5d59c9efab25")
                .addNBTTag("bg-sign", "#locationItem" + menuID)
                .addNBTTag("bg-arena-id", arena.getId())
                .addNBTTag("bg-position-id", String.valueOf(arena.getAirPlane().getPositions().indexOf(location)))
                .toItemStack());

        inv.setItem(5, new ItemBuilder(Material.BOOK_AND_QUILL)
                .addNBTTag("bg-sign", "#editItem" + menuID)
                .toItemStack());

        inv.setItem(19, new ItemBuilder(plusURL)
                .addNBTTag("bg-sign", "#plusX")
                .toItemStack());

        inv.setItem(20, new ItemBuilder("http://textures.minecraft.net/texture/5a6787ba32564e7c2f3a0ce64498ecbb23b89845e5a66b5cec7736f729ed37")
                .addNBTTag("bg-sign", "#xItem")
                .toItemStack());

        inv.setItem(21, new ItemBuilder(minusURL)
                .addNBTTag("bg-sign", "#minusX")
                .toItemStack());

        inv.setItem(28, new ItemBuilder(plusURL)
                .addNBTTag("bg-sign", "#plusY")
                .toItemStack());

        inv.setItem(29, new ItemBuilder("http://textures.minecraft.net/texture/c52fb388e33212a2478b5e15a96f27aca6c62ac719e1e5f87a1cf0de7b15e918")
                .addNBTTag("bg-sign", "#yItem")
                .toItemStack());

        inv.setItem(30, new ItemBuilder(minusURL)
                .addNBTTag("bg-sign", "#minusY")
                .toItemStack());

        inv.setItem(37, new ItemBuilder(plusURL)
                .addNBTTag("bg-sign", "#plusZ")
                .toItemStack());

        inv.setItem(38, new ItemBuilder("http://textures.minecraft.net/texture/90582b9b5d97974b11461d63eced85f438a3eef5dc3279f9c47e1e38ea54ae8d")
                .addNBTTag("bg-sign", "#zItem")
                .toItemStack());

        inv.setItem(39, new ItemBuilder(minusURL)
                .addNBTTag("bg-sign", "#minusZ")
                .toItemStack());

        inv.setItem(23, new ItemBuilder(plusURL)
                .addNBTTag("bg-sign", "#plusYaw")
                .toItemStack());

        inv.setItem(24, new ItemBuilder("http://textures.minecraft.net/texture/55a117e161f7a291d0a3a168e77a21b09d39ffdf5773d22ac02f5fa6611db67")
                .addNBTTag("bg-sign", "#yawItem")
                .toItemStack());

        inv.setItem(25, new ItemBuilder(minusURL)
                .addNBTTag("bg-sign", "#minusYaw")
                .toItemStack());

        inv.setItem(32, new ItemBuilder(plusURL)
                .addNBTTag("bg-sign", "#plusPitch")
                .toItemStack());

        inv.setItem(33, new ItemBuilder("http://textures.minecraft.net/texture/7c116694731fbd272c1ffa4352a5359b6c3a4cb5864a74a5dbe0f665f8385c")
                .addNBTTag("bg-sign", "#picthItem")
                .toItemStack());

        inv.setItem(34, new ItemBuilder(minusURL)
                .addNBTTag("bg-sign", "#minusPitch")
                .toItemStack());

        player.getBukkitPlayer().openInventory(inv);
    }

    @Override
    public void onClick(BattlePlayer player, InventoryClickEvent e) {
        ItemStack item = e.getCurrentItem();

        if (signEquals(item, "#back")) {
            positionsMenu.createMenu(player, getArena(e.getInventory()));
        }
    }

    private Arena getArena(Inventory inv) {
        return arenasManager.getArena(getNBTItem(inv.getItem(3)).getString("bg-arena-id"));
    }
}
