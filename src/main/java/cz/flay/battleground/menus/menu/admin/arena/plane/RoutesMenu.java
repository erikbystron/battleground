package cz.flay.battleground.menus.menu.admin.arena.plane;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.arena.plane.PlaneRoute;
import cz.flay.battleground.menus.Menu;
import cz.flay.battleground.menus.PageableMenu;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class RoutesMenu extends PageableMenu {

    private final ArenasManager arenasManager;
    private final Menu planeMenu, routeMenu;

    RoutesMenu(BattleGround plugin, PlaneMenu planeMenu) {
        super("#routesMenu", plugin, Message.ROUTES_MENU_TITLE);
        this.planeMenu = planeMenu;
        routeMenu = new RouteMenu(plugin, this);
        arenasManager = plugin.getArenasManager();
    }

    public void createMenu(BattlePlayer player, Arena arena) {
        createMenu(player, 1, arena);
    }

    public void createMenu(BattlePlayer player, int page, Arena arena) {
        List<PlaneRoute> routes = arena.getAirPlane().getRoutes();
        Inventory inv = Bukkit.getServer().createInventory(null, menuUtils.getSize(routes.size()), getName(player));

        inv = addPagination(inv, page, menuUtils.neededPages(routes.size()));

        inv.setItem(0, getBackItem(player));

        inv.setItem(4, new ItemBuilder(Material.COMPASS)
                .addNBTTag("bg-sign", "#newItem" + menuID)
                .addNBTTag("bg-arena-id", arena.getId())
                .toItemStack());

        int[] pos = menuUtils.getPositions(menuUtils.getSize(routes.size()));
        for (PlaneRoute route : routes) {
            inv.setItem(pos[routes.indexOf(route)],
                    new ItemBuilder(Material.FIREWORK_CHARGE)
                            .addNBTTag("bg-sign", "#routeItem" + menuID)
                            .addNBTTag("bg-route-id", String.valueOf(routes.indexOf(route)))
                            .toItemStack());
        }

        player.getBukkitPlayer().openInventory(inv);
    }

    @Override
    public void onClick(BattlePlayer player, InventoryClickEvent e) {
        ItemStack item = e.getCurrentItem();

        if (signEquals(item, "#back")) {
            ((PlaneMenu) planeMenu).createMenu(player, getArena(e.getInventory()));
        }

        if (signEquals(item, "#newItem")) {
            PlaneRoute route = new PlaneRoute();
                getArena(e.getInventory()).getAirPlane().addRoute(route);
            ((RouteMenu) routeMenu).createMenu(player, getArena(e.getInventory()), route);
        }

        if (signEquals(item, "#routeItem")) {
            int id = Integer.valueOf(getNBTItem(item).getString("bg-route-id"));
            ((RouteMenu) routeMenu).createMenu(player, getArena(e.getInventory()),
                    getArena(e.getInventory()).getAirPlane().getRoutes().get(id));
        }
    }

    private Arena getArena(Inventory inv) {
        return arenasManager.getArena(getNBTItem(inv.getItem(4)).getString("bg-arena-id"));
    }
}
