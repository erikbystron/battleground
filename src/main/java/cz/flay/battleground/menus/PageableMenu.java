package cz.flay.battleground.menus;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.ItemBuilder;
import de.tr7zw.itemnbtapi.NBTItem;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class PageableMenu extends Menu {

    protected PageableMenu(String menuID, BattleGround plugin, Message name) {
        super(menuID, plugin, name);
    }

    @Override
    public void createMenu(BattlePlayer player) {
        createMenu(player, 1);
    }

    public void createMenu(BattlePlayer player, int page) {

    }

    protected Inventory addPagination(Inventory inv, int page, int maxPage) {
        int firstPos = 18, secondPos = 26;
        int size = inv.getSize();
        if (size == 27) {
            firstPos = 18;
            secondPos = 26;
        } else if (size == 36) {
            firstPos = 27;
            secondPos = 35;
        } else if (size == 45) {
            firstPos = 36;
            secondPos = 44;
        } else if (size == 54) {
            firstPos = 45;
            secondPos = 53;
        }

        if (page != 1) {
            inv.setItem(firstPos, new ItemBuilder("http://textures.minecraft.net/texture/bb0f6e8af46ac6faf88914191ab66f261d6726a7999c637cf2e4159fe1fc477")
                    .addNBTTag("bg-sign", "#pageItem")
                    .addNBTTag("bg-toPage", String.valueOf(page - 1))
                    .toItemStack());
        }

        if (page != maxPage) {
            inv.setItem(secondPos, new ItemBuilder("http://textures.minecraft.net/texture/f2f3a2dfce0c3dab7ee10db385e5229f1a39534a8ba2646178e37c4fa93b")
                    .addNBTTag("bg-sign", "#pageItem")
                    .addNBTTag("bg-toPage", String.valueOf(page + 1))
                    .toItemStack());
        }
        return inv;
    }

    @Override
    protected void onClick(BattlePlayer player, InventoryClickEvent e) {
        ItemStack item = e.getCurrentItem();

        if (new NBTItem(item).getString("bg-sign").equals("#pageItem")) {
            createMenu(player, Integer.valueOf(new NBTItem(item).getString("bg-toPage")));
        }
    }

}
