package cz.flay.battleground.menus;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.LocaleManager;
import cz.flay.battleground.storage.locale.Message;
import cz.flay.battleground.util.ItemBuilder;
import cz.flay.battleground.util.MenuUtils;
import de.tr7zw.itemnbtapi.NBTItem;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class Menu implements Listener {

    protected final String menuID;

    protected final BattleGround plugin;
    protected final LocaleManager localeManager;
    protected final MenuUtils menuUtils;
    private final Message name;

    protected Menu(String menuID, BattleGround plugin, Message name) {
        this.plugin = plugin;
        localeManager = plugin.getLocaleManager();
        menuUtils = new MenuUtils();
        this.name = name;
        this.menuID = menuID;

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    public void createMenu(BattlePlayer player){

    }

    protected String getName(BattlePlayer player){
        String title = localeManager.getMessage(player, name);

        if (title.length() > 32) {
            return title.substring(0, 32);
        }

        return title;
    }

    protected void onClick(BattlePlayer player, InventoryClickEvent e) {

    }

    protected ItemStack getBackItem(BattlePlayer player) {
        return new ItemBuilder("http://textures.minecraft.net/texture/473e1fe0ffcd546c21b63eaa910ab5c93132a64e644179fccaa21dc8bc660")
                .setName(localeManager.getMessage(player, Message.BACK_ITEM_NAME))
                .addLore(localeManager.getMessagesList(player, Message.BACK_ITEM_LORE))
                .addNBTTag("bg-sign", "#back" + menuID)
                .toItemStack();
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent e){
        if (e.getInventory().getName().equals(getName(plugin.getBattlePlayer((Player) e.getWhoClicked())))) {
            e.setCancelled(true);

            if (e.getCurrentItem() == null || !e.getCurrentItem().hasItemMeta()) {
                return;
            }

            onClick(plugin.getBattlePlayer((Player) e.getWhoClicked()), e);
        }
    }

    protected NBTItem getNBTItem(ItemStack itemStack) {
        return new NBTItem(itemStack);
    }

    protected boolean signEquals(ItemStack itemStack, String sign) {
        return getNBTItem(itemStack).getString("bg-sign").equals(sign + menuID);
    }
}
