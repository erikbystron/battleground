package cz.flay.battleground;

import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.arena.border.types.radiation.zones.states.CircleZoneState;
import cz.flay.battleground.arena.plane.PlaneRoute;
import cz.flay.battleground.arena.reseting.MapChange;
import cz.flay.battleground.commands.CommandsManager;
import cz.flay.battleground.game.GamesManager;
import cz.flay.battleground.items.GameItemsManager;
import cz.flay.battleground.listeners.ListenersManager;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.player.BattlePlayerManager;
import cz.flay.battleground.player.corpse.CorpsesManager;
import cz.flay.battleground.player.inventory.InventoriesManager;
import cz.flay.battleground.player.inventory.InventoryItem;
import cz.flay.battleground.player.scoreboard.ScoreBoardsManager;
import cz.flay.battleground.storage.StorageManager;
import cz.flay.battleground.storage.config.ConfigManager;
import cz.flay.battleground.storage.locale.LocaleManager;
import cz.flay.battleground.storage.locale.managers.CustomManager;
import cz.flay.battleground.storage.locale.managers.LanguageAPIManager;
import org.bukkit.Bukkit;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import static cz.flay.battleground.Logger.*;

public class BattleGround extends JavaPlugin {

    private BattlePlayerManager battlePlayerManager;

    private ArenasManager arenasManager;
    private GamesManager gamesManager;

    private ConfigManager configManager;
    private LocaleManager localeManager;
    private StorageManager storageManager;

    private GameItemsManager itemsManager;
    private InventoriesManager inventoriesManager;
    private ScoreBoardsManager scoreBoardsManager;
    private CorpsesManager corpsesManager;

    public final static String VERSION = "1.0";

    static  {
        ConfigurationSerialization.registerClass(PlaneRoute.class, "PlaneRoute");
        ConfigurationSerialization.registerClass(MapChange.class, "MapChange");
        ConfigurationSerialization.registerClass(InventoryItem.class, "InventoryItem");
        ConfigurationSerialization.registerClass(CircleZoneState.class, "ZoneState-Circle");
    }

    @Override
    public void onEnable() {
        line();
        configManager = new ConfigManager();
        if (Bukkit.getPluginManager().isPluginEnabled("LanguageAPI")) {
            log("Found LanguageAPI, using LanguageAPI implementation");
            localeManager = new LanguageAPIManager(this);
        } else {
            log("LanguageAPI not found, using build-in implementation");
            localeManager = new CustomManager(this);
        }
        storageManager = new StorageManager(this);

        arenasManager = new ArenasManager(this);
        gamesManager = new GamesManager(this);

        itemsManager = new GameItemsManager(this);
        inventoriesManager = new InventoriesManager(this);
        scoreBoardsManager = new ScoreBoardsManager(this);

        battlePlayerManager = new BattlePlayerManager(this);
        new ListenersManager(this);
        new CommandsManager(this);
        corpsesManager = new CorpsesManager();

        log("BattleGround has been enabled ! Version: " + VERSION);

        if (Bukkit.getOnlinePlayers().size() != 0) {
            log("Detected " + String.valueOf(Bukkit.getOnlinePlayers().size()) + " online players, loading properties");
        }

        for (Player player : Bukkit.getOnlinePlayers()) {
            battlePlayerManager.getPlayer(player);
        }
        line();
    }

    @Override
    public void onDisable() {

        arenasManager.saveAll();

        log("Has been disabled !");
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }
    public LocaleManager getLocaleManager() {
        return localeManager;
    }
    public BattlePlayer getBattlePlayer(Player player){
        return battlePlayerManager.getPlayer(player);
    }
    public ArenasManager getArenasManager() {
        return arenasManager;
    }
    public GamesManager getGamesManager() {
        return gamesManager;
    }
    public GameItemsManager getItemsManager() {
        return itemsManager;
    }
    public StorageManager getStorageManager() {
        return storageManager;
    }
    public InventoriesManager getInventoriesManager() {
        return inventoriesManager;
    }
    public ScoreBoardsManager getScoreBoardsManager() {
        return scoreBoardsManager;
    }
    public CorpsesManager getCorpsesManager() {
        return corpsesManager;
    }
}
