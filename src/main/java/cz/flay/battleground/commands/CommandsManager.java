package cz.flay.battleground.commands;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.commands.subcommands.*;
import cz.flay.battleground.commands.subcommands.arenaCommands.*;
import cz.flay.battleground.commands.subcommands.chestCommands.ChestCommandEditor;
import cz.flay.battleground.commands.subcommands.gameCommands.GameCommandNew;
import cz.flay.battleground.commands.subcommands.planeCommands.PlaneCommandPositions;
import cz.flay.battleground.commands.subcommands.planeCommands.PlaneCommandTimer;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.LocaleManager;
import cz.flay.battleground.storage.locale.Message;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandsManager implements CommandExecutor {

    private final BattleGround plugin;
    private final LocaleManager localeManager;

    private final List<SubCommand> subCommands;
    private final List<SubCommand> gameCommands;
    private final List<SubCommand> arenaCommands;
    private final List<SubCommand> chestCommands;
    private final List<SubCommand> planeCommands;

    public CommandsManager(BattleGround plugin){
        this.plugin = plugin;
        localeManager = plugin.getLocaleManager();
        subCommands = new ArrayList<>();
        gameCommands = new ArrayList<>();
        arenaCommands = new ArrayList<>();
        chestCommands = new ArrayList<>();
        planeCommands = new ArrayList<>();
        registerCommands();

        plugin.getCommand("bg").setExecutor(this);
        plugin.getCommand("bgarena").setExecutor(this);
        plugin.getCommand("bggame").setExecutor(this);
        plugin.getCommand("bgchest").setExecutor(this);
        plugin.getCommand("bgplane").setExecutor(this);
    }

    private void sendMessage(CommandSender sender, Message message) {
        if (sender instanceof Player) {
            BattlePlayer player = plugin.getBattlePlayer((Player) sender);
            sender.sendMessage(localeManager.getMessage(player, message));
        } else {
            sender.sendMessage(localeManager.getConsoleMessage(message));
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!(sender instanceof Player) && !(sender instanceof ConsoleCommandSender)){
            return false;
        }

        if (args == null || args.length == 0){
            sendMessage(sender, Message.USE_HELP);
            return true;
        }

        if (cmd.getName().equals("bg")) {
            for (SubCommand subCommand : subCommands) {
                if (check(subCommand, args, sender)) {
                    return true;
                }
            }
        }

        if (cmd.getName().equals("bgarena")) {
            for (SubCommand subCommand : arenaCommands) {
                if (check(subCommand, args, sender)) {
                    return true;
                }
            }
        }

        if (cmd.getName().equals("bggame")) {
            for (SubCommand subCommand : gameCommands) {
                if (check(subCommand, args, sender)) {
                    return true;
                }
            }
        }

        if (cmd.getName().equals("bgchest")) {
            for (SubCommand subCommand : chestCommands) {
                if (check(subCommand, args, sender)) {
                    return true;
                }
            }
        }

        if (cmd.getName().equals("bgplane")) {
            for (SubCommand subCommand : planeCommands) {
                if (check(subCommand, args, sender)) {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean check(SubCommand subCommand, String[] args, CommandSender sender) {
        if (subCommand.check(args[0])){
            if (!sender.hasPermission(subCommand.getPermission())){
                sendMessage(sender, Message.NO_PERMISSIONS);
            }

            if (sender instanceof Player){
                subCommand.onExePlayer((Player) sender, adapt(args));
            } else {
                subCommand.onExeConsole((ConsoleCommandSender) sender, adapt(args));
            }
            return true;
        }
        return false;
    }

    private String[] adapt(String[] args) {
        if (args.length == 1) {
            return new String[0];
        }
        return Arrays.copyOfRange(args, 1 , args.length);
    }

    private void registerCommands() {
        subCommands.add(new SubCommandAdmin(plugin));
        subCommands.add(new SubCommandHelp(plugin));
        subCommands.add(new SubCommandAdminMode(plugin));
        subCommands.add(new SubCommandJoin(plugin));

        arenaCommands.add(new ArenaCommandCenter(plugin));
        arenaCommands.add(new ArenaCommandChest(plugin));
        arenaCommands.add(new ArenaCommandFirstPoint(plugin));
        arenaCommands.add(new ArenaCommandName(plugin));
        arenaCommands.add(new ArenaCommandNew(plugin));
        arenaCommands.add(new ArenaCommandRadius(plugin));
        arenaCommands.add(new ArenaCommandRemove(plugin));
        arenaCommands.add(new ArenaCommandSecondPoint(plugin));
        arenaCommands.add(new ArenaCommandShape(plugin));
        arenaCommands.add(new ArenaCommandMap(plugin));

        gameCommands.add(new GameCommandNew(plugin));

        chestCommands.add(new ChestCommandEditor(plugin));

        planeCommands.add(new PlaneCommandPositions(plugin));
        planeCommands.add(new PlaneCommandTimer(plugin));
    }
}
