package cz.flay.battleground.commands.subcommands;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.commands.SubCommand;
import cz.flay.battleground.player.BattlePlayer;
import org.bukkit.entity.Player;

public class SubCommandAdminMode extends SubCommand {

    public SubCommandAdminMode(BattleGround plugin) {
        super(plugin, "adminmode", "player");
    }

    @Override
    public void onExePlayer(Player player, String[] args) {
        BattlePlayer battlePlayer = plugin.getBattlePlayer(player);

        if (!battlePlayer.hasAdminStatus()) {
            battlePlayer.setAdminStatus();
        } else {
            battlePlayer.removeAdminStatus();
        }
    }
}
