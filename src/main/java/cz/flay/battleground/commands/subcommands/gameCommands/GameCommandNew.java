package cz.flay.battleground.commands.subcommands.gameCommands;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.commands.SubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class GameCommandNew extends SubCommand {

    public GameCommandNew(BattleGround plugin) {
        super(plugin, "new", "game");
    }

    @Override
    public void onExePlayer(Player player, String[] args) {

    }

    @Override
    public void onExeConsole(ConsoleCommandSender commandSender, String[] args) {

    }
}
