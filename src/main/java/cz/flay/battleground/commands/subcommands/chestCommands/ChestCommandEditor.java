package cz.flay.battleground.commands.subcommands.chestCommands;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.commands.SubCommand;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ChestCommandEditor extends SubCommand {

    public ChestCommandEditor(BattleGround plugin) {
        super(plugin, "editor", "chest");
    }

    @Override
    public void onExePlayer(Player player, String[] args) {
        player.setItemInHand(new ItemStack(Material.CHEST));
    }

}
