package cz.flay.battleground.commands.subcommands.arenaCommands;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.commands.SubCommand;
import org.bukkit.entity.Player;

public class ArenaCommandRadius extends SubCommand {

    private final ArenasManager arenasManager;
    //private final YamlConfiguration arenasConfig;

    public ArenaCommandRadius(BattleGround plugin) {
        super(plugin, "radius", "arena");
        //arenasConfig = plugin.getArenasConfig();
        arenasManager = plugin.getArenasManager();
    }

    @Override
    public void onExePlayer(Player player, String[] args) {
        if (args.length >= 2) {
            Arena arena = arenasManager.getArena(args[0]);
            //arenasConfig.set("arenas." + arena.getId() + ".radius", Integer.valueOf(args[1]));
            //arena.getBorder().load(arenasConfig, "arenas." + arena.getId() + ".");
        }
    }
}
