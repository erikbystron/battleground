package cz.flay.battleground.commands.subcommands.arenaCommands;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.commands.SubCommand;
import org.bukkit.entity.Player;

public class ArenaCommandShape extends SubCommand {

    private final ArenasManager arenasManager;

    public ArenaCommandShape(BattleGround plugin) {
        super(plugin, "shape", "arena");
        arenasManager = plugin.getArenasManager();
    }

    @Override
    public void onExePlayer(Player player, String[] args) {
        if (args.length >= 2) {
            arenasManager.getArena(args[0]).setShape(Arena.Shape.valueOf(args[1].toUpperCase()));
        }
    }
}
