package cz.flay.battleground.commands.subcommands.arenaCommands;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.commands.SubCommand;
import org.bukkit.entity.Player;

public class ArenaCommandCenter extends SubCommand {

    private final ArenasManager arenasManager;

    public ArenaCommandCenter(BattleGround plugin) {
        super(plugin, "center", "arena");
        arenasManager = plugin.getArenasManager();
    }

    @Override
    public void onExePlayer(Player player, String[] args) {
        if (args.length >= 1) {
            arenasManager.getArena(args[0]).setCenter(player.getLocation().toVector());
        }
    }
}
