package cz.flay.battleground.commands.subcommands.arenaCommands;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.commands.SubCommand;
import org.bukkit.entity.Player;

public class ArenaCommandSecondPoint extends SubCommand {

    private final ArenasManager arenasManager;
    //private final YamlConfiguration arenasConfig;

    public ArenaCommandSecondPoint(BattleGround plugin) {
        super(plugin, "ndpoint", "arena");
        arenasManager = plugin.getArenasManager();
        //arenasConfig = plugin.getArenasConfig();
    }

    @Override
    public void onExePlayer(Player player, String[] args) {
        if (args.length >= 1) {
            Arena arena = arenasManager.getArena(args[0]);
            String section = "arenas." + arena.getId() + ".secondPos";
            //arenasConfig.set(section + ".x", player.getLocation().getX());
            //arenasConfig.set(section + ".z", player.getLocation().getZ());
            //arena.getBorder().load(arenasConfig, "arenas." + arena.getId() + ".");
        }
    }
}
