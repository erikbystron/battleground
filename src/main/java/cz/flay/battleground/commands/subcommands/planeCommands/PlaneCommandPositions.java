package cz.flay.battleground.commands.subcommands.planeCommands;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.commands.SubCommand;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class PlaneCommandPositions extends SubCommand {

    public PlaneCommandPositions(BattleGround plugin) {
        super(plugin, "positions", "plane");
    }

    @Override
    public void onExePlayer(Player player, String[] args) {
        if (args.length >= 1) {
            if (args[0].equals("editor")) {
                player.setItemInHand(new ItemStack(Material.SKULL_ITEM, 1, (byte) 3));
            }
        }
    }
}
