package cz.flay.battleground.commands.subcommands.arenaCommands;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ArenaCommandMap extends SubCommand {

    private final ArenasManager arenasManager;

    public ArenaCommandMap(BattleGround plugin) {
        super(plugin, "map", "arena");
        arenasManager = plugin.getArenasManager();
    }

    @Override
    public void onExePlayer(Player player, String[] args) {
        if (args.length >= 3) {
            player.sendMessage(args[1]);
            arenasManager.getArena(args[0]).setWorld(Bukkit.getWorld(args[1]));
        }
    }
}
