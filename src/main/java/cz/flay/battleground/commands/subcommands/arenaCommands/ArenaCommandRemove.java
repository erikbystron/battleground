package cz.flay.battleground.commands.subcommands.arenaCommands;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.arena.ArenasManager;
import cz.flay.battleground.commands.SubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class ArenaCommandRemove extends SubCommand {

    private final ArenasManager arenasManager;

    public ArenaCommandRemove(BattleGround plugin) {
        super(plugin, "remove", "arena");
        arenasManager = plugin.getArenasManager();
    }

    @Override
    public void onExePlayer(Player player, String[] args) {
        if (args.length >= 1) {
            arenasManager.deleteArena(arenasManager.getArena(args[0]));
        }
    }

    @Override
    public void onExeConsole(ConsoleCommandSender commandSender, String[] args) {
        if (args.length >= 1) {
            arenasManager.deleteArena(arenasManager.getArena(args[0]));
        }
    }
}
