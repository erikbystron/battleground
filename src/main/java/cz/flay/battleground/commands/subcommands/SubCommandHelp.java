package cz.flay.battleground.commands.subcommands;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.commands.SubCommand;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class SubCommandHelp extends SubCommand {

    public SubCommandHelp(BattleGround plugin){
        super(plugin, "help", "player");
    }

    @Override
    public void onExePlayer(Player player, String[] args) {

    }

    @Override
    public void onExeConsole(ConsoleCommandSender commandSender, String[] args) {

    }
}
