package cz.flay.battleground.commands.subcommands;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.commands.SubCommand;
import org.bukkit.entity.Player;

public class SubCommandJoin extends SubCommand {

    public SubCommandJoin(BattleGround plugin) {
        super(plugin, "join", "player");
    }

    @Override
    public void onExePlayer(Player player, String[] args) {
        plugin.getBattlePlayer(player).joinGame(plugin.getGamesManager().getGame());
    }
}
