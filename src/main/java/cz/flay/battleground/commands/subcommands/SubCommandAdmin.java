package cz.flay.battleground.commands.subcommands;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.commands.SubCommand;
import cz.flay.battleground.menus.menu.admin.MainMenu;
import org.bukkit.entity.Player;

public class SubCommandAdmin extends SubCommand {

    private final MainMenu mainMenu;

    public SubCommandAdmin(BattleGround plugin) {
        super(plugin, "admin", "player");
        mainMenu = new MainMenu(plugin);
    }

    @Override
    public void onExePlayer(Player player, String[] args) {
        mainMenu.createMenu(plugin.getBattlePlayer(player));
    }
}
