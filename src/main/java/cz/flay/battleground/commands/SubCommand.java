package cz.flay.battleground.commands;

import cz.flay.battleground.BattleGround;
import cz.flay.battleground.player.BattlePlayer;
import cz.flay.battleground.storage.locale.LocaleManager;
import cz.flay.battleground.storage.locale.Message;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class SubCommand {

    protected final BattleGround plugin;
    private final LocaleManager localeManager;
    private final String permission, cmd;

    protected SubCommand(BattleGround plugin, String cmd, String group){
        localeManager = plugin.getLocaleManager();
        this.plugin = plugin;
        this.cmd = cmd;
        this.permission = "battleground.command." + group + "-" + cmd;
    }

    protected void sendMessage(CommandSender sender, Message message){
        if (sender instanceof Player) {
            BattlePlayer player = plugin.getBattlePlayer((Player) sender);
            sender.sendMessage(localeManager.getMessage(player, message));
        } else {
            sender.sendMessage(localeManager.getConsoleMessage(message));
        }
    }

    public String getPermission(){
        return permission;
    }

    public boolean check(String cmd){
        return this.cmd.equalsIgnoreCase(cmd);
    }

    public void onExePlayer(Player player, String[] args) {

    }

    public void onExeConsole(ConsoleCommandSender commandSender, String[] args) {
        denied(commandSender);
    }

    protected void denied(CommandSender commandSender){
        sendMessage(commandSender, Message.ONLY_PLAYER);
    }
}
