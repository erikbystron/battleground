package cz.flay.battleground.util;

import cz.flay.battleground.player.BattlePlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Placeholders {

    private final HashMap<String, String> placeholders;

    public Placeholders() {
        placeholders = new HashMap<>();
    }

    public Placeholders(Placeholders placeholders) {
        this.placeholders = new HashMap<>();
        this.placeholders.putAll(placeholders.getPlaceholders());
    }

    public Placeholders add(String placeholder, Object value) {
        placeholders.put(placeholder, value.toString());
        return this;
    }

    public HashMap<String, String> getPlaceholders() {
        return placeholders;
    }

    public String applyPlaceholders(BattlePlayer player, String message) {
        String replaced = message;

        for (String placeholder : placeholders.keySet()) {
            replaced = replaced.replaceAll(placeholder, placeholders.get(placeholder));
        }

        if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
            me.clip.placeholderapi.PlaceholderAPI.setPlaceholders(player.getBukkitPlayer(), replaced);
        }

        return replaced;
    }

    public List<String> applyPlaceholders(BattlePlayer player, List<String> messages) {
        List<String> replaced = new ArrayList<>();
        for (String message : messages) {
            replaced.add(applyPlaceholders(player, message));
        }

        return replaced;
    }
}
