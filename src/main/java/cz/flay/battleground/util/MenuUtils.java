package cz.flay.battleground.util;

import cz.flay.battleground.arena.Arena;
import cz.flay.battleground.arena.plane.PlaneRoute;
import cz.flay.battleground.items.GameItem;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

public class MenuUtils {

    public int getSize(int objects) {
        int size;

        if (objects <= 7){
            size = 27;
        }else if (objects <= 14){
            size = 36;
        }else if (objects <= 21){
            size = 45;
        }else {
            size = 54;
        }

        return size;
    }

    public int[] getPositions(int size) {
        int[] position;

        if (size <= 7){
            position = new int[]{
                    10, 11, 12, 13, 14, 15, 16
            };
        }else if (size <= 14){
            position = new int[]{
                    10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25
            };
        }else if (size <= 21){
            position = new int[]{
                    10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34
            };
        }else {
            position = new int[]{
                    10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43
            };
        }

        return position;
    }

    public int neededPages(int objects) {
        return (int) Math.floor(objects / 28) + 1;
    }

    public List<Arena> getPageArenas(List<Arena> list, int page) {
        List<Arena> pageObjects = new ArrayList<>();
        if (list.size() <= 28) {
            return list;
        }

        int startIndex = 28 * (page - 1);
        int stopIndex = 28 * page;

        for (Arena object : list) {
            if (list.indexOf(object) < stopIndex && list.indexOf(object) > startIndex) {
                pageObjects.add(object);
            }
        }

        return pageObjects;
    }

    public List<GameItem> getPageGameItems(List<GameItem> list, int page) {
        List<GameItem> pageObjects = new ArrayList<>();
        if (list.size() <= 28) {
            return list;
        }

        int startIndex = 28 * (page - 1);
        int stopIndex = 28 * page - 1;

        for (GameItem object : list) {
            if (list.indexOf(object) <= stopIndex && list.indexOf(object) >= startIndex) {
                pageObjects.add(object);
            }
        }

        return pageObjects;
    }

    public List<PlaneRoute> getPageRoutes(List<PlaneRoute> list, int page) {
        List<PlaneRoute> pageObjects = new ArrayList<>();
        if (list.size() <= 28) {
            return list;
        }

        int startIndex = 28 * (page - 1);
        int stopIndex = 28 * page - 1;

        for (PlaneRoute object : list) {
            if (list.indexOf(object) <= stopIndex && list.indexOf(object) >= startIndex) {
                pageObjects.add(object);
            }
        }

        return pageObjects;
    }

    public List<Location> getPagePositions(List<Location> list, int page) {
        List<Location> pageObjects = new ArrayList<>();
        if (list.size() <= 28) {
            return list;
        }

        int startIndex = 28 * (page - 1);
        int stopIndex = 28 * page - 1;

        for (Location object : list) {
            if (list.indexOf(object) <= stopIndex && list.indexOf(object) >= startIndex) {
                pageObjects.add(object);
            }
        }

        return pageObjects;
    }
}
