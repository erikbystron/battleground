package cz.flay.battleground.util;

import de.tr7zw.itemnbtapi.NBTItem;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.List;

import static cz.flay.battleground.util.Skull.getCustomSkull;
import static cz.flay.battleground.util.Skull.getPlayerSkull;

public class ItemBuilder {

    private final ItemStack itemStack;
    private final ItemMeta itemMeta;
    private final List<String> lore = new ArrayList<>();

    public ItemBuilder(ItemStack itemStack) {
        this.itemStack = itemStack;
        itemMeta = itemStack.getItemMeta();
        try {
            lore.addAll(itemMeta.getLore());
        } catch (NullPointerException ignored) {}
    }

    public ItemBuilder(Material material){
        itemStack = new ItemStack(material);
        itemMeta = itemStack.getItemMeta();
    }

    public ItemBuilder(Material material, byte data){
        itemStack = new ItemStack(material, 1, data);
        itemMeta = itemStack.getItemMeta();
    }

    public ItemBuilder(String url){
        itemStack = getCustomSkull(url);
        itemMeta = itemStack.getItemMeta();
    }

    public ItemBuilder(Player player) {
        itemStack = getPlayerSkull(player.getName());
        itemMeta = itemStack.getItemMeta();
    }

    public ItemBuilder setName(String name){
        itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        return this;
    }

    public ItemBuilder setName(String name, boolean colored){
        if (colored){
            setName(name);
        } else {
            itemMeta.setDisplayName(name);
        }
        return this;
    }

    public ItemBuilder addLore(String lore){
        this.lore.add(ChatColor.translateAlternateColorCodes('&', lore));
        return this;
    }

    public ItemBuilder addLore(String lore, boolean colored){
        if (colored){
            addLore(lore);
        } else {
            this.lore.add((lore));
        }
        return this;
    }

    public ItemBuilder addEnchantment(Enchantment enchantment, int level) {
        itemStack.addUnsafeEnchantment(enchantment, level);
        return this;
    }

    public ItemBuilder addLore(List<String> lore) {
        for (String string : lore) {
            addLore(string);
        }
        return this;
    }

    public ItemBuilder setSkullOwner(Player player){
        SkullMeta skullMeta = (SkullMeta) itemMeta;
        skullMeta.setOwner(player.getName());
        return this;
    }

    public ItemBuilder addNBTTag(String key, String value) {
        NBTItem item = new NBTItem(toItemStack());
            item.setString(key, value);
        return new ItemBuilder(item.getItem());
    }

    public ItemStack toItemStack(){
        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    @Override
    public ItemBuilder clone() {
        return new ItemBuilder(itemStack);
    }
}
