package cz.flay.battleground.util;

import org.apache.commons.lang3.Range;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class RandomPicker {

    private final ThreadLocalRandom random;

    public RandomPicker() {
        random = ThreadLocalRandom.current();
    }

    public int getRandom(List<? extends Randomable> list) {
        int value = 0;
        HashMap<Range<Integer>, Randomable> randomableMap = new HashMap<>();

        for (Randomable randomable : list) {
            randomableMap.put(Range.between(value, (value + randomable.getProbability())), randomable);
            value += randomable.getProbability();
        }

        try {
            int randomInt = random.nextInt(0, value);

            for (Range<Integer> range : randomableMap.keySet()) {
                if (range.contains(randomInt)) {
                    return list.indexOf(randomableMap.get(range));
                }
            }
        } catch (IllegalArgumentException ignored) {}

        return 0;
    }
}