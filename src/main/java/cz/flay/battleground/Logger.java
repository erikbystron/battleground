package cz.flay.battleground;

import org.bukkit.Bukkit;

public class Logger {

    public static void line() {
        log(false, "#################################################################################################");
    }

    public static void log(boolean prefix, String message) {
        if (prefix) {
            Bukkit.getLogger().info("[BattleGround] " + message);
        } else {
            Bukkit.getLogger().info(message);
        }
    }

    public static void log(String message) {
        log(true, message);
    }

    public static void error(boolean prefix, String message) {
        if (prefix) {
            Bukkit.getLogger().severe("[BattleGround] " + message);
        } else {
            Bukkit.getLogger().severe(message);
        }
    }

    public static void error(String message) {
        error(true, message);
    }

    public static void warn(boolean prefix, String message) {
        if (prefix) {
            Bukkit.getLogger().warning("[BattleGround] " + message);
        } else {
            Bukkit.getLogger().warning(message);
        }
    }

    public static void warn(String message) {
        warn(true, message);
    }
}
